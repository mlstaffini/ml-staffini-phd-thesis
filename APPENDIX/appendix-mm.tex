We report here the mode space derivation of the equations of motion of Chapter \ref{mm}; these were then coded in FORTRAN, and the results were not numerically viable because of low precision in summing oscillatory terms.

\section{Setting up the equations of motion}
\label{sec:eom-general}

We consider a longitudinally pumped cavity with a Bose-condensed cloud of atoms. We assume the BEC is confined in the z-direction, in the separable form $\Psi(x,y,z)=\psi(x,y)Z(z)$. 
We consider the in-plane motion of the atoms, which obey the equation of motion:

\begin{equation}
\label{eq:eqnpsifirst}
i\hbar \partial_t \psi(\vect{r})= -\frac{\hbar^2 \nabla^2}{2m_a}\psi(\vect{r})+ \frac{m_a \omega_T^2}{2} \vect{r}^2 \psi(\vect{r}) - \mathcal{E}_0 
\int \, \mathrm{d}z |Z(z)|^2|\varphi(\vect{r},z)|^2 \psi(\vect{r})
\end{equation}

where $m_a$ is the mass of the atoms, $\omega_T$ is the oscillator trap frequency, $\mathcal{E}_0$ is the shift due to the photon population, and the three-dimensional light field can be written as:

\begin{equation}
\label{eq:fulllightfield}
\varphi(x,y,z)= \sum_{n,m,q} \alpha_{nmq} u_{nmq}(x,y,z)
\end{equation}

where m, n label transverse modes and q labels the longitudinal modes. Note that $q$ is not completely free: in the confocal and near-confocal case we consider, the index $q$ relates to the $q$ of the fundamental mode $q_0$ and the m,n indeces as $2q+m+n=2 q_0$. This yields the condition that $m$ and $n$ must have the same parity, and $q$ is then fully determined by the other two indeces and can be dropped. We will restrict ourselves to even modes for which $m+n$ is even.

Note that $u_{n,m}$ as defined below has units of 1/L, and since the $\alpha_{nm}$ are dimensionless this means that $\mathcal{E}_0$ has units of energy times area, and the light field $\varphi(\vect{r},z)$ also has units of 1/L. 

The three-dimensional light equation of motion reads: 

\begin{multline}
\label{eq:eqnalphafirst}
 i\partial_t \alpha_{nm}=(\omega_{n,m}-\omega_{pump}-i\kappa)\alpha_{n,m}+f_{n,m} \\ \qquad - \mathcal{E}_0 N \sum_{n',m'} \int \int \mathrm{d}^2\vect{r} \mathrm{d}z |Z(z)|^2 |\psi(\vect{r})|^2 u^*_{m,n}(\vect{r},z) u_{m',n'}(\vect{r},z)\alpha_{m',n'}
\end{multline}

where we have taken $\hbar=1$, and therefore here the units of $\mathcal{E}_0$ are really frequency. 

The pump term in eq. \ref{eq:eqnalphafirst} is defined as:

\begin{equation}
\label{eq:pumptermdef}
  f_{m,n} = \int d^2\vect{r} f(\vect{r}) u^\ast_{m,n,q}(\vect{r}, - z_R),
\end{equation}

where $f(\vect{r})$ is the spatial profile of the pump in units of frequency over length (or root area). We will assume this takes a Gaussian form

\begin{displaymath}
\label{eq:gaussianpump}
f(\vect{r})=f_P e^{\frac{\vec{r}^2}{2 \sigma_P^2}}
\end{displaymath}

where $f_P$ is the pump intensity parameter, absorbing the normalisation factors, and $\sigma_P$ is the pump width.

\subsection{Basis functions}
\label{sec:basisfunction}

In solving the above, we may choose to work either with Gauss-Hermite or Gauss-Laguerre modes.  For numerics, Laguerre is preferable as it allows us to consider the radially symmetric case efficiently. Hermites are easier to do analytically.

In the Hermite case we considered:

\begin{multline}
  u_{m,n}(x,y,z) = 
  \frac{1}{w(z)}
  \chi_n\left( \frac{\sqrt{2} x}{w(z)} \right)
  \chi_m\left( \frac{\sqrt{2} y}{w(z)} \right)
  \\
  \cos \left[
    k \left( z + \frac{r^2}{2R(z)} \right) - (m+n+1) \psi(z) + \xi_{q} \right]
\end{multline}

where the beam waist $w(z)$, radius of curvature $R(z)$ and Guoy phase $\psi(z)$ are discussed in the appendix \ref{sec:gaussian-optics}. The $\xi_q$ term does not really need a $q$ defined since, as discussed above, we can get $q$ entirely from $m, n$ and $q_0$.

The functions $\chi_n(x)$ are normalised Gauss-Hermite functions, i.e.

\begin{displaymath}
  \chi_n(x) = \sqrt[4]{\frac{2}{\pi}}
  \frac{1}{\sqrt{2^n n!}} H_n(x) \exp\left(-\frac{x^2}{2} \right)
\end{displaymath}

In the Laguerre case we have:

\begin{multline}
  u_{m,n,q}(x,y,z) = 
  \frac{1}{w(z)}
  \chi^L_{n,m}\left( \frac{\sqrt{2}r}{w(z)} \right)
  e^{i m \theta}
  \\
  \cos \left[
    k \left( z + \frac{r^2}{2R(z)} \right) - (2n+m+1) \psi(z) + \xi_{q} \right]
\end{multline}

where $n$ now labels radial associated Laguerre functions, i.e.

\begin{displaymath}
  \chi^L_{n,m}(x) = 
  \sqrt{ \frac{2 n!}{\pi (m+n)!}}
  x^m L_n^m(x^2) \exp(-x^2/2)
\end{displaymath}

Note that this definition of $u_{nm}$ is also normalised.

\subsection{Mode frequency}
\label{sec:modefrequency}

We can rewrite the mode frequency in Eq. \ref{eq:eqnalphafirst} explicitly in terms of the fundamental frequency and deviation from nonconfocality.
From Appendix \ref{sec:gaussian-optics}, we have (taking sum and difference of cosine term respectively):

\begin{displaymath}
\xi=\frac{\pi}{2}(q+1), \quad 2kz_M=\frac{\pi}{2} \left[ 2q+(2n+m+1)\frac{4\psi(z_M)}{\pi}\right]
\end{displaymath}

Using $\omega_{nm}=c k_{nm}$ and the mode family equation and rearranging the second equation:

\begin{align}
\omega_{nm} & =\frac{\pi}{4} \frac{c}{z_M}
\left[ 2q_0-(2n+m)+(2n+m+1)\frac{4\psi(z_M)}{\pi}\right] \nonumber \\
 & = \frac{c}{z_M}
\left[  \frac{\pi}{2}q_0+\psi(z_M)+(2n+m)(\psi(z_M)-\frac{\pi}{4})\right] \nonumber
\end{align}

Now to obtain the frequency of the fundamental mode we set $n=m=0$:

\begin{equation}
\omega_0=\frac{c}{z_M} \left[ \frac{\pi}{2}q_0+\psi(z_M)\right]
\end{equation}

Therefore we obtain a relation between the frequencies of higher modes and the fundamental:

\begin{equation}
\omega_{nm}=\omega_0+ \frac{c}{z_M}
(2n+m)\left(\psi(z_M)-\frac{\pi}{4}\right) \nonumber \nonumber \\
\end{equation}

Defining a detuning $\Delta_{nm}=\omega_{nm}-\omega_{pump}$, we get:

\begin{equation}
\Delta_{nm}=\Delta_0+(2n+m)\epsilon
\end{equation}

where $\epsilon$ is the confocality parameter (in units of frequency), $\epsilon=\frac{c}{z_M} \left(\psi(z_M)-\frac{\pi}{4}\right)$.

The the z-integral in both equations integrate as in Section \ref{zoverlaps}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODE SPACE

\section{Equations of motion in mode space}

We first try to solve the problem in mode space, to keep the full information about the light field modes expressed in Eq. \ref{eq:eqnalphafirst}. We solve this in Laguerre basis hoping to use their cylindrical symmetry.
We define $\psi$ (normalised) in terms of mode basis factors similarly to Eq. \ref{eq:fulllightfield}:

\begin{equation}
\label{eq:fullatomicfieldmodes}
\psi(r, \theta)=\frac{1}{\sqrt{2}\lho} \sum_{m,n} \psi_{nm} \  \chi_{n,m}^L\left(\frac{r}{\lho}\right)\ e^{im\theta}
\end{equation}

We can substitute this into Eq. \ref{eq:fullatomicfield}, then multiply both sides by $e^{-i \mu \theta} \chiaf$ and integrate over $d^2r$ to use the orthonormality relation of Laguerre functions, from Appendix \ref{sec:normalisation-from-HG} (noting the extra factor for the normalisation of the $\chi$s). 

\subsection{Interaction terms}
To obtain equations of motion in mode basis we work on Eq.\ref{eq:eqnpsifirst}, starting with the most complicated term, the interaction term; however, since $\psi$ appears in Eq. \ref{eq:fulllightfield}, we look at both integral terms in the hope we can get them into a similar form.

Using Eq. \ref{eq:zoverlap} to perform z-integral, substituting in \ref{eq:fulllightfield}, and carrying out the $\theta$ integral of the cylindrical coordinate $\textbf{r}$, together with substituting in the full form of psi as explained above we obtain the integral term of the atomic field equation $i \hbar \partial_t \psi_{\nu\mu}$:
	
\begin{align}
\label{eq:lasttermpsi}
\mathcal{E}_0 \sum_{n,m,n',m',\nu',\mu'} \frac{\lf^* \lfp}{\wzsq \lho^2} \frac{\pi}{2} &\delta_{m'-m+\mu-\mu'}
\nonumber \\ \times &\cos \big[(2n+m-  2n'-m') 
\ \theta(z_0)\big]  \intfour \psi_{\nu' \mu'}
\end{align}

where we use greek subscripts to indicate atomic field terms and latin letters for light field, and the integral term is:

\begin{equation}
\label{eq:intfour}
\mathcal{I}nt[\chi^4]=\int \mathrm{d}r \ r \  \chiaf \chiafp \chilf \chilfp
\end{equation}

Note: the $\pi$ term comes from the fact that while the other terms containing $\psi$ will be integrated in both $r$ and $\theta$ explicitly, in the integral term the $r$ integral remains to be evaluated, so that we have to divide this last term by a factor of $l_{HO}^2/\pi$ coming from LG normalisation to remove the factor from the $\partial_t$ term and the kinetic/potential energy terms.

Similarly in Eq. \ref{eq:fulllightfield}, substituting in $\psi$ and carrying out the theta integral:

\begin{align}
\label{eq:lasttermalpha}
\mathcal{E}_0 \sum_{n,m,n',m', \nu, \mu, \nu',\mu'} \frac{ \af^* \afp}{\wzsq \lho^2}
\frac{\pi}{2} &\delta_{m'-m+\mu'-\mu,0} \nonumber \\ &
\cos \big[(2n+m-  2n'-m') 
\ \theta(z_0)\big] \intfour
\end{align}

\subsection{Pump term}

We use a gaussian pump profile $f(\vect{r})= f_0 e^{\frac{-r^2}{2\sigma_P^2}}$ in eq. \eqref{eq:pumptermdef}, where $f_0$ is the variable pump intensity. Note that this expression doesn't actually work (yields cos=0 since this is the perfect cavity case). One should really consider the overlap of effective quasi-modes with the eigenmodes of cavity, but oh well.

In practice: set the cos term=1, solve:

\begin{equation}
f_{m,n,q} = \int d^2\vect{r} f_0 \ \exp \left(\frac{-r^2}{2\sigma_P^2} \right) \frac{1}{w(-z_{M})} \ \chi_{n,m}^L  \left(\frac{\sqrt{2}r}{w(-z_{M})}\right) \frac{e^{im\theta}}{\sqrt{1+\delta_{m,0}}}
\end{equation}

Solving this and enforcing the delta function from theta integral:

\begin{equation}
\label{eq:pumpterm}
f_{n,m}=\delta_{m,0} \ f_0  \ \sqrt{\pi}\frac{w(-z_{R})}{2} \sum_k (-1)^k  \frac{n!}{(n-k)! k!} \left(\frac{2 \sigma_P}{\sqrt{w^2(-z_{R})+2\sigma_P^2}} \right)^{2k+2} 
\end{equation}

\subsection{Laguerre equation to get energy shift term}

We want to combine the first and second term in Eq.\ref{eq:eqnpsifirst} and use the Laguerre equation to obtain:

\begin{equation}
\label{eq:laguerre1}
-\frac{\hbar^2 \nabla^2}{2m_a} \psi+ \frac{m_a \omega_T^2}{2}r^2 \psi= \mathcal{E}_{nm} \psi
\end{equation}

We have $\lho=\sqrt{\frac{\hbar}{m_A \omega_T}}$, and in dimensionful 2D cylindrical coordinates $\nabla^2=\frac{1}{r} \frac{\partial}{\partial r}\left( r \frac{\partial }{\partial r} \right)+\frac{1}{r^2}\frac{\partial ^2}{\partial \theta^2} $; however, we want to work in dimensionless units $\tilde{r}=r/l_{HO}$:

\begin{equation}
\label{eq:laguerre2}
\bigg[ 2 \frac{\mathcal{E}_{nm}}{\hbar \omega_{T}}  + \frac{1}{\tilde{r}} \frac{\partial}{\partial \tilde{r}}\left( \tilde{r} \frac{\partial }{\partial \tilde{r}} \right)+\frac{1}{\tilde{r}^2}\frac{\partial ^2}{\partial \theta^2}  - \tilde{r}^2 \bigg]\psi =0
\end{equation}
\\
Now, given that the only part of $\psi$ that is r-dependent is $\left( \frac{r}{l_{HO}} \right)^m \ L^m_n\left(\frac{r^2}{l^2_{HO}} \right) \ e^{-r^2/2 l_{HO}^2}$, and that the only theta dependent term is $e^{im\theta}$, we have $\frac{1}{\tilde{r}^2}\frac{\partial ^2}{\partial \theta^2}\psi=-\frac{m^2}{\tilde{r}^2}\psi$, and the dimentionless r-derivatives in terms of derivatives of $L$. Simplifying and dropping the tilde for convenience, we get from eq. \eqref{eq:laguerre2} an equation in terms of the Laguerre terms:

\begin{align}
L'' + \frac{L'}{r} \left( 2m+1-2r^2 \right) + 
 L \left(2 \frac{\mathcal{E}_{nm}}{\hbar \omega_{T}} -2(m+1)\right) =0
\end{align}

This is similar to Laguerre equations $xy''+(\lambda+1-x)y'+n y=0$, where $n$ and m are integers; however, our Laguerre functions are in terms of $r^2$; to find a solution for this, let $L(r)=y(r^2)$ -- we then have then $L'=2ry'$,$L''=4r^2y''+2y=2xy''+2y'$, and the equation becomes:

\begin{align}
xy''+y'(m+1-x)+ y \ \frac{1}{2}\left(\frac{\mathcal{E}_{nm}}{\hbar \omega_{T}} -(m+1)\right)=0 
\end{align}

We can now read off the value of the integer n and rearrange to get $\mathcal{E}_{nm}$ in terms of m and n:

\begin{align}
\label{eq:laguerreterm}
\mathcal{E}_{nm}=\hbar \omega_T (2n+m+1)
\end{align}

\subsection{Equations of motion for $\af$}

Gathering the terms, substituting $\psi$ everywhere and integrating as above we get:

\begin{multline}
i \partial_t \psi_{\nu \mu} = \omega_T (2\nu+\mu+1) \psi_{\nu, \mu}
\\ -  \mathcal{E}_0 \sum_{n,m,n',m', \nu', \mu'} \frac{\lf^* \lfp}{\wzsq}  \cos(...) \delta_{m'-m+\mu-\mu} 
\frac{\pi}{2}
\intfour \psi_{\nu' \mu'}
\end{multline}

and

\begin{multline}
\partial_t \alpha_{nm}= -i(\Delta_{nm}-i\kappa) \alpha_{nm}-if_{nm} +i\mathcal{E}_0 \sum_{\nu, \mu,\nu', \mu',n',m'} \\ \frac{ \psi_{\nu\mu}^* \psi_{\nu'\mu'}}{ w^2(z_0)} \cos (...) \delta_{m'-m+\mu-\mu} \ 
\frac{\pi}{2} \intfour \ \alpha_{n'm'} 
\end{multline}

where we've redefined dimensionless units of space:

\begin{equation}
\tilde{r}=\frac{r}{l_{HO}}, \qquad d\tilde{r}=\frac{dr}{l_{HO}} \qquad \mathrm{and} \qquad \eta=\frac{\sqrt{2}\lho}{\wz} 
\end{equation}

The integral reads, dropping the tilde:

\begin{equation}
\int \mathrm{d}r \ r \ \chi_{\nu, \mu}^L (r) \ \chi_{\nu', \mu'}^L (r) \ \chi_{n,m}^L (\eta r) \ \chi_{n',m'}^L (\eta r)
\end{equation}

Note that $\mathcal{E}_0$ has (setting $\hbar=1$ in the atomic field equation too now) units of frequency times area.
To this end, we want to absorb the beam waist (variance) of the light mode yielding:

\begin{equation}
E_0=\mathcal{E}_0 \left(\frac{\sqrt{2}}{w(z_0)} \right)^D
\end{equation}

We therefore obtain the equations of motion for $\af$ and $\lf$ where the $\lf$, $\af$ and $\intfour$ are unitless, and $E
_0$ is in units of energy:


\begin{multline}
\label{eq:psinm-modespace}
i\partial_t \psi_{\nu \mu} = \omega_T (2\nu+\mu+1) \psi_{\nu, \mu}
- E_0 \sum_{n,m,n',m', \nu', \mu'} \lf^* \lfp
\\ \times \cos \big[(2n+m-  2n'-m') \ \theta(z_0)\big] 
\delta_{m'-m+\mu'-\mu} \frac{\pi}{4} \intfour \ \psi_{\nu'\mu'} 
\end{multline}

and:

\begin{multline}
\label{eq:alphanm-modespace}
\partial_t \alpha_{nm}= -i(\Delta_{nm}-i\kappa) \alpha_{nm}-if_{nm} +i E_0 \sum_{\nu, \mu,\nu', \mu',n',m'} \psi_{\nu\mu}^* \psi_{\nu'\mu'} \times \\     
\times \cos \big[(2n+m-  2n'-m') \ \theta(z_0)\big]  \delta_{m'-m+\mu'-\mu} 
\frac{\pi}{4}  \intfour \ \alpha_{n'm'} 
\end{multline}

\section{Linear Stability Analysis}

We start from eqs. \eqref{eq:psinm-modespace} and \eqref{eq:alphanm-modespace}. Definining for short $Q^{m,m',\mu,\mu'}_{n,n',\nu,\nu'}=\cos \big[(2n+m-  2n'-m')\ \theta(z_0)\big]  \ \intfour$, we have:

\begin{multline}
\partial_t \psi_{\nu \mu} = -i\omega_T (2\nu+\mu+1) \psi_{\nu, \mu}
+i E_0 \sum_{n,m,n',m', \nu', \mu'} \\
\delta_{m'-m+\mu'-\mu} \ Q^{m,m',\mu,\mu'}_{n,n',\nu,\nu'} \ \lf^* \lfp  \psi_{\nu'\mu'} 
\end{multline}

and

\begin{multline}
\partial_t \alpha_{nm}= -i(\Delta_{nm}-i\kappa) \alpha_{nm}-if_{nm} +i E_0 \sum_{\nu, \mu,\nu', \mu',n',m'} \\ \delta_{m'-m+\mu'-\mu} \ 
Q^{m,m',\mu,\mu'}_{n,n',\nu,\nu'} \ \psi_{\nu\mu}^* \psi_{\nu'\mu'}  \alpha_{n'm'} 
\end{multline}

We want to consider small deviations from the steady state, and determine when they start growing rather than decaying.
First, split the fields in steady state plus perturbations:

\begin{align}
\af=& \ \Psi_{\nu} \delta_{\mu,0} + \delta \af \\
\lf=& \ A_{n} \delta_{m,0} + \delta \lf
\end{align}

where $A_n$ and $\Psi_{\nu}$ are the steady states of the light and atomic fields.

Substituting this into the equations, only keeping terms up to first order in $\delta$, and subtracting the equations for $\partial_t \Psi_{\nu}$ and $\partial_t A_n$ from the first and second equation respectively, we obtain equations of motion for the perturbations only (the delta functions from the steady state terms and the four-terms delta function combine to pick out a single azimuthal quantum number for the second terms):

\begin{multline}
\partial_t (\delta \af) = -i\omega_T (2\nu+\mu+1) \delta \af +i E_0  \sum_{n',\nu, \nu'} \bigg[ 
Q^{0,\mu,\mu,0}_{n,n',\nu,\nu'}  A^*_n \Psi_{\nu'} \delta \alpha_{n'\mu} + \\
Q^{-\mu,0,\mu,0}_{n,n',\nu,\nu'}  A_{n'} \Psi_{\nu} \delta \alpha^*_{n\mu} + 
Q^{0,0,\mu,\mu}_{n,n',\nu,\nu'}  A^*_{n} A_{n'} \delta \psi_{\nu' \mu} \bigg]
\end{multline}

and

\begin{multline}
\partial_t (\delta \lf) = -i(\Delta_{nm}-i\kappa) \delta \lf +i E_0  \sum_{n',\nu, \nu'} \bigg[ 
Q^{m,m,0,0}_{n,n',\nu,\nu'}  \Psi^*_{\nu} \Psi_{\nu'} \delta \alpha_{n'm} + \\
Q^{-\mu,0,\mu,0}_{n,n',\nu,\nu'}  \Psi^*_{\nu} A_{n'}  \delta \psi_{\nu' m} + 
Q^{0,-m,0,m}_{n,n',\nu,\nu'}  \Psi_{\nu'} A_{n'} \delta \psi^*_{\nu \mu} \bigg]
\end{multline}

Where the values of $A_n$ and $\psi_\nu$ can obtained from the steady state where all m-indeces are zero (spherically symmetric case), provided we can calculate the integrals numerically.

\subsection{Calculating $\mathcal{I}nt[\chi^4]$}

The stability analysis considerations above mean we can get some information about the state of the system by computing the $\intfour$ terms for only six combinations of indeces, $(-m,0,m,0)$, $(m,0,m,0)$, $(-m,m,0,0)$, $(m,m,0,0)$, $(0,0,m,-m)$, and $(0,0,m,m)$, on top of the steady state case $(0,0,0,0)$. 

\subsubsection{Calculating $\mathcal{I}nt[\chi^4]$ analytically}

We tried calculating the (ultimately gaussian) integral analytically. Ignoring prefactors to illustrate:

\begin{multline}
\intfour \propto \int \mathrm{d}r \ r \ \left( \eta r\right)^{m+m'}  r^{\mu+\mu'} e^{-r^2(1+\eta^2)}
  L^m_n\left(\eta r^2\right) \\  L^{m'}_{n'}\left(\eta r^2\right)  L^{\mu}_{\nu}\left( r^2 \right) L^{\mu'}_{\nu'}\left(r^2\right)
\end{multline}

Expanding the Laguerre polynomials, indeces $\kappa$ and $k$ and their primed versions are summed up to $n$ and $\nu$:

\begin{equation}
\sum_{\kappa, \kappa', k, k'} \int \mathrm{d}r \ r^{2(k+k'+\kappa+\kappa')+m+m'+\mu+\mu'+1} e^{-r^2(1+\eta^2)}
\end{equation}

This gaussian integral yields:

\begin{multline}
\label{eq:intfoureqn}
\intfour \propto \sum_{k, k', \kappa, \kappa'} \frac{1}{2} \left(\frac{(m+m'+\mu+\mu')}{2}+k+k'+\kappa+\kappa'\right)! \\ \left(\frac{1}{\sqrt{1+\eta^2}}\right)^{2\left(\frac{(m+m'+\mu+\mu')}{2}+k+k'+\kappa+\kappa'\right)+2}
\end{multline}

This was guided by the notion that doing these sums instead of the integrals numerically would be preferrable. However, the sum sampled numerically is highly oscillatory due to lack of precision in summing entries (Fig.\ref{fig:oscillatorysum}).

\begin{figure}[h]
\centering
\includegraphics[width=0.7\linewidth]{oscillatoryseries}
\caption{Convergence of the sum over entries from the FORTRAN code.}
\label{fig:oscillatorysum}
\end{figure}

For completeness, the factors before and after the sum are:

\begin{multline}
A(n,m,n',m',\nu,\mu,\nu',\mu') = \\ \sqrt{\frac{n!}{(m+n)!}} \frac{1}{\sqrt{1+\delta_{m,0}}} \sqrt{\frac{n'!}{(m'+n')!}} \frac{1}{\sqrt{1+\delta_{m',0}}} \\
\sqrt{\frac{ \nu!}{(\mu+\nu)!}} \frac{1}{\sqrt{1+\delta_{\mu,0}}}
\sqrt{\frac{ \nu'!}{(\mu'+\nu')!}} \frac{1}{\sqrt{1+\delta_{\mu',0}}}
\end{multline}

\begin{multline}
B(n,m,k,n',m',k',\nu, \mu,\kappa,\nu',\mu',\kappa') =  \\ 
(-1)^{k} \frac{(n+m)!}{(n-k)!(m+k)!k!}
(-1)^{k'} \frac{(n'+m')!}{(n'-k')!(m'+k')!k'!} \nonumber \\
(-1)^{\kappa} \frac{(\nu+\mu)!}{(\nu-\kappa)!(\mu+\kappa)!\kappa!}
(-1)^{\kappa'} \frac{(\nu'+\mu')!}{(\nu'-\kappa')!(\mu'+\kappa')!\kappa'!}
\\ \eta^{m+m'+2(k+k')} 
\end{multline}

Though these do not help things (B particularly will just make the sum more unwieldy and imprecise).

\subsubsection{Calculating $\mathcal{I}nt[\chi^4]$ numerically}

Next we tried to get integrals numerically using NAG routines in Fortran, and also semi-analytically using Wolfram script plugged into the original Fortran code. Both of these examples are available in the \texttt{previous-versions/modebasis} folder of code. I compared results obtained with Mathematica, numerically and by the sum method above, and found discrepancies even for the best case scenario. The integral doesn't seem well behaved enough to just attack in this way.