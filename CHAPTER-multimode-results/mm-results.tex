Fig \ref{realspace-res-1} shows the behaviour of the modulus squared of atomic (red) and light (blue) field as a function of $x$ at different time snapshots; the time evolution was simulated for $t=20$ms, in timesteps of $20 \mu$s (i.e. 1000 timesteps over the whole time evolution). Unless specified, we work at $z_a=0$ (centre of the cavity) and $\epsilon=0$ (perfectly confocal). Unless specified, we simulate a range $(-12,12)$ with 128 gridpoints. The graphs generated contain this information in the titles, as well as the value of $f_P$ and $\Delta_0$ simulated.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{compare_fP=10000_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_cloud-2_pump-2}
\caption{\textbf{\textbar} Time evolution for $f_P=10000, \Delta_a=\pm 100$. }
\label{fig:realspace-res-1}
\end{figure}

The results shown in Fig \ref{realspace-res-1}. We will further characterise the system behaviour below, but we can say immediately that it does not match our (admittedly naive) expectations of a detuning-dependent instability. The atomic and light field instead show some complicated behaviour which, while varying somewhat in exact value with detuning, isn't as clear as the statement of instability for only one sign of detuning $\Delta_0$. 

This is not overly surprising, given how many approximations were necessary to arrive to that conclusion. Summarising each subsequent approximations of Section \ref{mm-weak-coupling-deriv}:
\begin{enumerate}
\item We have assumed an infine pump spot size $\sigma_P$, Eq. \ref{eq:bigpumpspot}; 
\item We have assumed weak light-matter coupling, obtained Eq. \ref{eq:weak-cpl-eom};
\item We have assumed no overall trapping in setting up the linear stability analysis;
\item We have neglected the mirror image contribution;
\item We have assumed that the atomic field uniform density in writing Eq.\ref{mm-BdG-distr};
\item We have linearised in perturbation order, i.e. discarded contributions of order $\mathcal{O}(\delta^2)$ and higher, on the assumption that $\delta \psi$ is small, to write Eq. \ref{mm-BdG-eom}.
\end{enumerate}

Our conclusion that we expect instability for only one sign of $\Delta_0$ comes at the end of all these steps, and can only be reached by going through \textit{all} of them.

We can attempt to follow these steps with the code to see whether we can find at which step the simple understanding breaks, and whether we can gain more insight about the nature of the behaviour.

\subsection{One-dimensional realspace code, no assumptions}

We begin by characterising the behaviour in Fig. \ref{fig:realspace-res-1} a little more. Given that this does not show behaviour we immediately understand, and that we have no parameter space `map' to guide us, we'll just characterise a few aspects, then move on with our plan to follow the assumptions trail. 


\subsubsection{Dependence on $f_P$ and $\abs{\Delta_a}$}

We see complex behaviour for $f_P=10000$, and very weakly for $f_P=1000$; for $f_P<1000$, the effect of the light-matter coupling in negligible and the atomic cloud just breathes as expected in a simple harmonic oscillator (see Appendix \ref{sec:sanity-check-interaction-off}).

Naively, the value of $f_P$ for which we see instability for seems `high', but this isn't really a meaningful statement in itself; unitful quantities need to be compared to other energy scales in the system in order to say -- size is only relative.

We can get a back-of-the-envelope ideas about those from the weak coupling calculation, if we consider preparing the cloud in a simple harmonic oscillator (as we have without the interaction) and then asking how big the perturbation of turning on the light field is on the harmonic oscillator levels; this amounts to comparing $\omega_T$ to the prefactors of the interaction term, $f_P^2 N \Delta_a E_0/(\Delta_a^2 + \kappa^2)^2$; we are using $\omega_T=1$, while the prefactor of the interaction with $E_0=50$, $\Delta_a=100$, $\kappa=1000$ and $N=10^5$ is $f_P^2 \times 2.5 \times 10^{-2}$, so we expect $f_P$ of order $10$ or $100$ to be enough; however, this calculation only makes sense in the weak coupling approximation where we are treating this as a perturbation. The weak coupling expansion is justified for small $E_0 N/(\Delta_a+i \kappa)$, but here this is not small as the denominator $E_0 N$ is of order $10^6$, $\kappa=1000$ and we're only considering $\Delta_a$ of order 10 or 100. In this case, we do not have an intuitive understanding of whether this is a `big' value of pump because this kind of consideration does not apply.

If we want this kind of calculation to be meaningful, we need to make $E_0$ several order of magnitudes smaller in order to apply the reasoning above. This makes the interaction prefactor $f_P^2 N \Delta_a E_0/(\Delta_a^2 + \kappa^2)^2 \sim f_P^2 \times 10^{-9}$, so we expect to need $f_P$ of order $\sim 10^{6}$ for strong interaction; if we run the code for $E_0=0.01$, we indeed see the complex behaviour at $f_P=10^6$, and weakly at $f_P=10^5$, Fig. \ref{fig:realspace-res-small-E0}, as we would expect.

\begin{figure}[h]
\centering
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{small-E0-10d5-pump}
        \caption{}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{small-E0-10d6-pump}
        \caption{}
    \end{subfigure}
\caption{\textbf{\textbar} Time evolution for $E_0=0.01$, $\Delta_a=-100 $ and pump \textbf{(a)} $f_P=10^5$ and \textbf{(b)} $f_P=10^6$ at the same time in the time evolution ($\sim 8$ms). The blue curves are the modulus squared of the atomic field $\abs{\psi(r)}^2$ and the red are the modulus squared of the light field $\abs{\alpha(r)}^2$. The opposite detuning is so similar we omit it for tidyness.}
\label{fig:realspace-res-small-E0}
\end{figure}

There seems to be a `sweet spot' for stronger behaviour in spanning $\Delta_a$ from 1 to 1000 (log scale): $\Delta_a=1$ has weaker behaviour than $\Delta_a=10$ which has weaker behaviour than $\Delta_a=100$, but $\Delta_a=1000$ has slightly weaker behaviour than $\Delta_a=100$. This makes sense as the interaction depends on  $\Delta/(Delta^2+\kappa^2)^2$; this just goes as $\Delta_a$ at $\Delta_a \ll \kappa$, and as $1/\Delta_a^2$ at $\Delta_a \gg \kappa$, and has some maximal value around  $\Delta_a \sim \kappa$. However, the behaviour is very complex and, as we will see, very parameter dependent, so it is hard to state this with certainty without further investigation beyong the log scale of $1,10,100,1000$.

\subsubsection{Dependence on $\sigma_A$ and $\sigma_P$}

Similar complex behaviour is present for different sizes of $\sigma_A$ and $\sigma_P$, Fig. \ref{fig:realspace-res-sigma}. As there is no explicit lengthscale in the problem, particularly if we turn off the external trap, we might expect $\sigma_A=\sigma_P=1$ to behave similarly to $\sigma_A=\sigma_P=2$ as the ratio is the same, but this barely shows any effect of the interaction (the same is true for $\sigma_A=\sigma_P=0.5$). However, all units of length are made dimensionless with the beamwidth, so there is a lengthscale baked into the problem, which explains why this intuition doesn't hold.

%%% This picture should be a collage of sigma values
\begin{figure}[h]
\centering
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{size-compare-1-1}
        \caption{}
    \end{subfigure}
        \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{size-compare-4-2}
        \caption{}
    \end{subfigure}
        \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{size-compare-2-4}
        \caption{}
    \end{subfigure}
\caption{\textbf{\textbar} Snapshot of time evolution for $f_P=10000, \Delta_a=\pm 100$, for varying cloud and pump sizes: \textbf{(a)} $\sigma_a=\sigma_P=1$, \textbf{(b)} $\sigma_a=4, \sigma_P=2$ and \textbf{(c)} $\sigma_a=2, \sigma_P=4$.  The blue curves are the modulus squared of the atomic field $\abs{\psi(r)}^2$ and the red are the modulus squared of the light field $\abs{\alpha(r)}^2$.}
\label{fig:realspace-res-sigma}
\end{figure}

\subsection{Weak coupling code}

We want to begin moving down the list of assumptions now, to see if we cannot find the culprit for our analytic understanding breaking down.

As we've discussed above, the back-of-the-envelope calculation about energy scales suggests that in the weak coupling approximation, the energy scale of the pump $f_P$ should be of order 100 if $E_0$; while the weak coupling criterion of a small interaction strength is not met for our parameters, the model is built to satisfy the approximation, so we can expect the reasoning to apply.

Indeed, we find that at $f_P sim 4$ we start seeing signs of similar behaviour to the realspace code, and at $f_P=100$, we have a similar complex behaviour in both signs of the detuning, Fig. \ref{fig:weak-cpl-res-1}, as we did in the realspace 1D case; we still do not see only one sign of detuning going unstable. In this picture we can see more strongly the detuning difference due to the effective contact interaction $U^{\delta}$ -- this will yield an attractive interaction for negative $\Delta_0$, i.e. overall negative sign of the contact interaction, and repulsive interaction for positive $\Delta_0$. This is resposible for the spreading out, or lack thereof, of the atomic cloud; however, the complex behaviour is still qualitatively present in both detunings, as we can see from the real part of the atomic field (teal curves).

Note that this step is responsible for both assumption (1) and (2), i.e. we are now working with an infinitely large pump, and are using the weak coupling equation of motion Eq. \ref{eq:weak-cpl-eom}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_cloud-2_pump-2}
\caption{\textbf{\textbar} Weak coupling time evolution for $f_P=100, \Delta_a=\pm 100$.  The blue curves are the modulus squared of the atomic field $\abs{\psi(r)}^2$ and the teal are the real part of the light field $\abs{\alpha(r)}^2$.}
\label{fig:weak-cpl-res-1}
\end{figure}

\subsubsection{Removing external and effective trap}

We can deal with the assumptions that there is no external or effective trap, assumption (3), straightforwardly. As explained in Section \ref{mm-numerics}, in coding the weak coupling code we can simply turn off the effective potential Eq. \ref{eq:Veff}; turning off the external trap is trivial. We refer to Appendix \ref{appendix-mm-extra} for the graphs. Qualitatively, these do not show any difference, although quantitatively the two potentials have effects, i.e. they are actually modifying the system, just not changing the dynamics. 

We also are beginning to notice how susceptible the sharp peaks of the distribution are to changes; these aren't numerical artefacts, but numerical noise and periodic boundary conditions of the code will add to the variability of these peaks.

We proceed with both external and effective potentials turned off, and descend further down our list.

\subsubsection{Removing mirror-image effects}

Similarly, turning off the mirror image terms is straightforward. We can see that as expected, the distributions are not symmetric anymore, but it still displays the same qualitative behaviour.

The complex behaviour makes it difficult to judge whether the `lopsidedness' is the right way around, and the non-symmetric behaviour is entirely in the interaction, so it sets in at the same time as the messy peaks. Again, we refer to Appendix \ref{appendix-mm-extra} for the figure.

We keep the mirror contribution off in going forward, so we are working in the regime where assumptions (1) through (4) apply.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Uniform atomic field}

We can attempt to emulate the assumption of a uniform density in a few ways: a very wide Gaussian, a flat distribution, and a flat distribution with small perturbations, e.g. flat plus a small cosine term $1+0.1 \cos(x)$ (such that we can think about the wavevectors of the distribution in $k$-space).

We need to be careful with all of these, as the boundary conditions will matter now that we have distributions extending past the boundary, especially for the large Gaussian, and the cosine perturbation to the flat distribution which will not have the same phase at the boundaries.

\begin{figure}[h]
\centering
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_NO-POT_NO-VEFF_NO-MIRROR_cloud-48_pump-2}
        \caption{}
    \end{subfigure}
        \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_NO-POT_NO-VEFF_NO-MIRROR_FLAT_cloud-2_pump-2}
        \caption{}
    \end{subfigure}
        \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_NO-POT_NO-VEFF_NO-MIRROR_FLAT_cloud-2_pump-2_SIN}
        \caption{}
    \end{subfigure}
\caption{\textbf{\textbar} Snapshot of weak coupling time evolution for $f_P=100, \Delta_a=\pm 100$, with all previous approximations and trying out uniform distribution.}
\label{fig:realspace-res-uniform-distr}
\end{figure}


Fig. \ref{fig:realspace-res-uniform-distr} shows that all of these go unstable for both detunings. As expected, these distributions pick out fewer peaks as they have fewer Fourier components. 

We show the snapshots of the time evolution in Appendix \ref{sec:mm-extra-densities}.

\subsubsection{Long and short range interactions}

We can see in Fig. \ref{fig:realspace-res-long-short} that the short-range interaction is responsible for most of the complex behaviour, where the peaks which develop (especially in the repulsive interaction case) are extremely sensitive to parameters. The long-range distribution, however, also develops instabilities for both signs.

\begin{figure}[h]
\centering
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_NO-POT_NO-VEFF_SHORTRANGE_NO-MIRROR_cloud-2_pump-2}
        \caption{}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{compare_WEAK-CPL_fP=100_modDelta=100_tmax=20_zA=0_epsilon=0_1D_X_NO-POT_NO-VEFF_LONGRANGE_NO-MIRROR_cloud-2_pump-2}
        \caption{}
    \end{subfigure}
\caption{\textbf{\textbar} Time evolution for $\Delta_a=\pm 100 $ and pump $f_P=100$  at the same time in the time evolution ($\sim 8$ms) for \textbf{(a)} the short-range interactions only and (b) the long-range interactions only. The blue curves are the modulus squared of the atomic field $\abs{\psi(r)}^2$ and the red are the modulus squared of the light field $\abs{\alpha(r)}^2$.}
\label{fig:realspace-res-long-short}
\end{figure}

Using a flat distribution has no intuitive behaviour for the long range behaviour, and in the short-range case simply stays flat, which we expect, as a flat distribution just yields a constant for the self interaction. Using a flat distribution with a small periodic perturbation also gives no further insight.

\subsubsection{Attempting to match weak coupling linear stability conditions}

We can go back to the range where we believed the weak coupling approximation $E_0=0.01$, as a last ditch effort to find a regime where our understanding from the analytics holds.

We run this for a wide Gaussian, $f_P=10^6$ as discussed above, no mirror, effective or external potential, in the weak coupling code; this matches assumptions (1) through (5). We still have instability and complex behaviour for both detunings.

\subsubsection{Behaviour out of centre of the cavity and out of confocality}

Varying the $z$-position of the atoms confirms what we suspected, that the sharp peaks are very sensitive to parameters and show no clear dependence on the Gouy phase value (which is what encodes $z$-position).

This is true for moving out of confocality by changing the small parameter $\epsilon$.

\subsection{Conclusion}

Having eliminated assumption (1) through (5), we conclude that assumption number six does not hold. There is never a point where the complex behaviour goes away. The linear stability is not justified, and linearising in the perturbations leads to false conclusions when applied to the code, even when it is the weak coupling code, with every other assumption enforced. 

The sensitivity of the behaviour show to parameters in the model (and numerical artefacts were they to crop up more strongly elsewhere in parameter space) make it difficult to characterise the behaviour further, and while a few more things could be gleamed from repeated application of the code, there is no understanding to guide us, and ultimately it would be futile.  Given this, there is no point trying to use the 2D realspace code, or investigating the parameter space, or varying the values of $\epsilon$ or $z_R$, i.e. investigate the behaviour off-centre and off-confocality.

The behaviour observed is otherwise hard to say anything quantitative about; we conclude that all we can see is some complex behaviour which is strongly sensitive to parameters, and that there is nothing of interest in terms of ordered behaviour in this setup.

