The code to produce the following results is available at: 

We have now developed a handful of models, general and simplified, that can be coded to time-evolve and analyse results. We code all of these in XMDS2, a differential equation solver developed "to simplify the process of creating simulations that solve systems of initial-value first-order partial and ordinary differential equations" \cite{Dennis2013}. The boundary conditions of XMDS2 are periodic.

Let us briefly break down what we have available:

\begin{itemize}

\item \texttt{realspace.xmds} -- A two-dimensional real space model of coupled equations, Eq. \ref{eq:realspacelight} and \ref{eq:realspaceatoms}, in $\vr=(x,y)$.

This take a fair amount of time to run, so to explore parameter space we code a 1D version of the code, which should be sufficient to see qualitative instability, if there is any. \wip{stronger argument? The Ds are not separable}

\item \texttt{realspace-1D.xmds} -- A Cartesian one-dimensional version of the full code, i.e. on the $y=0$ axis. In the analysis of results we will refer to this as the 1D code, or the realspace 1D code. 

In principle, coding the 1D in polar coordinates $(r,\varphi)$, with Laguerre-Gauss modes and using the Bessel space capability of XMDS2, would lead to a more natural and elegant effective one dimensional model -- where what we assume is rotational symmetry, which is a precise physical assumption, instead of a less well-defined 'flattening' or just throwing away a dimension.

However, coding the model in Bessel geometry is much noisier, and subject to less intuitive boundary conditions; a flat distribution with no harmonic trap develops a peak at $r=0$ even when the pump, the only explicitly position-dependent term left, is off-centre. We therefore stick to Cartesian one-dimensional models to avoid these artefacts of numerics in our results.

The Bessel code is available in the \texttt{previous-versions} folder on the repository.

\item \textbf{\texttt{weak-coupling-realspace.xmds}} -- The weak coupling model, Eq. \ref{eq:weak-cpl-eom}, in Cartesian 1D. The analytic form of the irregular interaction term Eq. \ref{eq:irregular-integral} is coded, to get around the numerical intractability of the delta function. In the analysis of results we will refer to this as the weak couping code, or the 1D weak coupling code.
\end{itemize}

We discuss the numerical parameters below; both of the 1D models we will use as the bread and butter of our numerical investigation also have control parameters to turn features of the model on and off. These are passed at runtime from shell script which also handle results organisation and management.

We can: 

\begin{itemize}
\item[$-$] turn light-matter interactions on and off to sanity check the baseline code behaviour; 

\item[$-$] change from a Gaussian to a flat distribution (any other distribution would have to be recompiled);

\item[$-$] turn the external trap on or off;

\item[$-$] turn the mirror image effect on or off -- that is, in the 1D code, we de-symmetrise the kernels in Eq. \ref{eq:realspacelight} and \ref{eq:realspaceatoms} such that $D(r,r',\xi)=K(x,x',\xi)K(y,y',\xi)$ (i.e. in 1D using only $K(x,x',\xi)$), and in the weak coupling code we discard the $\abs{\psi(-\vr)}^2$ term in the irregular part, as well as desymmetrising the kernels in the regular one;

\item[$-$] in the weak coupling code, turn the short-range interaction $U^{\delta}$ and long-range interaction $U^{reg}$ on or off separately;

\item[$-$] in both codes, `cancel out' the extra contribution to the effective potential described in Section \ref{weak-coupling-veff} -- this means simply not adding the extra contibution Eq. \ref{eq:Veff} to the external potential in the weak coupling code, and adding a term $+ \frac{E_0}{2(\Delta_0^2+\kappa^2)} \abs{F(\vect{r},\theta_a)}^2$ to the realspace code to cancel the contribution (i.e. imagining that we have a second tailored potential making up for this contribution).
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Parameter values}

We use parameter values to match the experimental setup and results covered in Section \ref{confocal-lit}.

The confocal cavity length is $\approx 1$cm, so $z_R=5000 \mathrm{\mu m}$. From Eq. \ref{eq:rayleigh-range}, we have:

\begin{equation}
w_0=\sqrt{\frac{\lambda_0 z_R}{ \pi}}
\end{equation}

Using $\lambda_0=780 \textrm{nm}$ and $z_R=5000 \mu \mathrm{m}$\footnote{Note that as $z$ and $z_R$ only appear as a ratio in the Gouy phase, the units of these do not matter, but we present them in $\mu$m to be consistent with other lengthscales.}, we get

\begin{equation}
w_0 \approx 35 \mathrm{\mu m} 
\end{equation}

and the value of beam width $w_a$ at $z \ne 0$ is given by Eq. \ref{eq:beam-waist-def}.

Which is consistent with Section \ref{confocal-lit}. The atomic distribution size $\sigma_A$, and pump spot size $\sigma_P$ at the mirror are given in units of $\frac{w_a}{\sqrt{2}}$. 

We begin at confocality parameter $\epsilon=0$, i.e. in the perfectly confocal regime.

The frequency quantities in kHz are $E_0=50$ and $\kappa=1000$. The variables $\Delta_0$ and $f_P$ are also measured in kHz, as well as the confocality parameter $\epsilon$. This yields time measurements in milliseconds.

The number of atoms is $N=10^5$.

\subsubsection{Relationship between external trap frequency $\omega_T$ and $\eta$}

There is a subtlety in defining $\omega_T$ and $\eta=\frac{\sqrt{2} l_{HO}}{w_a}$ (defined in Section \ref{mm-model}). 
We might be tempted to set $\omega_T=1$, which is a reasonable trap frequency, and $\nu=1$ for convenience. However, $\nu$ contains a dependence on oscillator length, so the two are not independent.

Let us call the harmonic trap length that does match the $\eta=1$ criterion $\tilde{l}_{HO}\approx 25 \mu$m -- this is the oscillator length which matches the variance of the beamwaist.

Using $l=\sqrt{\hbar / m_a \omega}$ and $m_a=1.42\cdot 10^{-25} \text{kg}$ for rubidium atoms, we can get the frequency of the harmonic trap needed:

\begin{equation}
\tilde{\omega}_T=\sqrt{\frac{\hbar}{m_a \tilde{l}_{HO}^2}}=1.09 \text{Hz}
\end{equation}

i.e. $\eta=1$ actually corresponds to a very shallow trap. We can write a generic oscillator length $\lho$ in terms of the `normalised' oscillator length $\tilde{l}_{HO}$ and frequency $\tilde{\omega}_T$ for which $\eta=1$:

\begin{equation}
\lho=
\sqrt{\frac{\hbar}{m_a \tilde{\omega}_T}}
\sqrt{\frac{\tilde{\omega}_T}{\omega_T}}
=\tilde{l}_{HO} \sqrt{\frac{\tilde{\omega}_T}{\omega_T}}
\end{equation}

then a general $\eta$, using $\frac{\tilde{l}_{HO}}{w_a / \sqrt{2}}=1 $ by definition:

\begin{equation}
\eta= \sqrt{\frac{\tilde{\omega}_T}{\omega_T}}
\end{equation}

Since $\tilde{\omega}_T \approx 1$Hz, we get a simple dependence:

\begin{equation}
\eta=\frac{1}{\sqrt{\omega_T}}
\end{equation}

with $\omega_T$ measured in Hz, or $\eta=10^{-1.5}/\sqrt{\omega_T}$ in kHz as in the code, i.e. the $\eta$ corresponding to $\omega_T=1$kHz is $\eta=0.0316$, the oscillator length is a few orders of magnitude smaller than the beamwidth.

However, numerics are bad at calculating things that are very big or very small, and we have kinetic energy going as $\eta^2$ while the potential term goes as $1/\eta^2$. This is bound to cause trouble for $\eta \sim 10^{-2}$.

We can enforce $\eta=1$, then point out that either a much weaker trap $\omega_T$ (as above) is required, or a smaller beamwaist (smaller cavity) is required for $\omega_T=1$. Both of these are difficult in practice, but starting from theoretically simple parameters makes sense in order to begin piecing together a story.

We summarise the numerical value of parameters in Table \ref{tab:mm-num-vals}.

%%%TABLE OF PARAMETERS%%%

\begin{table}[t]
\centering
    \begin{tabular}{ | c | c | }
    \hline
    \textbf{Parameter} & \textbf{Value} \\ \hline
    $w_0$ & 35 $\mu$m \\	
    $z_R$ & 5 mm \\
    N & $10^5$ \\	
  	$\kappa$ & 1 MHz \\
  	$E_0$ & 50 kHz \\
	$\omega_T$ & 1 kHz \\	
   	$\eta$ & 1 \\	
    \hline
\end{tabular}
\caption{Summary of numerical values of variables in the code. Units are absent unless specified.}
\label{tab:mm-num-vals}
\end{table}