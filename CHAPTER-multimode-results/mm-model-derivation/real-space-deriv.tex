We now have equations capturing the subtleties of our setup; we should commit to working in either real space, or mode space, as currently, the equation of motion for atoms is in real space, while that of the light field is in mode space, and the interaction term in both contains mode components from the light field.

Appendix \ref{sec:mm-modes-appendix} discusses the mode space approach and why it is not productive; for the rest of these results, we commit to the real space basis, and to a Cartesian set of coordinates. This means we will write the light field equation in real space, and use Hermite-Gauss modes from here on.

A further step we can take in the derivation to make matters easier is to make our equations dimensionless. A good choice of unit length is the variance of the beam width at the $z$-position of the atomic pancake, $w_a / \sqrt{2}$, where $w_a$ is $w(z_{atoms})$, as this simplifies all the beamwaist factors in the quantities introduced in Section \ref{mm-initial-eoms}.

We will carry out both processes at once. We begin by redefining dimensionless atomic and light field, and associated quantities:

\begin{equation}
\label{eq:fullatomicfield}
\psi(\textbf{r})= \sum_{n,m} \psi_{nm} \  \chi_{n}(x) \chi_m (y)
\end{equation}

and

\begin{equation}
  u_{n,m,q}(x,y,z) = 
  \chi_n (x) \chi_m(y)
  \cos \left[ f_P (x,y,z) - \theta_0 (n+m) \right]
\end{equation}

where $\theta_a$ is the modified Gouy phase $\theta(z)=\psi(z)+\pi/4$ evaluated at the $z$-position of the atoms, and the $f_P$ term contains all explicitly z-dependent terms which we can integrate as in Sec. \ref{sec:zoverlaps}.

Similarly, we can define our real space light field as:

\begin{equation}
\label{eq:fulllightfieldR}
\alpha(\textbf{r})= \sum_{n,m} \alpha_{nm} \  \chi_{n}(x) \chi_m (y)
\end{equation}

The $\chi$ terms are:

\begin{equation}
\label{eq:chi-dimless}
  \chi_n(x) = \frac{1}{\sqrt[4]{\pi}}
  \frac{1}{\sqrt{2^n n!}} H_n(x) \exp\left(-\frac{x^2}{2} \right)
\end{equation}

where the $H_n$ are Hermite polynomials. We should take a moment to check the normalisation and completeness of these $\chi_n$, as we will need to make use of it multiple times in the following, and to make statements about the normalisation of our fields.

\subsubsection{Orthogonality and completeness of $\xi_n(x)$}

The Hermite-Gauss polynomials form a complete basis, and are orthogonal with respect to a weight function $e^{x^2}$, such that:

\begin{equation}
\int_{- \infty}^{\infty} dx H_n(x) H_m(x) e^{-x^2}=\sqrt{\pi} 2^n n! \delta_{nm}
\end{equation}

If we then integrate $\int dx \chi_n \left(x\right) \chi_m \left(x\right)$, and substitute in the definition Eq. \ref{eq:chi-dimless}, we can convince ourselves that we get:

\begin{equation}
\label{eq:HGorthonormality}
\int dx \ \chi_n (x) \chi_m (x) = \ \delta_{nm}
\end{equation}

i.e. the $\chi_n(x)$ are orthonormal, and therefore both $\alpha(x,y)$ and the $u_{nm}$ are normalised.

We also want to show a completeness relation. Inheriting the fact that Hermite-Gauss polynomials form a complete basis, we can use the $\chi$ (which are just weighted HG polynomials) to write any function $f(x)$  such that:

\begin{equation}
f(x) = \sum_n A_n \chi_n(x)
\end{equation}

If we then use the orthonormality of the $\xi$, we can write:

\begin{equation}
\int dx f(x) \chi_m(x) = \sum_n A_n \delta_{nm} = A_m
\end{equation}

i.e. we can extract the eigenvalues by $A_n=\int dx f(x) \chi_m(x)$. If we substitute this back into the definition of $f(x)$ above, we get:

\begin{align}
f(x) &= \sum_n \int dx' f(x') \chi_n(x') \chi_n(x) \\&= \sum_n \int dx' f(x') \chi_n(x') \chi_n(x)
\end{align}

which means that for this to be true, it must follow that $\sum_n \chi_n(x) \chi_n(x') = \delta(x-x')$, so our completeness relation is:

\begin{equation}
\label{eq:HGcompleteness}
\sum_n \ \chi_n (x) \chi_n (x') = \ \delta(x-x')
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Unitless GPE}

We can rewrite the first two terms of Eq. \ref{eq:eqnpsifirst} in terms of dimensionless units of length, introducing $\eta=\frac{\sqrt{2} \lho}{w_a}$; the $\nabla^2$ term will yield $\left(\frac{\sqrt{2}}{w_a}\right) ^2 \tilde{\nabla}^2$ and the $\textbf{r}^2$ term will yield $\left(\frac{w_a}{\sqrt{2}}\right) ^2 \tilde{\textbf{r}}^2$, where the tilde indicates new dimensionless units.
Therefore, we have:

\begin{equation}
\label{eq:oscillatorpartPsiRealSpace}
- \frac{\hbar \omega_T}{2} 
 \left( \eta^2 \nabla^2-\frac{\textbf{r}^2}{\eta^2}\right) \psi(\textbf{r}) \end{equation}

where $\lho$ is the harmonic oscillator length for the external potential, $\lho=\sqrt{\frac{\hbar}{m_a \omega_T}}$.

We can now divide by $\hbar$ and redefine interaction strength $E_0$ in units of frequency: $\mathcal{E}_0 $ was in terms of frequency times area, but we now have an $E_0$ in terms of frequency only due to moving to an unitless problem, where effectively we have defined $E_0= \mathcal{E}_0 \left( \frac{\sqrt{2}}{w_a}\right)^2$.

Rewriting the interaction term in the GPE into real-space form, because at the moment it is written in terms of the three-dimensional light field, and doing the z-integral as in Sec. \ref{sec:zoverlaps} we get:

\begin{multline}
i\partial_t \psi(\textbf{r})= ... - E_0 
\sum_{n,n,m',m'} \alpha_{nm}^* \alpha_{n'm'}
 \chi_n (x) \chi_m (y) \\ \chi_{n'} (x) \chi_{m'} (y)
 \frac{1}{4}  \left(e^{i\theta_0(n+m)}e^{-i\theta_0(n'+m')}+h.c. \right)
\psi(\vect{r})
\end{multline}

Inserting the identities $\int d\textbf{r}' \chi_{nm} (\textbf{r}') \chi_{n'm'} (\textbf{r}')=\delta_{n,m}$ and $\int d\textbf{r}'' \chi_{pq} (\textbf{r}'') \chi_{p'q'} (\textbf{r}'')=\delta_{n,m}$ from Eq. \ref{eq:HGorthonormality} (where we use the shorthand $\chi_{nm} (\textbf{r})$ for $\chi_n(x) \chi_n(y)$),  to detangle the $\alpha_{n,m}$ terms from the exponential terms:

\begin{multline}
\label{eq:eqnpsi}
i\partial_t \psi(\textbf{r})= ... - \frac{E_0}{4} \int d\textbf{r}' \int d\textbf{r}''
\sum_{n,n,m',m',p,q,p',q'} \alpha_{nm}^* \alpha_{n'm'} \\
 \chi_{nm} (\textbf{r}') \chi_{pq} (\textbf{r}) \chi_{pq} (\textbf{r}') \chi_{n'm'} (\textbf{r}'') \chi_{p'q'} (\textbf{r}) \chi_{p'q'} (\textbf{r}'') \\
  \left(e^{i\theta_0(p+q+1)}e^{-i\theta_0(p'+q'+1)}+h.c. \right)
\psi(\vect{r})
\end{multline}

Some of the $\chi$ will be absorbed in the definition of $\alpha(r)$ from Eq.\ref{eq:fulllightfieldR}; we also define a function $D(\vr,\vr',\xi)$ which we go into some detail below so as to not break up the logical flow of this derivation.

In terms of these we write the last term of the Gross-Pitaevskii equation entirely in real space as:

\begin{multline}
i\partial_t \psi(\textbf{r})= ... - \frac{E_0}{4} \int d\textbf{r}' \int d\textbf{r}'' \bigg[
D(\textbf{r},\textbf{r}',\theta_0) \alpha^*(\textbf{r}') \alpha(\textbf{r}'')  D(\textbf{r}, \textbf{r}'',-\theta_0)
+ \\
D(\textbf{r},\textbf{r}',-\theta_0) \alpha^*(\textbf{r}') \alpha(\textbf{r}'')  D(\textbf{r}, \textbf{r}'',\theta_0) \bigg] \psi(\vect{r})
\end{multline}


\subsubsection{Integral kernels}

The last step involved definiting a function of the form:

\begin{equation}
\label{eq:kernInChis}
K(x,x',\xi)=\sum_{n} \chi_n (x) \chi_n (x') e^{-i\xi n}
\end{equation}

Let us consider this for a moment. Substituting in the definitions of $\chi$:

\begin{equation}
K(x,x',\xi)=\sum_{n} 
\frac{1}{\sqrt{\pi}} \frac{1}{2^n n!} H_n(x) H_n(x') e^{-\frac{x^2-x'^2}{2}}
\end{equation}

by using the Mehler Kernel \cite{Mehler1866}:

\begin{equation}
\sum_{n}^{\infty} \frac{(\rho/2)^n}{n!} H_n(x) H_n(x') = \frac{1}{\sqrt{1-\rho^2}}
\exp{\left(-
\frac{\rho^2(x^2+x'^2)-2\rho x x'}{(1-\rho^2)}\right)
}
\end{equation}

we can rewrite explicitly in real space:

\begin{equation}
\label{eq:integralkernelK}
K(x,x',\xi)=\sqrt{\frac{e^{i \xi}}{2 i \pi \sin(\xi)}} \exp{\left[ 
\frac{i }{2 } \left(
\frac{x^2+x'^2}{\tan(\xi)}-\frac{2 x x'}{\sin(\xi)}
\right) \right] }
\end{equation}

This makes a lot of sense within our setup: this is the harmonic oscillator's Green's Function (or propagator) which encodes propagating the light backwards or forwards by a certain amount of reflections given by the $\xi$.
\wip{The wikipedia for mehler kernel is really good -- should I just use citations therein? 2/3 esp}

At this point we should stop to reiterate that we are only working with even modes i.e. for which $(n+m)$ is even; since we've lost the explicit sum to enforce this, we need to insert a similar symmetrising effect in these kernels.
The full kernel should read:

\begin{equation}
D(\textbf{r},\textbf{r}',\xi)= \sum_{n,m} \frac{1}{2} (1+(-1)^{n+m}) \chi_n (x) \chi_n (x') \chi_m (y) \chi_m (y') e^{i\xi(n+m+1)}
\end{equation}

The $K$ we have above yields the term corresponding to the 1 in the round bracket; we also need to build terms corresponding to the Mehler kernels computed with $(-1)^n$ in the definition, which yield a sign change in the $xx'$ term, i.e.:

\begin{multline}
\sum_{n} (-1)^n \chi_n (x) \chi_n (x') e^{i\xi(n+1/2)}= \\\sqrt{\frac{e^{i\xi}}{2 i \pi \sin(\xi)}} \exp{\left[ \frac{i }{2 } \left(
\frac{x^2+x'^2}{\tan(\xi)}+\frac{2 x x'}{\sin(\xi)}  
\right) \right] } = K(x,-x',\xi)
\end{multline}

So the kernel with the correct parity will read:

\begin{equation}
\label{eq:integralkernelD}
D(\textbf{r},\textbf{r}',\xi)=\frac{1}{2} \big[ K(x,x',\xi)K(y,y',\xi)+ K(x,-x',\xi)K(y,-y',\xi) \big]
\end{equation}

This also makes sense, as it encodes mathematically the fact that, for even order modes, we have mirror image symmetry.

From completeness relation Eq. \ref{eq:HGcompleteness}, we expect $K(x,x',\xi\rightarrow 0)=\delta(x-x')$; we can check that this is true as:

\begin{equation}
K(x,x',\xi\rightarrow 0)=\frac{1}{\sqrt{2i\pi \xi}} e^{-\frac{1}{2 i \xi}(x-x')^2}
\end{equation}

If $x \ne x'$, the exponential will go to zero much faster than the root, and this will be zero; integrating over this to check the normalisation of the delta, the factors from the exponent exactly cancel the prefactors, and the completeness relation holds.

\subsubsection{Real space, unitless light field equation}

The mode frequency term in real space will be a harmonic oscillator term:

\begin{equation}
-\frac{\nabla^2}{2m_{\alpha}}+ \frac{m_{\alpha} \omega_{\alpha}^2}{2} \vect{r}^2
\end{equation}

From Sec. \ref{sec:modefrequency}, the detuning $\Delta_{nm}=\omega_{pump}-\omega_{nm}$ was defined as $\Delta_{nm}=\Delta_0+(2n+m)\epsilon$, where $\epsilon$ is a `confocality parameter', $\epsilon= \frac{c}{z_M}\left(\psi(z_M)-\frac{\pi}{4}\right)$; the $\Delta_0$ part is independent of n and m, i.e. independent of the basis we're writing things in, therefore simply carries across, whereas the $\epsilon$ acts as our oscillator frequency $\omega_{\alpha}$, so that if the cavity is perfectly confocal the atoms are unbounded by this part of the light field due to the infinite modes available.

The oscillator length, in this case our beam waist at the atomic position $ w_a / \sqrt{2}$, relates to the oscillator quantities:

\begin{equation}
\frac{w_a}{\sqrt{2}}=\sqrt{\frac{1}{m_{\alpha} \epsilon}} \quad \therefore \quad m_{\alpha}=\frac{2}{\epsilon w_a^2}
\end{equation}

In terms of unitless quantities, then, we have:

\begin{equation}
-\left[ \frac{\epsilon}{2}  \left( \nabla^2 - \textbf{r}^2 \right) + \deltaplusikappa \right] \alpha(\textbf{r})
\end{equation}

For the pump term, we straight-forwardly use the Gaussian pump definition we introduced above.

The last term then reads, remembering to multiply by $\sum_{nm} \chi_{nm}(r)$ to turn $\alpha_{nm}$ into $\alpha(r)$, and doing the z-integral:

\begin{multline}
i\partial_t \alpha(\textbf{r})= ... - \frac{N E_0}{4}
\sum_{n,n,m',m'} \int d\textbf{r}' |\psi(\textbf{r}')|^2 \chi_{nm} (\textbf{r}) \\
\chi_{nm} (\textbf{r}') \chi_{n'm'}(\textbf{r}')  
\left(e^{i\theta_0(n+m)}e^{-i\theta_0(n'+m')}+h.c. \right) \alpha_{n'm'}
\end{multline}

As above, we introduce the orthonormal expression $\int d\textbf{r}'' \chi_{n'm'}(\textbf{r}'') \chi_{ab}(\textbf{r}'')$ so that we can untangle the indeces in the alpha from the indeces in the exponential:

\begin{multline}
i\partial_t \alpha(\textbf{r})= ... - \frac{N E_0}{4}
\sum_{n,n,m',m',p,q} \int d\textbf{r}' |\psi(\textbf{r}')|^2 \chi_{nm} (\textbf{r}) \\
\chi_{nm} (\textbf{r}') \chi_{n'm'}(\textbf{r}')  \int d\textbf{r}'' \chi_{n'm'}(\textbf{r}'') \chi_{pq} (\textbf{r}'') \alpha_{pq} \\
\left(e^{i\theta_0(n+m)}e^{-i\theta_0(n'+m')}+h.c. \right) 
\end{multline}

Then substituting in the form of $\alpha(r)$ from Eq. \ref{eq:fulllightfieldR} and the integral kernel from \ref{eq:kernInChis}:

\begin{multline}
i\partial_t \alpha(\textbf{r})= ... - \frac{N E_0}{4}
\int d\textbf{r}' \int d\textbf{r}''
\bigg[
D(\textbf{r},\textbf{r}',\theta_0) |\psi(\textbf{r}')|^2 D(\textbf{r}',\textbf{r}'',-\theta_0) +\\
D(\textbf{r},\textbf{r}',-\theta_0)  |\psi(\textbf{r}')|^2 D(\textbf{r}',\textbf{r}'',\theta_0)
\bigg]
\alpha(\textbf{r}'')
\end{multline}

\subsection{Periodicity of the equations of motion}

Given that we are in a confocal or near-confocal cavity, we'd normally expect the equations of motion to have a $2 \pi$ periodicity (four round trips to come back to itself, which makes sense given $0<\theta_0<\pi/2$); however, given that we take only even order modes which have, as observed above, mirror image symmetry in the transverse plane, the effective symmetry of the equations should be $\pi$ s.t. $\pi-\theta=\theta$.

The parameter that governs the phase accumulation (and therefore the z-position dependent term in the now transversal EoMs) appears in the exponential term:

\begin{equation}
e^{i \theta_0 (n+m-n'-m')}+e^{-i \theta_0 (n+m-n'-m')}
\end{equation}

which is periodic in $\pi$, just as the combination of two integral kernels that include this, whereas a single integral kernel does not have this periodicity.

This makes sense -- consider the (quantum) harmonic oscillator $e^{i\omega(n+1/2)}$ with $n=2N$; this yields a part which is periodic in $\pi$ and a part that isn't due to the $1/2$ factor (zero point energy). This is just a phase factor so it doesn't influence the observable behaviour, but it does mean that technically the wavefunction only comes back to itself exactly afer $2 \pi$. The same thing is happening in the integral kernels, which makes sense as they are the harmonic oscillator Green's functions.
The kernels never appear on their own in the equations of motion.

\subsection{Dimensionless equations of motion in real space}

Our final equations of motion for the atomic field $\psi(\vect{r})$ and the light field $\alpha(\vect{r})$, in real space, and made unitless in the beam waist variance $w_a/\sqrt{2}$, read:

\begin{multline}
\label{eq:realspaceatoms}
i\partial_t \psi(\textbf{r})= - \frac{\omega_T}{2} 
 \left( \eta^2 \nabla^2-\frac{\textbf{r}^2}{\eta^2}\right) \psi(\textbf{r}) \\ - \frac{E_0}{4}  \bigg[ \abs*{ \int d\textbf{r}' D(\textbf{r}, \textbf{r}',\theta_0) \alpha(\textbf{r}')} ^2+\abs*{\int d\textbf{r}' D(\textbf{r},\textbf{r}',-\theta_0) \alpha(\textbf{r}')}^2 \bigg] \psi(\vect{r})
\end{multline}

and 

\begin{multline}
\label{eq:realspacelight}
i\partial_t \alpha(\textbf{r})=  \left[ -\frac{\epsilon}{2} \left( \nabla^2 - \textbf{r}^2 \right) - \deltaplusikappa \right] \alpha(\textbf{r}) + f_p(\textbf{r})- \\ \frac{N E_0}{4}
 \int d\textbf{r}'\int d\textbf{r}'' \bigg[ D(\textbf{r},\textbf{r}'',\theta_0) |\psi(\textbf{r}'')|^2  D(\textbf{r}',\textbf{r}'',-\theta_0)  + h.c.\bigg]\alpha(\textbf{r}')
\end{multline}

where the kernels $D(r,r',\xi)$ are Eq. \ref{eq:integralkernelD}, $\omega_T$ is the oscillator frequency of the transverse harmonic trap, $\nu$ is the ratio to the oscillator length to the beam waist variance, $E_0$ is strenght of the light shift due to atoms and vice-versa, $\eta$ is the near-confocality parameter, $\eta=0$ for perfectly confocal, $\kappa$ is the cavity loss rate, and the Gaussian pump is Eq. \ref{eq:gaussianpump}.

At this point, we could just time evolve these numerically; however, we can also treat them analytically with some assumptions, to guide our understanding and simplifications. Therefore, the following analysis aims to make things analytically tractable, beginning with the assumption that the fields are weakly coupled, and that the light field can be perturbatively eliminated. 

However, we want to stress again that these equations of motion are entirely numerically treatable: while in what follows we lose generality and gain analytical tractability with each assumption, we will return to these general equations once we have gained some insight.