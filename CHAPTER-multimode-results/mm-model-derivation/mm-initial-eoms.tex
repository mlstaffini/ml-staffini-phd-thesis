\subsubsection{Atomic field}

We begin by considering how to write an equation of motion for the atomic field. Through the derivations and results, we will define the $z$-axis as parallel to the cavity axis, and the $xy$ plane as transversal.

There are a few considerations that the atomic field needs to meet: the atoms are Bose-condensed; they are spatially confined in $xy$ by a harmonic trap; they have have a defined $z$ extension, and we can write the atomic field in a separable form:

\begin{equation}
\Psi(\vect{x},\vect{y},\vect{z})=\psi(\vect{x},\vect{y})Z(\vect{z})
\end{equation}

so that we may write and carry out $z$-dependences explicitly.

Given that we are trying to write an equation of motion for Bose-condensed atoms, we begin with a Gross-Pitaevskii equation \cite{Gross1961,Pitaevskii1961,Rogel-Salazar2013}; we write this for $\psi(\vect{x},\vect{y})$, or $\psi(\vect{r})$ here for short\footnote{The geometry of the $d^2\vect{r}$ integrals will of course depend on the choice of basis we make.}, a cross-section of the atoms at a position $z$.

The Gross-Pitaevskii equation is an extension of Schroedinger equation with an extra term accounting for interactions, i.e. it has a kinetic term, a potential term $V_{ext}$, and an interaction term.

We want to model the atoms trapped by a harmonic potential, so $V_{ext}=\frac{m_a \omega_T^2}{2} \vect{r}^2$.

If we define the light field $\varphi(\vect{r},\vect{z})$ (the structure of which we will discuss in a moment), we can write a generic interaction term between atoms and light as $H=...-E_0 \int d \mathrm{r} \int d \mathrm{z} \abs{\Psi (\vect{r},z)} \abs{\varphi(\vect{r},z)}$. 

When we go to calculate this term in the equation of motion as introduced in Section \ref{master-equations}:


\begin{equation}
i \partial_t \psi(r)=...+\left[ E_0 \int d \mathrm{r} \int d \mathrm{z} \abs{\Psi (\vect{r},z)},\psi \right]
\end{equation}

everything but the $\abs{\psi(\vect{r})}$ part of the interaction will commute; from this term we'll get a $\delta(r-r') \psi(\vect{r}')$, so we lose the $r$ integral and get, assembling the other terms as above:

\begin{equation}
\label{eq:eqnpsifirst}
i\hbar \partial_t \psi(\vect{r})= -\frac{\hbar^2 \nabla^2}{2m_a}\psi(\vect{r})+ \frac{m_a \omega_T^2}{2} \vect{r}^2 \psi(\vect{r}) - \mathcal{E}_0 
\int \, \mathrm{d}z |Z(z)|^2|\varphi(\vect{r},z)|^2 \psi(\vect{r})
\end{equation}

where $m_a$ is the mass of the atoms, $\omega_T$ is the oscillator trap frequency, $\mathcal{E}_0$ is the coupling strength (i.e. the energy shift due to the photon population). There is no contact interaction term $-UN \abs{\psi}^2$ as $UN$ is much smaller than all other quantities, \wip{not sure if necessary, but why true?}

The atomic field is normalised, $\int d^2 \vect{r} \vert\psi (\vect{r}) \vert^2=1$.

\subsubsection{Light field}

Before writing down an equation for the light field, we need to discuss the shape it is going to take in some detail, in the spirit of the discussion in Section \ref{multimode-optics}.

For the light field, it is more natural to work in mode space, as the degenerate set of modes supported by a confocal cavity can be written analytically with Hermite-Gauss or Laguerre-Gauss polynomials, and therefore we only need to time evolve the coefficients of each mode.

Let us write the light field as 

\begin{equation}
\label{eq:fulllightfield}
\varphi(x,y,z)= \sum_{n,m,q} \alpha_{n,m,q} u_{n,m,q}(\vect{r},z)
\end{equation}

We have introduced three-dimensional mode functions $u_{n,m,q}$ and their coefficients $\alpha_{n,m,q}$, where the $m$, $n$ label transverse mode structure and q the longitudinal one. Note that $q$ is not completely free: in the confocal and near-confocal case we consider, the index $q$ relates to the $q$ of the fundamental mode $q_0$, as we will see below. We will restrict ourselves to the even family of degenerate modes, as explained in Section \ref{multimode-optics}.

Let us show this, and some beam propagation subtleties, explicitly.

The form of the $u_{n,m}$ should encode the detail of mode structure; we follow Siegman's Lasers Ref. \cite{Siegman1986} to write a closed form for higher order Gaussian mode solutions supported by the cavity. We will break down the steps in logical components; for derivation we defer to the textbook.

We first consider the propagating beams in the cavity, forward and back. To begin, we need to write down the forward propagating beam, which will have the form covered in Section \ref{multimode-optics}; let us break this down further to understand the components of our model better. The first term in the exponent is the longitudinal phase, the second term is the Gouy phase term, the third term describes propagating surfaces of constant phase and the fourth term is the phase difference we need to match between the propagating and counter-propagating beams:

\begin{equation}
\exp(-ikz+i(2p+m+1)\psi(z)-i \frac{k \textbf{r}^2}{2 R(z)}+i \xi)
\end{equation}

where $R$ is the radius of curvature of the mirror (for spherical mirrors, $z+r^2/2R(z)$ is constant). The back-propagating beam is just the complex conjugate of this; this yields, so far:

\begin{multline}
u_{n,m,q}(r,z)= \chi_n(\textbf{x}) \chi_m(\textbf{y}) [ \exp(-ikz+i(2p+m+1)\psi(z)-i \frac{k \textbf{r}^2}{2 R(z)}+i \xi]+\\ \exp(ikz-i(n+m+1)\psi(z)+i \frac{k \textbf{r}^2}{2 R(z)}-i \xi)]
\end{multline}

We then need to do the phase matching at the mirrors and obtain $\xi$. At the right mirror, of which $z_2$ is the z-coordinate:

\begin{align}
&u_{n,m,q} \left(\textbf{r}, z=z_2-\frac{\textbf{r}^2}{2R(z_2)}\right)=0 \\
&\therefore \quad \exp(-i(kz_2-(2p+m+1)\psi(z_2)-\xi))+H.c.=0 \nonumber \\
&\therefore \quad \xi = \frac{\pi}{2}+(n+m+1)\psi(z_2)-kz_2
\end{align}

Similarly for the left mirror, with coordinate $z_1$ and negative curvature $R(z_1) < 0$:

\begin{equation}
\xi = \frac{\pi}{2}+ q\pi+(n+m+1)\psi(z_1)-kz_1
\end{equation}

where we have only left the $q \pi$ term (where $q$ is an integer number, simply encoding the fact that adding factors of $\pi$ does not alter the value of $\xi$) in one expression since we are only concerned with the difference between the two expressions, and therefore only need one of these degrees of freedom.

If we introduce the symmetric notation $z_2=z_0$, $z_1=-z_0$, and considering that $\psi(z_2)=-\psi(z_1)$, we have:

\begin{equation}
2\xi=\pi+q \pi \qquad \Rightarrow \qquad \xi= \bigg(\frac{1+q}{2} \bigg) \pi
\end{equation}

As we've convered in Section \ref{multimode-optics}, the confocal case, families of degenerate modes obey
$2q+n+m = 2 q_0$, where the label $q_0$ of the fundemental
mode is related to the wavevector by $\omega = c k = c\pi (2q_0 +1) / 4 z_0$ (this is simply Eq. \ref{eq:mode-frequency-families} written for the confocal cavity and somewhat tamed).  In this case, we may then label $\xi_{n,m,q} = \xi_{n,m,q_0} = \xi_{q_0} - (n+m)\pi/4$.  It is therefore convenient to write the mode as:

\begin{displaymath}
\label{eq:cos-mode-component}
  \cos\left[ k \left( z + \frac{r^2}{2R(z)} \right)
    + \xi_{q_0} - \psi(z)
    - (n+m) \left( \frac{\pi}{4} + \psi(z) \right)
  \right],
\end{displaymath}

which shows that the $n,m$ phase dependence involves the combination $\theta(z)=\psi(z) + \pi/4$, which goes between $0$ and $\pi/2$.

We encode the transversal structure with Hermite-Gauss polynomials as covered in Section \ref{multimode-optics}; the transverse structure should also include a Gaussian envelope. Let us define the function: 

\begin{equation}
\label{eq:chidef}
\chi_{n}(x)= \sqrt[4]{\frac{1}{\pi}} \sqrt{\frac{1}{2^n n!}} \ H_n(x) \ e^{-x^2/2}
\end{equation}

where $H_n$ is the $n^{th}$ Hermite polynomial. This function only contains one transverse direction and the corresponding quantised coefficient -- the $y$ direction will have a corresponding $\chi_m(y)$.

Accounting for the right units in the $\chi$ terms, and tidying up, the three-dimensional light modes $u_{nm}(x,y,z)$ are: 

\begin{multline}
u_{n,m,q}(x,y,z)= \frac{\sqrt{2}}{w_a} \ \chi_{n}^L  \left(\frac{\sqrt{2} x}{w_a}\right)
\chi_{m}^L  \left(\frac{\sqrt{2} y}{w_a}\right)
 \\ \cos \left[ kz+\frac{kr^2}{2R(z)}-(n+m+1)\psi(z)+\xi_q \right]
\end{multline}

where $w_a$ is the the beam width at the position of the atoms, $w_a=w(z_a)$.
This similar to the result we covered in Section \ref{multimode-optics}, plus a z-propagating term.

Using orthogonality of Hermite polynomials:

\begin{equation}
\int_{- \infty}^{\infty} dx H_n(x) H_m(x) e^{-x^2}=\sqrt{\pi} 2^n n! \delta_{nm}
\end{equation}

We get:

\begin{equation}
\int dx \ \chi_n \left(\frac{\sqrt{2}x}{w_a}\right) \chi_m \left(\frac{\sqrt{2}x}{w_a}\right) = \frac{w_a}{\sqrt{2}} \ \delta_{nm}
\end{equation}

i.e. the $u_{n,m}$ are normalised.
 
We can rewrite the argument of the cosine as containing a spatial-dependent part $f_P$, and an independent part (only weakly dependent on z) $\theta(z)=\frac{\pi}{4}+\psi(z)$, where $\psi(z)$ is again the Gouy phase:

\begin{equation}
kz+\frac{kr^2}{2R(z)}-(n+m+1)\psi(z)+\xi_q \equiv f_P(x,y,z)-\theta(z)(n+m)
\end{equation}

This takes care of the structure of the light field, so that we can just time evolve the $\alpha_{qnm}$.

The equation of motion for these must contain losses, and a pump term, plus an interaction term similar to the light field; the losses will yield a straightforward term $-\kappa \alpha_{nm}$.

We want the pump term to represent a Gaussian pump:

\begin{displaymath}
\label{eq:gaussianpump}
f(\vect{r})=f_P e^{\frac{\vec{r}^2}{2 \sigma_P^2}}
\end{displaymath}

where $\sigma_P$ is the pump width and the $f_P$ is the pump intensity. Given that this is a longitudinal pump, we want it to have the same shape as the cavity beam to match at the mirror:

\begin{equation}
\label{eq:pumptermdef}
f_{n,m} = \int d^2\vect{r} f(\vect{r}) u^\ast_{n,m,q}(\vect{r}, - z_R),
\end{equation}

Including an interaction term like in the atomic field interaction, and by similar considerations to write the equation of motion, we finally write:

\begin{multline}
\label{eq:eqnalphafirst}
 i\partial_t \alpha_{nm}=(\omega_{n,m}-\omega_{pump}-i\kappa)\alpha_{n,m}+f_{n,m} \\ \qquad - \mathcal{E}_0 N \sum_{n',m'} \int \mathrm{d}\vect{r} \int \mathrm{d}z |Z(z)|^2 |\psi(\vect{r})|^2 u_{n,m}(\vect{r},z) u_{n',m'}(\vect{r},z)\alpha_{n',m'}
\end{multline}

where we have lost the complex conjugation from the modulus squared as the $u_{n,m}$ are real.

