This leaves two terms from Eq. \ref{eq:weak-cpl-interaction-1}, the sum of the two cross-terms $f * Af$ and the product of the two $Af$ terms. 
The latter as is of order $\mathcal{O}(E_0^3)$ so we drop it in the perturbative approach.
The former, substituting in the expression for $A_{nmn'm'}$ from Eq.\ref{eq:Amn}, yields:

\begin{multline}
i \partial_t \psi(\vect{r})=...-
E_0 
\int \, \mathrm{d}z |Z(z)|^2 \sum_{n,m,n',m'} u_{n,m}^*(\vect{r},z) u_{n',m'}(\vect{r},z) \\
\bigg[
- \frac{f_{n,m} E_0 N}{(\Delta_0-i\kappa)\deltaplusikappa^2} \sum_{i',j'} A_{n',m',i',j'} f_{i',j'} 
\\- \frac{f_{n',m'} E_0 N}{\deltaplusikappa(\Delta_0-i\kappa)^2 } \sum_{i,j} A^*_{n,m,i,j} f^*_{i,j}
\bigg] 
\end{multline}

Now, given that $A_{n,m,n',m'}$, $u_{n,m}(\vect{r},z)$ and $f_{i,j}$ are real, we can drop the complex conjugation and relabel the indeces of one of the terms within the square brackets, so that the square brackets term reads:

\begin{multline}
\usign E_0 N\bigg[
\frac{1}{(-i\kappa - \Delta_0)^*\deltaplusikappa^2}  
+\frac{ 1}{\deltaplusikappa[(-i\kappa - \Delta_0)^2]^* } 
\bigg]f_{n',m'} \sum_{i,j} A^*_{n,m,i,j} f_{i,j} 
\end{multline}

which then becomes

\begin{multline}
i \partial_t \psi(\vect{r})=...\usign \frac{ 2E^2_0 N \Delta_0 }{(\Delta_0^2+\kappa^2)^2} \\ \times
\int \, \mathrm{d}z |Z(z)|^2 \sum_{n,m,n',m'} u_{n,m} (\vect{r},z) u_{n',m'}(\vect{r},z) f_{n',m'}  \sum_{i,j} A_{n,m,i,j} f_{i,j}
\end{multline}

Substituting in $A_{n,m,n',m'}$ from (\ref{eq:Amn}) and $u_{n,m}$ from (\ref{eq:umn}):

\begin{align}
i \partial_t \psi(\vect{r})=&
  ...\usign \frac{ 2E^2_0 N \Delta_0 }{(\Delta_0^2+\kappa^2)^2} \sum_{n,m,n',m',i,j}f_{n',m'} f_{i,j}
\chi_n\left( x \right)
  \chi_m\left( y \right) \chi_{n'}\left( x \right)
  \chi_{m'}\left( y \right) \nonumber  \\& \qquad
   \frac{1}{2} \cos \left( \theta(z) (n+m-n'-m') \right) \frac{1}{2} \cos \left( \theta(z) (n+m-i-j) \right)  
 \nonumber \\ &  \qquad \qquad
  \int \mathrm{d}^2 \vect{r'}  |\psi(\vect{r}')|^2 \chi_n\left( x' \right) \chi_m\left( y' \right) \chi_i\left( x' \right) \chi_j\left( y' \right)
\end{align}

We can define for convenience an effective interaction $U(\vect{r},\vect{r}')$, such that we may write the interaction term in the form $ \int \mathrm{d}^2 \vect{r'} U(\vect{r},\vect{r}')  |\psi(\vect{r}')|^2  $ -- this means, after some maths and expanding the the cosines:

\begin{multline}
\label{eq:U-weak-coupling}
U(\vect{r},\vect{r}')
=\usign\frac{ 2E^2_0 N \Delta_0 }{8(\Delta_0^2+\kappa^2)^2} \sum_{n',m'}
 f_{n',m'} \chi_{n'}\left( x \right)
  \chi_{m'}\left( y \right) 
 \\
   \sum_{i,j} f_{i,j} \chi_i\left( x' \right) \chi_j\left( y' \right)  
\sum_{n,m}
\chi_n\left( x \right)
  \chi_m\left( y \right) 
  \chi_n\left( x' \right) \chi_m\left( y' \right)  
 \\
\times \big[
   \expn{\theta_z (2(n+m)-i-j-n'-m')} +
   \expn{\theta_z (n'+m'-i-j)}   +  
   \\ 
   \expn{\theta_z (i+j-n'-m')} +
   \expn{\theta_z (i+j+n'+m'-2(n+m))}
  \big]
\end{multline}

where the integral kernels are the same as Eq. \ref{eq:integralkernelD} from the full model derivation.
We can tidy this up by defining a complex pump term $F(\vect{r},\xi)$:

\begin{equation}
F(\vect{r},\xi)= \sum_{n,m} f_{n,m} \chi_{n}\left( x \right)
  \chi_{m}\left( y \right) \expn{-i \xi (n+m)}
\end{equation}

Recalling that $f_{nm}=\int d\vect{r} f(\vect{r}) u_{n,m}(\vect{r},-z_R)$, we can write this as:

\begin{equation}
F(\vect{r},\xi)= \sum_{n,m} \int d\vect{r}' f(\vect{r}) \chi_{n}\left( x' \right)
  \chi_{m}\left( y' \right) \chi_{n}\left( x \right)
  \chi_{m}\left( y \right) \expn{-i \xi (n+m)}
\end{equation}

The cosine term in the $u_{nm}(\vect{r},-z_R)$ is one, as seen by substituting in coordinated on mirror $-z_M+\frac{r^2}{2|R_M|}$, $R(-z_M)=-|R_M|$, $\psi(z_M)=\pi/4$ and obtaining an expression for $\xi_{q_0}$ from the Gaussian optics.
\\
This is clearly related to the integral kerels defined above, as we can write:

\begin{equation}
F(\vect{r},\xi)= \int d\vect{r}' f(\vect{r}') D(\vect{r},\vect{r}',\xi)
\end{equation}

with the kernels $D(r,r',\xi)$ as in Eq. \ref{eq:integralkernelD}.

