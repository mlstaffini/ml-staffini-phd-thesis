We now need to make some more choices and lose some more generality before proceeding.

Let us, for the purpose of simplifying the maths, assume we are working in the middle of the cavity; then $\theta_a=\pi/4$, and we can write Eq. \ref{eq:U-analytic} as:

\begin{multline}
\label{eq:U-pi-4}
U(\vect{r},\vect{r}')=  \frac{U_0 f_P^2}{\pi} \int d \vect{r}' \bigg[ 2 \pi \left(\delta(r-r')+\delta(r+r')\right)
+ \\
\ 
 \left( \sin{\left( \frac{(\vect{r}+\vect{r}')^2}{2 } \right)} + \sin{\left( \frac{(\vect{r}-\vect{r}')^2}{2} \right)} \right) \bigg] \abs{\psi(\vect{r}')}^2
\end{multline}

\wip{This bracketing is hideous shame on you}

The mirror symmetry is very apparent in this equation -- looking at this, we can clearly see how to write an interaction term for only one of the contribution:

\begin{equation}
\label{eq:us-no-mirror}
u(\vect{s})=  \frac{U_0 f_P^2}{\pi}
\int d \vect{s}' \left[
 2 \pi \delta (\vect{s}) + 
 \sin{ \left( \frac{\vect{s}^2}{2} \right)} \right]
\end{equation}

We want to make no assumptions on the atomic distribution, but we want to extricate this integral from the $\abs{\psi(\vr)}^2$ to have a look at it. We can write things in $\vect{k}$-space, $\psi_{\vect{k}} = \int d \vr \psi(\vr) e^{-i\vect{k} \vr}$ etc., up to some volume factor which we don't need to worry about right now, and substitute this into the full equation of motion to isolate the $d \vr'$ integral plus some exponent $e^{i\vect{k} \vr'}$ due to the $\abs{\psi(\vr)}^2$ term. In practice, we're Fourier transforming Eq. \ref{eq:us-no-mirror} into $u(\vect{k})$; being careful about dimensions again, because we're evaluating the Gaussians in the integral\footnote{e.g. the Gaussian integral formula for this needs to be applied twice, $\int d^2 \vect{x} e^{-a x^2 +b x}=\frac{\pi}{a} e^{\frac{-b^2}{2a}}$.}, we have:

\begin{equation}
\label{eq:u-k-form}
u(\vect{k})=\int d^2 \vect{s} e^{i \vect{k} \cdot \vect{s}} u(\vect{s})= 8 U_0 f_P^2 \left[ \cos \left( \frac{\vect{k}^2}{2} \right) +1 \right]
\end{equation}

This yields, if we believe the maths, something that is always positive, which means that we can characterise the sign and behaviour of this $k$-space interaction by the sign of $U_0$ only, i.e. by the sign of detuning $\Delta_0$ as all other quantities are squared.

Given that this is the case, we want to consider some form of stability analysis that gives us some insight into wether there's a meaningful statement to be made about the sign of $u(\vect{k})$, as if this is the case we could readily see the behaviour in simulations.

\subsubsection{Bogoliubov-de Gennes analysis}

We can consider writing, for part of the equation of motion for the atoms:

\begin{equation}
\label{eq:GPE-reduced}
i \partial_t \psi(\vect{r}) = -\frac{\nabla^2}{2m} \psi(\vect{r})+\int d r' U(\vr-\vr') \abs{\psi(\vr')}^2 \psi (\vr)
\end{equation}

where we have thrown away the external trap, as well as still focusing on the no-mirror-image case. The reason we want to write this is that this form is in the shape of a GPE for a weakly interacting gas, for which we know\wip{\footnote{`Know' here being a somewhat hopeful term.}} it is possible to do a meaningful Bogoliubov-de Gennes analysis.

We can consider perturbations around a uniform distribution which solves the reduced equation:

\begin{equation}
\label{mm-BdG-distr}
\psi (\vr) = e^{-i \mu t} \left( \sqrt{\rho_0} +\delta \psi (\vr) \right)
\end{equation}

where $\rho_0$ is some uniform density, and we need to account for the time evolution due to the interaction term due to the uniform distribution:

\begin{equation}
\mu=\int d^2 \vr U(\vr-\vr') \rho_0
\end{equation}

We can now substitute this into Eq. \ref{eq:GPE-reduced}, and discard anything of order $\mathcal{O}(\delta^2)$ or higher. The equation is linear \wip{word?} in $\psi(\vr)$ so we can just write Eq. \ref{eq:GPE-reduced} for the perturbation $\delta \psi$, but we need to pay some attention with the interaction term. 

This will look like:

\begin{equation}
e^{-i \mu t} e^{-i \mu t} e^{+i \mu t} \bigg[
 \left( \sqrt{\rho_0} +\delta \psi^* (\vr') \right) \left( \sqrt{\rho_0} +\delta \psi (\vr') \right) \left( \sqrt{\rho_0} +\delta \psi (\vr) \right)
\bigg]
\end{equation}

and we want to only keep the terms in a single delta; the delta in $\vr$ will just yield a $\mu \delta \psi(\vr)$ term which cancels the $\mu$ from the time derivative on the left-hand side, so the GPE for the perturbation will be:

\begin{equation}
i \partial_t \delta \psi(\vr) = \left(-\frac{\nabla^2}{2m}+\mu \right) \delta \psi(\vect{r})+
\int d \vr' U(\vr-\vr') \rho_0 \left( \delta \psi(\vect{r}')+\delta \psi^*(\vect{r}') \right)
\end{equation}

where the exponentials in time have cancelled out.
We can Fourier transform into $k$-space:

\begin{equation}
\int \frac{d \vr}{\sqrt{V}} \delta \psi(\vr) e^{-i \vect{k} \cdot \vr} = \delta \psi_{\vk}
\end{equation}

and conversely:

\begin{equation}
\delta \psi (\vr)=\frac{1}{\sqrt{V}} \sum_k \delta \psi_k e^{i \vect{k} \cdot \vr}
\end{equation}

Applying the forward FT to both sides of the equation, and backwards for the $\psi(\vr')$ which we want to avoid integrating over:

\begin{multline}
i \partial_t \delta \psi(\vect{r}) = \left(\frac{\vk^2}{2m}+\mu \right) \delta \psi(\vect{r})
\\ + \rho_0 \int d \vr e^{-i \vect{k} \vr} \int \frac{d \vr'}{\sqrt{V}} U(\vr-\vr')  \frac{1}{\sqrt{V}} \sum_{\vect{k}'} 
\left( \delta \psi_{\vect{k}'} +\delta \psi^*_{-\vect{k}'}  \right) e^{i \vect{k}' \vr'}
\end{multline}

We are left with the following integral to evaluate:

\begin{equation}
\int d \vr \int \frac{d \vr'}{V} U(\vr-\vr') e^{i ( \vect{k}' \vr' - \vect{k} \vr)}
\end{equation} 

To do this we can transform to coordinates $\vr=\vect{R}+\frac{\vect{\sigma}}{2}$ and $\vect{r}'=\vect{R}-\frac{\vect{\sigma}}{2}$; the Jacobian for this coordinate change is one, so we can straightforwardly get the geometry of the integrals, as well as the argument of the interaction function $U(\vr-\vr')$; substituting the new variables into the exponential:

%\begin{equation}
%\int d \vect{\sigma} \int \frac{d \vect{R}}{V} U(\sigma) e^{i\vect{R}(\vect{k}'-\vect{k})} e^{-i\frac{\vect{\sigma}}{2}(\vect{k}'+\vect{k})}
%\end{equation} 

\begin{equation}
\int d \sigma \int \frac{d \vect{R}}{V} U(\sigma) e^{i\vect{R}(\vk'-\vk)} e^{-i\frac{\sigma}{2}(\vk'+\vk)}
\end{equation} 

then the $\vect{R}$ integral (including the volume factor) yields a delta function $\delta_{\vect{k},\vect{k}'}$; if we define $\epsilon_{\vk}=\vk^2/2m$, and leave the integral $U(\vk)=\int d \vect{\sigma} U(\vect{\sigma}) \exp(-i \vect{k} \cdot \vect{\sigma} )$ as is, we have:

\begin{equation}
\label{mm-BdG-eom}
i \partial_t \delta \psi_{\vect{k}} = \epsilon_{\vect{k}} \delta \psi_{\vect{k}}+ \rho_0 U(\vect{k}) (\delta \psi^*_{-\vect{k}}+\delta \psi_{\vect{k}})
\end{equation}

We can now write a Bogoliubov-De Gennes parametrisation of the perturbation as in Chapter \ref{sm}; we define:

\begin{equation}
\delta \psi_{\vect{k}} = u_{\vect{k}} e^{-i \nu_{\vect{k}} t}+v_{\vect{k}}^* e^{-i \nu^*_{\vect{k}} t}
\end{equation}

where in principle $\nu$ can be complex.
Once again, the idea behind this is to obtain equations for the weights $u_{\vect{k}}$ and $v_{\vect{k}}$ by collecting the terms with $e^{-i \nu_{\vect{k}} t}$ dependence, and $e^{i \nu^*_{\vect{k}} t}$ for the latter.

The terms time evolving as $e^{-i \nu_{\vect{k}} t}$ yield:

\begin{equation}
\nu_{\vk} u_{\vk} = \epsilon_{\vk} u_{\vk} + \rho_0 U(\vk) (u_{\vk}+v_{-\vk})
\end{equation}

while the terms with time dependence $e^{i \nu^*_{\vect{k}} t}$ yield (noticing the negative sign due to the partial time derivative):

\begin{equation}
\nu_k v_{-k}= -\epsilon_{\vk} v_{-\vk} - \rho_0 U(\vk) (u_{\vk}+v_{-\vk})
\end{equation}

We can then get the mode frequencies $\nu_{\vk}$:

\begin{equation}
\nu_{\vk }
\begin{pmatrix}
  u_{\vk} \\
  v_{-\vk} 
\end{pmatrix}
=
 \begin{pmatrix}
  \epsilon_{\vk}+\rho_0 U(\vk) & \rho_0 U(\vk) \\
  -\rho_0 U(\vk) & -\epsilon_{-\vk}-\rho_0 U(\vk)
 \end{pmatrix}
 \begin{pmatrix}
  u_{\vk} \\
  v_{-\vk} 
\end{pmatrix}
 \end{equation}
 
Flipping the sign of $v_{\vk}$ and taking the determinant, we get:

\begin{equation}
\nu_{\vk}=\sqrt{(\epsilon_{\vk} + \rho_0 U(\vk))^2-(\rho_0 U(\vk))^2} = \sqrt{\epsilon_{\vk}(\epsilon_{\vk}+2\rho_0 U(\vk))}
\end{equation}

This means that when the argument of the bracket becomes negative, the frequencies become imaginary, and the exponents in the Bogoliubov-de Gennes parametrisation blow up, i.e. the perturbation grows instead of decaying, and the system becomes unstable.

This criterion yields:

\begin{equation}
\label{eq:instability}
2 \rho_0 U(\vk)<-\epsilon_{\vk}
\end{equation}

If we return to the form of interaction we derived above, Eq. \ref{eq:u-k-form}, we conclude that this means that we only expect instability for one sign of detuning: as we saw above, the sign of the term is entirely determined by the sign of detuning, $U(\vk)\propto \Delta_0$, as every other quantity is squared; therefore, for positive $\Delta_0$ the system is never unstable, and we should only see instability for negative $\Delta_0$.
%is always positive, and more negative than $-\epsilon_{\vk}$, for positive detuning, which means that criterion Eq. \ref{eq:instability} is only be fulfilled for \textbf{positive} detuning, and we should \textbf{only} see instability for positive $\Delta_0$.

At this point, we could continue on with qualitative analysis, as we have closed forms of all quantities in Eq. \ref{eq:instability}; however, we have come quite far from the full model here: we have assumed weak coupling and fast evolution of the light field, an (infinitely) large pump spot, atoms exclusively in centre of the cavity along the $z$-axis, a certain sloppiness in dimensions, and only considered the self-interaction, discarding the mirror image. The \textit{qualitative} understanding that we may expect instability for only one sign of detuning is a helpful one to bring to the general case in order to begin puzzling it out; the \textit{quantitative} analysis is very likely to be too reductive, and we have heaped enough maths upon this problem; let us turn back to more general cases for the numerics.