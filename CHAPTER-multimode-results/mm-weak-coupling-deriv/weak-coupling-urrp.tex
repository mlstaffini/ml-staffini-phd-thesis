The expression in Eq.\ref{eq:Urr} includes $D(\vect{r},\vect{r}',0)$ factors. Naively substituting this in, we can see this spawns a whole host of infinities -- we need to tackle this more formally.

Going back to the definition of the integral kernels $K(x,x',\xi)$, we recall that $K(x,x',\xi\rightarrow 0)=\delta(x-x')$; in other words, the full kernels $D(r,r',\xi)$ will yield spatial delta functions.

Taking this into consideration, for the integral kernel we have:

\begin{equation}
D(\vect{r},\vect{r}',0)=\frac{1}{2}\left[\delta(\vr+\vr')+\delta(\vr-\vr')\right]
\end{equation}

We can write the integral Eq. \ref{eq:Urr} as having a ``regular" part, i.e. the contributions from the $2\theta_a$ terms which are well behaved everywhere but at the ends of the cavity, where we expect pathological behaviour to happen, and a part involving the terms which yield delta functions, something like:

\begin{equation}
\int \mathrm{d}  \vect{r}' \left[ U^{reg}(r,r') + U^{delta}(r,r') \right] \abs{\psi(\vect{r}')}^2
\end{equation}

where $U(r,r')=U^{reg}(r,r')+U^{\delta}(r,r')$
Let us define $U_0=\usign\frac{ E^2_0 N \Delta_0 }{4(\Delta_0^2+\kappa^2)^2}$ so we can stop worrying about signs and prefactors; then we can write the regular and delta terms as:

\begin{multline}
\label{eq:weak-cpl-Ureg-1}
U^{reg}(\vect{r},\vect{r}')=U_0
\bigg[
F(\vect{r},-\theta_a)F(\vect{r}',-\theta_a)D(\vect{r},\vect{r}',2\theta_a) \\ + F(\vect{r},\theta_a)F(\vect{r}',\theta_a)D(\vect{r},\vect{r}',-2\theta_a)
\bigg]
\end{multline}

and 

\begin{align}
\label{eq:weak-cpl-Udelta-1}
U^{\delta}(\vect{r},\vect{r}')=\frac{U_0}{2}
\bigg[
F(\vect{r},\theta_a)&F(\vect{r}',-\theta_a)
\big(\delta(r-r')+\delta(r+r') \big)
\\&+ F(\vect{r},-\theta_a)F(\vect{r}',\theta_a) \big(\delta(r-r')+\delta(r+r') \big) \nonumber 
\bigg]
\end{align}

Let us work on these two terms in turn, beginning with the delta function term Eq. \ref{eq:weak-cpl-Udelta-1}, for a large pump spot.

In this limit, i.e. using Eq. \ref{eq:bigpumpspot}, Eq. \ref{eq:weak-cpl-Udelta-1} expands to:

\begin{multline}
U^{\delta}(r,r')=\frac{U_0}{2} \left( \frac{f_P}{\cos(\theta_a)} \right)^2
\bigg[ e^{-i\frac{r^2-r'^2}{2} \tan (\theta_a)}
\big(\delta(r-r')+\delta(r+r') \big)
\\
+ e^{i\frac{r^2-r'^2}{2} \tan (\theta_a)}
\big(\delta(r-r')+\delta(r+r') \big)
\bigg]
\end{multline}

which is just:

\begin{equation}
U^{\delta}(\vect{r},\vect{r}')=U_0 \left( \frac{f_P}{\cos(\theta_a)} \right)^2
\cos \left( {\frac{(r^2-r'^2)}{2} \tan \theta_a}\right) \left(\delta(r-r')+\delta(r+r') \right)
\end{equation}

%%
When we go to integrate this, the cosine integrates to one for both forms of delta function, and we are left with something explicitly dependent on the densities at both position and mirror image position:

\begin{equation}
\label{eq:irregular-integral}
\int \mathrm{d} r' U^{\delta} \abs{\psi(r')}^2
=U_0 \left( \frac{f_P}{\cos(\theta_a)} \right)^2 \left(\abs{\psi(r)}^2+\abs{\psi(-r)}^2 \right)
\end{equation}

This has a physical meaning which makes sense: this term is a local self-interaction, plus a mirror-image self-interaction due to cavity symmetry.

For the regular part, we can code $U^{reg}$ as is, but we can also carry on with analytics in the large pump spot limit; substituting this into Eq. \ref{eq:weak-cpl-Ureg-1}, we can carry out the two convolutions; for the first of the components of $U^{reg}$ we get:

\begin{multline}
F(\vect{r},-\theta_a)F(\vect{r}',-\theta_a)D(\vect{r},\vect{r}',2\theta_a) =\\ \left(\frac{f_P}{\cos \theta_a}\right)^2 \frac{1}{4 \pi i \sin 2\theta_a}
\exp \left[\frac{i}{2}(\vect{r}^2+\vect{r}'^2) \left( \tan \theta_a + \frac{1}{ \tan 2\theta_a} \right) \right] 
\\
 \times \left[ \exp\left(\frac{i \vect{r}\vect{r}'}{\sin 2\theta_a} \right) + \exp\left(-\frac{i \vect{r}\vect{r}'}{\sin 2\theta_a} \right) \right]
\end{multline}

Using the fact that $\tan x + \frac{1}{ \tan 2x} = \frac{1}{ \sin 2x}$, we can write this as:

\begin{multline}
F(\vect{r},-\theta_a)F(\vect{r}',-\theta_a)D(\vect{r},\vect{r}',2\theta_a) =\\ \left(\frac{f_P}{\cos \theta_a}\right)^2 \frac{1}{4 \pi i \sin 2\theta_a} \left[
 \exp\left(\frac{i (\vect{r}+\vect{r}')^2} {2 \sin 2\theta_a} \right) + \exp\left(\frac{i (\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right) \right]
\end{multline}

Similarly, for the second term (notice the minus sign):

\begin{multline}
F(\vect{r},\theta_a)F(\vect{r}',\theta_a)D(\vect{r},\vect{r}',-2\theta_a) = \\ \left(\frac{f_P}{\cos \theta_a}\right)^2 \frac{-1}{4 \pi i \sin 2\theta_a} \left[
 \exp\left(-\frac{i (\vect{r}+\vect{r}')^2} {2 \sin 2\theta_a} \right) + \exp\left(-\frac{i (\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right) \right]
\end{multline}

Combining $\frac{1}{2 i}\left[\exp\left(\frac{i (\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right) - \exp\left(-\frac{i (\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right)  \right]$ into $\sin \left( \frac{(\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right)$, and likewise for $r+r'$ terms, we get an overall, analytic form of the interaction term:

\begin{multline}
\label{eq:U-analytic}
\frac{U_0}{2 \pi} \left( \frac{f_P}{\cos(\theta_a)} \right)^2
\int d \vect{r}' \bigg[
 2 \pi \left( \delta(r-r') + \delta(r+r') \right)
 +
 \\
\frac{1}{\sin 2\theta_a}
\bigg(
 \sin \left( \frac{(\vect{r}+\vect{r}')^2}{2 \sin 2\theta_a} \right)  + \sin \left( \frac{(\vect{r}-\vect{r}')^2}{2 \sin 2\theta_a} \right) 
 \bigg) 
\bigg] \abs{\psi(\vect{r}')}^2
\end{multline}

where again, this term encodes explicitly the presence of both the atom cloud and from its mirror image, in the self- and mirror-interaction, and in the long-range contribution from the interference of these two.