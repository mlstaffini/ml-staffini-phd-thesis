We aim to review of the literature around an experimental setup realising confocal multimode cavity optics, resulting from a collaboraion between Lev group in Stanford, and the Keeling group in St Andrews; we begin by introducing the setup itself, as this would be the relevant setup for our own results, then cover some of the phases and effects already observed.

As stated before, atoms in cavities lend themselves to simulators, given the ease of observation and manipulation: this is the guiding principle in wanting to investigate the breadth of available physics within a multimode cavity setup.

\subsubsection{Experimental setup}

We begin by introducing the experimental setup described om \citet{Kollar2015}, which is relevant for Refs. \cite{Kollar2017,Kroeze2018,Vaidya2018,Kroeze2019,Guo2019A,Guo2019Lett}, as well as the natural setup for any further investigation we carry out.

The setup realises a confocal or near-confocal multimode cavity, and  the cavity geometry is fully adjustable to access up to the single mode regime, i.e. as it is moved out of confocality the mode degeneracy lifts, until a point where the pump-cavity detuning is much smaller than the frequency difference between distinct TEM modes.

In the multimode regime, the interaction length is less than infinite-range due to the interference of Hermite-Gauss (or Laguerre-Gauss) modes, and falls off outside the beam width $w_0$ of the $TEM_{0,0}$; the mode families addressed are either odd or even transverse modes as explained in Section \ref{multimode-optics}.

\input{CHAPTER-multimode-lit/figure-codes/figure-lev-setup}
\footnotetext{Reprinted from Kollár, A. J., Papageorge, A. T., Baumann, K., Armen, M. A. \& Lev, B. L. An adjustable-length cavity and Bose-Einstein condensate apparatus for multimode cavity QED. New J. Phys. 17, 043012 (2015) under Creative Commons Attribution 3.0 license.}

Fig. \ref{fig:lev-setup} shows the apparatus, which has two parts: one dedicated to the production of Bose-condensed atoms, and one containing the multimode cavity; experimental problems include vibration isolation, maintaining the vacuum, accuracy and stability of cavity length, mirror quality, and allowing for optical access to the science chamber\cite{Kollar2015}.

The two mirrors have radii $R_1=9.959(1)$mm and $R_2=R_1+8.6(1.1) \mathrm{\mu m}$, i.e. the cavity a small amount of astigmatism.

The pump beam is at $780$nm, the cavity loss rate is $\kappa \approx 1$ MHz, and the cavity stability is almost four times smaller than the cavity linewidth $2 \kappa$. The lifetime of the atoms within the optical dipole traps ($\approx 7s$) is enough to evaporate the ultracold gas into a Bose-condensed cloud of about $10^5$ atoms, and then probe QED physics with it. 

Imperfection of mirrors limits the number of degenerate modes actually available due to worse cavity finesse at higher transverse modes, but the high order in each transverse index of the modes observed, as well as further observation in Ref.\cite{Vaidya2018}, shows that several thousand modes are supported at confocality. 

The dispersive shift of the cavity can be measured and yields the bare light-matter coupling $g_0$; the beam waist for the $TEM_{00}$ is $w_0=35 \mu m$.

The cavity losses are detected by a single-photon detector at about $10$\% efficiency for the single mode case and for $TEM_{00}$ in the multimode case, and decrease for higher order transverse modes.

While the science chamber can be pumped either transversally or longitudinally, in the spirit of the single-mode transverse pumping self-organisation, the transverse configuration is used in the following literature.

\subsubsection{Tunable range interaction in confocal cavities}

The same self-organisation mechanism of the single mode, transversely pumped cavity holds in the multimode case\footnote{And in fact, a `supermode' self-organisation (where supermode is a superposition of cavity modes by the atomic interaction) was demonstrated in this setup in a few-mode case \cite{Kollar2017}.}. Above a critical pump value, the kinetic energy cost is overcome and atoms are drawn into a $\lambda$-spaced lattice of pump light plus scattering into the cavity field, choosing either even or odd antinodes of the cavity field; the superradiance is heralded by both spatial organisation, and collective emission into the cavity field. In a confocal multimode cavity, however, the atoms can scatter into a superposition of any of the degenerate cavity modes dictated by the their strength at the $(x,y)$ position of the atoms; in the near-confocal case, the weights of the superposition also depends on the detuning between transverse modes, $\Delta_{l,m}=\Delta_{0,0}+(l+m)\epsilon$, where $\epsilon$ is a small parameter encoding the non-confocality, $\epsilon=c \delta L/L^2$.

This means that the interaction, which is all-to-all in the single mode case, need not be long range in multimode cavities, as the transverse structure can mediate shorter range interactions.
As in Ref. \cite{Vaidya2018}, one can consider writing the interaction energy spatial structure as something like:

\begin{equation}
\mathcal{D}(x,x') \propto \sum_{l,m} \frac{\Xi_{l,m}(x) \Xi_{l,m}(x')}{\Delta_{0,0}+(l+m)\epsilon}
\end{equation} 

where the $\Xi$ are Hermite-Gauss modes, i.e. to obtain the interaction between points $x$ and $x'$ one needs to sum over the contributions of the modes at those points, weighted by the detunings.

\input{CHAPTER-multimode-lit/figure-codes/figure-mirror-scheme}
\footnotetext{Reprinted from \citet{Vaidya2018} under the terms of the Creative Commons Attribution 4.0 International license.}

Fig. \ref{fig:mirror-scheme} shows the beam pattern in a confocal cavity in three dimensions: because of the `bowtie' pattern, if a cloud is placed at $x_1$ a mirror image of it will appear at $-x_1$; we can therefore expect that any total interaction $\mathcal{D}(x,x')$ contain two contributions: a direct interaction $\mathcal{D}(x,x')$, and a mirror image interaction $\mathcal{D}(x,-x')$. Evaluating $\mathcal{D}(x,-x')$ at $x=x'$, for a perfect cavity, the self-interaction should be entirely flat, and the mirror image interaction a perfect delta function (due to orthogonality of transverse HG modes); however, for a real cavity supporting finite albeit large family of modes, the self-interaction yields a wide, flat background, whereas for small $x_1$, the mirror-image interaction creates a short-range peak of interaction. The ratio controlling the form of $\mathcal{D}(x,x')$ is $\epsilon/\Delta_{00}$; this can be adjusted experimentally to control the interaction range between $w_0$ of the single mode regime and a small fraction of $w_0$.

\input{CHAPTER-multimode-lit/figure-codes/figure-cavity-waist}
\footnotetext{Reprinted from \citet{Vaidya2018} under the terms of the Creative Commons Attribution 4.0 International license.}

Fig. \ref{fig:cavity-waist} shows the superradiant emission in the cavity as a Bose-condensed cloud is moved from $x_1=-1.92 w_0$ to $1.92 w_0$, panels (a) through (i); here we can see the contribution of the mirror image clearly. This is compared to the single mode regime, panels (j-p), which we can see the superradiant emission has a much smaller width; the $TEM_{00}$ beamwaist $w_0$ is marked for scale.

Two BEC clouds can also be made to interact via the mirror image mechanism: each BEC can be placed to overlap with the mirror image of the other, thus allowing for interaction without unwanted effects from real merging of two BECs.

Thus, there is already a rich set of features in this setup: tunable interaction length, a small beamwaist, and the possibility of mirror-image interaction of two BECs.

Refs. \cite{Guo2019A,Guo2019Lett} also see sign-changing interaction: consider the Gouy phase accumulation (covered in Section \ref{multimode-optics}) and the mode pattern image presented in Fig. \ref{fig:mode-comb}. So far, we have not considered the longitudinal structure of the family of modes presented in this figure; we however know that the Gouy term in Eq.\ref{eq:mode-frequency-families} matches modes from different $q$ indeces together. This yields different longitudinal phase shifts within a family of degenerate modes in confocal cavities; the form of this interference will vary along the transverse plane depending on which modes participate, and has an overall nonlocal contribution $\cos(2 \vect{r}\cdot \vect{r}'/w_0^2)$, which changes sign depending on the placement of the cloud. 

Spin-correlated superradiant phases are also observed in Refs. \cite{Kroeze2018,Kroeze2019}, as well as Meissner-like effects \cite{Ballantine2017}; the presence of sign-changing, tunable-range, and spin-correlation interactions within one apparatus suggests the possibility of simulating interesting physics like spin glasses in the setup.

\wip{do I need \cite{Landig2016}}\\
\wip{I just do not understand the sign changing}