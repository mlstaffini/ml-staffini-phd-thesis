\input{CHAPTER-multimode-lit/figure-codes/figure-cavity-geometry-1}

We now want to consider optical resonators -- cavities -- with a geometry such that multiple modes are supported and can couple to the atoms. Where the single mode case yielded a straightforward, long-range, mean-field like coupling where all atoms are coupled to all with the same intensity, introducing higher order modes allows for spatial patterns in the cavity mode. The exact family of modes a cavity supports depends on the geometry of it. Not all cavity geometries are stable (i.e. supporting a set of resonant cavity modes); the detail of their behaviour can be derived from Gaussian optics.

If we have a pair of mirrors of radius $R$, and a light Gaussian beam contained within this cavity, we can imagine that, if $R$ matches the radius of the Gaussian wavefront at the position of the mirrors, the beam is reflected into a perfect mirror image (assuming the mirrors are much larger than the beamwaist so they don't clip it) i.e. the cavity supports the standing wave; since it supports the zeroeth mode, higher order modes will also be resonant.

For generic mirrors of radius $R_1$ and $R_2$, and spacing $L$ between them, it is not always possible that a Gaussian beam in the cavity meets the criterion of matching both mirrors radii at the mirror locations. 

For any such cavity, we can define parameters $g$-parameters $g_1=1-L/R_1$ and $g_2=1-L/R_2$ \cite{Siegman1986}; any cavity geometry can then be defined on the $g_1,g_2$ plane\footnote{The $g$ parameters follow from Gaussian optics, and are a sort of characterisation of where a stable beam waist wants to be given a certain cavity shape.}.

We can write a stability criterion for the $g$-parameters:

\begin{equation}
0 \le g_1 g_2 \le 1
\end{equation}

This defines an area on the $g_1,g_2$ plane, within which optical resonators are stable. Let us restrict ourselves to symmetric cavities, $R_1=R_2$, and identify the limiting cases and the geometries they yield. The full plane is shown is Fig. \ref{fig:cavity-optics}.

At the $g_1g_2=1$ end, for a symmetric cavity, we have two possible configurations: either $g_1=g_2=1$, or $g_1=g_2=-1$. 

The $g_1=g_2=1$ is obtained when $R_1=R_2=\infty$, i.e. a \textbf{planar} cavity with flat mirrors and foci at $\pm \infty$; the $g_1=g_2=-1$ case corresponds to $R_1=R_2=1/2$, i.e. a \textbf{concentric} cavity with foci at the same location. It makes sense that these are the limiting cases on each side of the $g_1=g_2$ line on \ref{fig:cavity-optics}: we have stability between the two cases of infinite overlap and one-point overlap of the foci; beyond, the cavities don't have a beam waist that matches both mirrors.

In between the $g_1=g_2=-1$ and $g_1=g_2=1$ cases we have the central point of the stability diagram, $g_1=g_2=0$, which is obtained by $R_1=R_2=L$, i.e. the focus of each mirror lies on the other. This is called a \textbf{confocal} cavity, and as it is the centre point of the stability diagram, it should not surprise that it has interesting properties; we will focus on this cavity geometry in the literature and our own results, so we cover more detail of it below, after a brief introduction to the formalism of Gaussian cavity optics.

\subsection{Gaussian optics}

Before continuing, we want to take a step back and connect these intuitive geometry considerations to the Gaussian optics underlying them, and to define some quantities that are commonly used in characterising the optics of Gaussian beams and cavity field.

For a given cavity shape, the Gaussian beam will have a unique Rayleigh range, an important quantity in optics defined as the distance at which the cross section of the beam doubles.

The Rayleigh range is defined as:

\begin{equation}
\label{eq:rayleigh-range}
z_R=\frac{w_0^2 \pi}{\lambda}
\end{equation}

where $w_0$ is the beam waist, the point of minimum beam width, and the beam width at position $z$ away from the beam waist is:

\begin{equation}
\label{eq:beam-waist-def}
w(z)=w_0 \sqrt{1+\frac{z^2}{z_R^2}}
\end{equation}

We can capture the detail of the transverse structure of the cavity field, and how it depends on position along the cavity azis, with Hermite-Gauss (HG) modes, for Cartesian space, and in Laguerre-Gauss (LG) when there is a rotational symmetry to be exploited. Mathematically, these are built out of Hermite-Gauss or Laguerre-Gauss polynomial as an orthonormal basis, hence the name. 

Deriving the lowest-order mode expression involves a generous handful of optics: solving the paraxial wave equation for spherical waves and allowing for complex source points is one way\footnote{Alternatively using a trial function to then refine -- we refer to Chapter 16 of \citet{Siegman1986}, or alternatively Ref. \cite{Pampaloni2004} for the full derivation.}. The fundamental mode is one which minimises beam divergence and cross section at the same time \cite{Pampaloni2004}. These derivation `blurbs' point to the underlying spirit of the solution: these are modes built to best describe a travelling- or standing-wave Gaussian beam. 

We will go through the detail of the shape of these modes in the model derivation of Chapter \ref{mm}; here, let us consider some aspects of them which fall out of the maths.

We consider a Gaussian beam with complex curvature, i.e. the radius of which is described by a complex quantity with both a real, spherical wave radius part, and a imaginary part to encode phase, as:

\begin{equation}
\label{eq:complex-radius}
\frac{1}{q(z)}=\frac{1}{R(z)}-\frac{i \lambda}{\pi w^2(z)}
\end{equation}

where the first part is the real radius $R(z)=z+z_r^2/z$ (in a symmetric stable cavity), and the second we recognise as the Rayleigh range for $w_0$. 

In terms of the complex radius (and in Cartesian coordinates), we can write the transverse structure in $xy$ at a point $z$ along the cavity in terms of the Hermite-Gauss modes separately in $x$ and $y$, $u_{nm}(x,y,z)=u_n(x,z) \times u_m(y,z)$, where the $n$ and $m$ are indeces in the transverse directions $x$ and $y$ respectively, with:

\begin{equation}
\label{eq:u-intro}
u_n(x,z)= \left( \frac{2}{\pi} \right)^{\frac{1}{4}} 
\left( \frac{1}{2^n n! }\right)^{\frac{1}{2}} 
\left( \frac{q_0}{w_0 q(z)}\right)^{\frac{1}{2}} 
\left(\frac{q_0}{q_0^*} \frac{q^*(z)}{q(z)}\right)^{\frac{n}{2}} 
H_n \left( \frac{\sqrt{2} x}{w(z)} \right) 
e^{-i\frac{kx^2}{2 q(z)}}
\end{equation}

This is less than transparent, and we will come back to each factor in turn later when they are needed for model derivation, but briefly, we can see some prefactors depending on the beam waist and mode order (first and second brackets), a spherical wave contribution (last brackets), the Hermite polynomial terms $H_n$, and then two terms depending on complex radiuses. Let's talk about these for a moment.

The complex radius encodes real curvature, but it is also an expression of phase within the cavity. We can make this explicit by expressing it as a function of a phase angle $\psi(z)$ as well as a magnitude, following Siegman:

\begin{equation}
\frac{i}{q(z)}= \frac{\lambda}{\pi w^2} \left(1+i \frac{\pi w^2(z)}{R(z) \lambda}\right)\equiv \frac{exp(i\psi(z))}{\vert q\vert}
\end{equation}

Then the phase angle $\psi(z)$ is the argument of the complex number $1+i \frac{\pi w^2(z)}{R(z) \lambda}$ which means:

\begin{equation}
\label{eq:Gouy-phase-original}
\psi(z)= \arctan \frac{\pi w^2(z)}{R(z) \lambda}
\end{equation}

We can also rewrite this in terms of Eq. \ref{eq:beam-waist-def}, and, using $R(z)=z+z_r^2/z$, and $k=2\pi/\lambda$, as:

\begin{equation}
\label{eq:Gouy-phase}
\psi(z)= \arctan \frac{z}{z_R}
\end{equation}

This is called the Gouy phase shift, and encodes the extra phase accumulation of a Gaussian beam as it propagates through a focus\cite{Gouy1890,Gouy1891,Boyd1980,Feng2001,Visser2010}, i.e. it is a phase factor due to the geometry of Gaussian beams. Note that at the position where $w(x)=w_0$, the Gouy phase is zero. 

Using this one can write the terms depending on the various $q$ in Eq. \ref{eq:u-intro} as:

\begin{equation}
\label{eq:Gouy-fundamental}
\frac{1}{w_0} \frac{q_0}{q(z)} = \frac{1}{w(z)} e^{i\psi(z)}
\end{equation}

which is a (normalised) correction to the fundamental mode, and an extra phase factor only present for higher order modes:

\begin{equation}
\label{eq:Gouy-higher-order}
\frac{q_0}{q_0^*} \frac{q^*(z)}{q(z)}=\frac{1}{w(z)} e^{i \psi(z)}
\end{equation}

which is a pure phase factor depending on mode order. The Gouy phase term in the Hermite-Gauss modes will look like:

\wip{sign of exponential in Siegman 60}

\begin{equation}
\label{eq:hg-un}
u_n(x,z)= \left( \frac{2}{\pi} \right)^{\frac{1}{4}} 
\left( \frac{1}{2^n n! }\right)^{\frac{1}{2}} 
\frac{e^{\frac{i}{2}(n+1)\psi(z)}}{\sqrt{w(z)}}
H_n \left( \frac{\sqrt{2} x}{w(z)} \right) 
e^{-i\frac{kx^2}{2 R(z)}-\frac{x^2}{w^2(z)}}
\end{equation}

We now return to the question of fitting the Hermite-Gauss modes we've derived to cavity geometry. We can write the unique Rayleigh range of a cavity in terms of g parameters as:

\begin{equation}
z_R=\sqrt{\frac{g_1 g_2 (1-g_1 g_2)}{(g_1^2+g_2^2-2g_1 g_2)}} L
\end{equation}

This means we can write beam waist and spot size in terms of $g$-parameters from Eq. \ref{eq:beam-waist-def}, which we can also use to write the position of the mirrors:

\begin{equation}
z_1=\frac{(1-g_1)g_2}{g_1+g_2-2 g_1 g_2} L \qquad z_2=\frac{(1-g_2)g_1}{g_1+g_2-2 g_1 g_2}L
\end{equation}

As promised, the cavity optics can be defined in terms of $g$ factors.

We can combine these two parts of our knowledge to write the frequencies of the transverse modes by using the phase shift from Eq. \ref{eq:hg-un}, and observing that in order for the cavity to be stable, there must be a transverse phase accumulation of an integer multiple of $\pi$ from one end of the cavity to the other, so that a round trip is $2 n \pi$. We can write:

\wip{Where eq. 22 from?}

\begin{equation}
\frac{\omega L}{c} - (n+m+1) \arccos \left( \pm \sqrt{g_1 g_2} \right)= q \pi
\end{equation}

%\wip{but confocal every two round trips? how does this fit?}

where the first part is how many wavelengths fit in the cavity, and the second the correction for the Guoy phase accumulation. The overall frequency in longitudinal plus transverse direction is then:

\begin{equation}
\label{eq:mode-frequency-families}
\omega=\omega_{qnm}=\frac{\pi c}{
L}\left[q+(n+m+1)  \frac{\arccos (\pm \sqrt{g_1 g_2})}{\pi} \right]
\end{equation}

where:

\begin{equation}
\frac{\arccos (\pm \sqrt{g_1 g_2})}{\pi} \approx
\begin{cases} 
      0 & g_1=g_2=1 \\
      1/2 & g_1=g_2=0 \\
      1 & g_1=g_2=-1
   \end{cases}
\end{equation} 

\input{CHAPTER-multimode-lit/figure-codes/figure-mode-comb}

We can use this equation to obtain the frequencies of families of transverse modes. Fig. \ref{fig:mode-comb} shows the configuration in the confocal case, as well as near-planar and near-concentric.

We discuss the confocal case in detail below.

\subsection{Confocal cavity geometry}

\input{CHAPTER-multimode-lit/figure-codes/figure-confocal-ray-tracing}

The confocal cavity is the configuration used in most of the literature we cover, as well as our own results, due to its properties, so we will characterise it more extensively.

The confocal cavity has the property that the mirrors are located at exactly $\pm z_R$; working at exactly the Rayleigh range simplifies quantities considerably; from Eq. \ref{eq:rayleigh-range} we can write $w_0=\sqrt{L\lambda / 2\pi}$, and $w_1=w_2=\sqrt{L\lambda / \pi}$. Tilting and misalignment of one of the mirrors does not affect the geometry of the cavity as the focus moves along the radius of the other mirror.

The Guoy phase $\psi=\arctan z/z_R$ goes from $-\pi/4$ to $\pi/4$; i.e. the overall phase accumulation between different transverse modes takes two round trips to come back to itself. The beam pattern in a confocal cavity is a `bowtie', as shown in Fig. \ref{fig:confocal-ray-tracing}; the light field returns to itself exactly only after two round-trips.

We are interested in the mode structure from Eq. \ref{eq:mode-frequency-families} of the confocal cavity, as this informs us about which modes we address at a given frequency, as shown in Fig. \ref{fig:mode-comb}. Let's briefly make sense of this.

We compare to the planar cavity, where since the $n+m+1$ gives no contribution, we simply have all $n,m$ modes with a given $q$ at a degenerate frequency (figure shows near degenerate in order to see the detail in the transverse structure).

We now want to compare the confocal case:

\begin{equation}
\label{eq:mode-families-confocal}
\omega=\omega_{qnm}=\frac{\pi c}{
L}\left[q+\frac{1}{2}(n+m+1) \right]
\end{equation}

and keep the contents of the brackets constant so we're at the same frequency as the planar.

For the mode which has $n+m=0$, i.e. the fundamental $u_{0,0}$, this means that the frequency of the longitudinal mode $q$ is shifted to a frequency halfway between the $q$ and $q+1$ mode frequencies of the planar cavity; families of modes where $n+m=1$, i.e. $01$ and $10$, are shifted to the frequency of the $q$ planar mode; $n+m=2$ fall halfway betwene planar $q$ and $q+1$ frequencies and so on, such that we see a set of degenerate modes every half planar mode spacing, alternating between even- or odd-only transverse modes. 

In other words, the confocal cavity supports either a set of degenerate even modes, or a set of odd. We only ever address one of these subsets at a time, which allows for assumptions about the parity of the cavity modes as a whole.