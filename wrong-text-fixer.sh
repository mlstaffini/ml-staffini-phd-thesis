#!/bin/bash

echo "Replacing $1 with $2"

grep -rl "$1" . | xargs sed -i "s/$1/$2/g"
