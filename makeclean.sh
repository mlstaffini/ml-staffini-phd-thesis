#!/bin/bash

echo "Deleted:"
for i in "out" "bbl" "toc" "gz" "aux" "blg" "~"
do
    #find . -type f -name '*$i' -delete
    #echo "Deleted all $i extension."
    find . -name "*$i" -type f
    find . -name "*$i" -type f -delete
done
echo "Done."
