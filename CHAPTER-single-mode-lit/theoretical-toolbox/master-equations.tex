The standard approach of quantum mechanics to describing collective atom dynamics generally involves closed systems, for which the state of the system follows a unitary evolution, i.e. we are concerned with only the system's evolution, disregarding the possibility of coupling with a bath, undergoing losses, being measured, et cetera.
It should be obvious that a description of an open quantum system suitable for capturing these external, irreversible processes must necessarily contain a non-unitary part, as well as the original unitary evolution of the system in isolation; these processes can be nonconservative, and therefore their description should allow for the option.

There is a standard formalism for a quantum system coupled to a bath, namely that of master equations -- subject to various assumptions and approximations (for example, Markovianity, i.e. the bath lacks a `memory', and weak coupling). In these treatments, one is usually looking to trace out the bath from the overall density matrix of the system, to obtain an equation for the atomic density matrix only. We start by introducing the bare bones of this formalism, as it is such a common language for open quantum systems, and from there discuss the subtleties necessary for applying/generalising this formalism in a way that suits atoms in a cavity.

\subsubsection{Sketching a derivation}

To begin the discussion, we approach the problem in the `standard' way, that is considering a system coupled to a bath -- we will adapt this to our cavity necessities in a moment.

Consider the case of a system $S$, with density matrix $\rho_S$, coupled to an external bath $B$: we can describe the overall Hamiltonian in terms of that of S and B separately, plus an interaction Hamiltonian, $H=H_S+H_B+\alpha H_{SB}$, where $\alpha$ is an interaction strength.
The Liouville-von Neumann equation for $S+B$ on the total density matrix $\rho_{SB}$ is:

\begin{equation}
i \hbar \frac{\partial}{\partial t} \rho_{SB}= [H_S+H_B+\alpha H_{SB},\rho_{SB}]
\end{equation}

We know that this contains the unitary parts describing the internal evolutions of system and bath; we trace out the bath density matrix $\rho_S=tr_B{\rho_{SB}}$\cite{Pearle2012,Brasil2012}, to obtain a Lindblad equation\cite{Scully1997,Breuer2002} of the form\footnote{Note that this equation of motion is now non-unitary due to the partial trace `losing' information about the overall unitary evolution.}:

\begin{equation}
\dot{\rho_S}=-\frac{i}{\hbar} [H_S, \rho_S]+\mathcal{D} [\rho_S]
\end{equation}

This clearly shows a unitary part, describing the dynamics within $S$, plus a non-unitary term, which depends on the nature of the coupling. Consider, for example, the form of a non-unitary term for a two level system coupled to a bath; we define a new superoperator:

\begin{equation}
\mathcal{L} [X] (\rho)= \frac{1}{2} (X^{\dagger} \ X  \ \rho -2 X \ \rho \ X^{\dagger}+\rho \ X^{\dagger} X)
\end{equation}

where $X$ is some operator. For example, for a system which can exchange photons with an external bath we would write the non-unitary term as:

\begin{equation}
\mathcal{D}[\rho_S]= \gamma_{\downarrow} \mathcal{L} [a] + \gamma_{\uparrow} \mathcal{L} [a^{\dagger}]
\end{equation}

where $a$ and $a^{\dagger}$ are the field operators of the system, and $\gamma_{\uparrow,\downarrow}$ some rates of exchange. Inspecting this, we can convince ourselves that $\mathcal{L} [a]$ represents losses to the bath ($a$ acting on $\rho_S$) and $\mathcal{L} [a^{\dagger}]$ represents pumping from the bath ($a^{\dagger}$ acting on $\rho_S$).
Crucially, this term encodes the interaction via \textit{just} operators of the system operators Hilbert space, acting on \textit{just} the system density matrix; this equation describes only the evolution of the system $S$.
This makes sense, and provided eliminating the bath is a sensible thing e.g. for a Markovian, weakly coupled system, the Lindblad master equation is a powerful tool in open quantum systems. It can be applied to coupled light-matter systems, e.g. atoms in a cavity, with the consideration that now the system is atoms \textit{and} cavity, and the bath is the environment outside the cavity, i.e. our system Hamiltonian will be of the form $H_S=H_{atoms}+H_{cavity}+H_{atoms,cavity}$.

\wip{Is there a more poignant point to make here that isn't horrid?}