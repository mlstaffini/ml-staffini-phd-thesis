Given that a considerable amount of the present thesis is concerned with phase transitions, we should take a moment to discuss how to effectively characterise and speak of phase transitions -- especially given that an extensive formalism exists for equilibrium phase transition in the form of Landau analysis of the free energy. We will therefore review this briefly, focusing especially on the meaning and qualitative difference of different orders of phase transitions; we'll briefly touch upon types of critical points associated with these; and finally we'll discuss how this relates to open systems beyond the scope of the formalism. 

\subsubsection{Phase transition order}

When we speak of a phase transition, we generally mean the following: the system begins in a disordered state, where the order parameter is zero; a variable is then changed, e.g. temperature $T$, and at a critical value $T_C$ the system undergoes a change into a different state, with a nonzero order parameter $\langle \phi \rangle$\footnote{We follow the notation of \citet{Chaikin1995} chapter 4, which also informs most of this section.}. There are two ways this can occur, either continuously or not -- this determines the transition order.

\input{CHAPTER-single-mode-lit/figure-codes/figure-transition-order-diagram}

In a first order phase transition, the value of the order parameter jumps from $\langle \phi \rangle =0$ in the disordered phase, to a nonzero value discontinuously at the transition point, Fig. \ref{fig:first-order-diagram}. The first derivative of the free energy in terms of the order parameter is discontinous at $T_C$.

In second order phase transitions, the order parameter $\langle \phi \rangle$ begins growing continuously from zero at $T_C$, Fig. \ref{fig:second-order-diagram}. The first derivative of the free energy is also continuous, while the second derivative has a discontinuity.

In characterising a phase transition, we need to also discuss the associated critical points. These are points on the phase diagram where derivatives of the free energy are singular. We use the term\textit{ quantum critical point} (and transition) to refer to transitions occurring at zero temperature.

We are mainly concerned here with two types of critical points: critical endpoints, and multicritical points.

A critical endpoint is the point at which a second order transition line ends; a multicritical points is a point at which a number of second order transition lines meet (bicritical points for two, tricritical for three and so on) and become a first order line.

\input{CHAPTER-single-mode-lit/figure-codes/figure-3d-tcp}

Some care needs to be applied in counting the second order lines. In Fig. \ref{fig:3d-tcp}, there is a tricritical point (TCP), but on the $h=0$ plane, this looks as if a single second order line becoming a first order one. Several places in literature use the label of tricritical point  in this sense for two-dimensional phase diagrams (e.g. Ref. \cite{Griffiths1970} as a textbook example, and from literature covered in this thesis, Refs. \cite{Keeling2010, Bhaseen2012,Keeling2014,Jin2018,Soriente2018}), and we will do the same in the context of our own results.

\subsubsection{Landau theory}

We briefly discuss how to treat the free energy landscape analytically with the Landau expansion \cite{Siegman1986}, which consists of writing out the free energy of a system as an expansion in terms of the powers of the order parameter of the transition.

%It is a mean field theory -- that is, it assumes that the field is uniform, i.e. that the effect of the field on one of the particles can be captured fully by writing every other particle as a uniform background.
% COMMENT TO SELF: this is not necessarily true, grad term in expansion -- coarse grained gradient expansion.

Landau theory also assumes that the order parameter is small around the transition, which lets us truncate the expansion to lower powers of the order parameter. This is obviously very good in the case of second order transitions where the order parameter is small around $T_C$; however, it also yields information about first order transitions when used discerningly, if not quantitatively, then qualitatively about the properties of the ordered phase.

Other than this, the Landau expansion terms are entirely determined by the symmetries of the ordered phase (and how many terms need to be included for the free energy expansion to be bounded from below).

In practice, we write a local free energy $f(X,\phi)$, where $\phi$ is the order parameter, $X$ is the model variable, and Taylor expand on the assumption that it is small around transition. The linear term in the free energy generally needs to be absent because of thermodynamical considerations\footnote{See p.151 on in Chaikin and Lubensky \cite{Chaikin1995}.}. Because each term of the expansion need to be invariant under the symmetries of the disordered phase (as the full function \textit{must} be so too), the odd terms are often absent. One should also consider the sign of the highest term in the truncation -- it must be positive in order for the free energy to be bounded, which informs the choice of power at which to truncate.

In general, one works with a function of the form:

\begin{equation}
f(X,\phi)=\frac{1}{2} r \phi^2 + u \phi^4 + ...
\end{equation}

where the coefficients $r$ and $u$ depend on the model, and can depend on $X$. It is then a matter of analysing the signs of the coefficients to determine phases. There will determine the shape of the free energy landscape like in Fig. \ref{fig:order-f}.

\input{CHAPTER-single-mode-lit/figure-codes/figure-transition-order-f}

If the fourth order term is positive, then we can neglect the sixth order term, as $u>0$ is sufficient for stability. The free energy looks like Fig. \ref{fig:second-order-f}; here, the global minimum continuously becomes a maximum, so the system `slides gently' into one of new minima, breaking the symmetry in making a choice.  

If $u<0$ the sixth order term is required to maintain stability, and the potential can look like Fig. \ref{fig:first-order-f}; this is a first order transition as the minimum corresponding to the normal state remains a minimum as new minima appear, and only once the new minima have become energetically favourable does the system switch, breaking symmetry and taking up the nonzero order parameter of the new minima.

\subsubsection{Phase transitions in open systems}

Until now, we've discussed phase transition in closed systems, where the free energy is well defined and one can straightforwardly apply the machinery of thermodynamical phase transitions. The ground state is the global minimum of the free energy, and it changes with the changing free energy landscape.

In an open system with pumping and losses we need to approach the discussion differently -- and the matter of phase transition order in open systems is a relatively new field of discussion, see e.g. \cite{Minganti2018}. 

The collective behaviour of open quantum systems can be treated in the language of dynamical systems; these can have a number of types of attractors (i.e. asymptotic end states of the dynamics) and the idea of a ground state does not readily apply. However, the nature of the transitions between these dynamical attractors can follow equivalent considerations to those for equilibrium transitions: the order parameters of the system can still show continuous growth or jumps, which we can classify using the first- or second-order language; critical points can then be catalogued similarly by using the order of the dynamical transitions defined this way.

In driven dissipative systems, one cannot straight-forwardly compare the values of the free energy; the competition between losses and pumping drives the choice of which attractor of the dynamics is favoured, so every transition has an associated switching rate; one can talk about this rate, instead of the free energy minimisation which does not translate to open systems.

Mean field theoretical approaches to dynamical systems show the presence of multistability in first order phase transitions. This is similar to their closed system analogue: around the transition line, there are multiple local minima available at once. In open systems, this multistability can be associated with hysteretic behaviour\cite{Kessler2012,Rodriguez2017} as the switching rate needs to be high enough for the system to switch out of the state it is currently in within the timescale of the experiment.

%A more general treatment \cite{Minganti2018} shows that the bistability in first order transitions is not associated with bimodality of the density matrix (i.e. a weighted mixture of the two states): the density matrix is bimodal only at the critical point. \wip{further cite or refer to or explain?}