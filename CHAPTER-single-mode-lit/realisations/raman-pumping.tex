\subsubsection{Theory}

\input{CHAPTER-single-mode-lit/figure-codes/figure-raman-pumping}
\footnotetext{Reprinted from Optica under OSA Publishing gold open access license.}

Consider the theoretical setup proposed by \citet{Dimer2007}: N atoms are prepared in a cavity pumped by two co-propagating laser beams. The cavity is in the single mode regime, in this case only supporting $TEM_{00}$ where we assume beam waists are big enough that the atoms see a homogeneous field. 

Every atom has two stable ground states $\ket{0}$ and $\vert 1 \rangle$, as well as two excited states $\vert r \rangle$ and $\vert s \rangle$. Raman (inelastic) pumping from the external lasers drives the two transitions $\ket{0}\leftrightarrow\ket{s}$ and $\ket{1}\leftrightarrow\ket{r}$; these have Rabi frequencies $\Omega_s$ and $\Omega_{r}$ respectively, and the lasers are red-detuned from excited states $\ket{s}$ and $\ket{r}$ by detunings $\Delta_s$ and $\Delta_r$.

The coupling with the cavity mode is responsible for the transitions $\ket{0}\leftrightarrow\ket{r}$ and $\ket{1}\leftrightarrow\ket{s}$; these occur with coupling strength $g_r$ and $g_s$ respectively.  

The scheme is illustrate in Fig.\ref{fig:raman-pumping}.

We will discuss the four-level scheme, but one should note that it's possible for this pumping scheme to be realised with a single excited state, i.e. with $\ket{s}$ and $\ket{r}$ being the same level. In order to pump the Raman transitions from the two ground states to the excited level independently, these need to be distinct channels, i.e. the ground states cannot be degenerate and therefore detuning $\omega_1$ between the two needs to be non-zero.

We treat this as an open system with the master equation formalism introduced in Sec.\ref{master-equations}: the time evolution of the density matrix depends on the Hamiltonian, and two Lindbladian terms representing cavity losses with rate $\kappa$, and spontaneous emission from the excited states.
The term representing losses can be written as:

\begin{equation}
\label{eq:Dimerlosses}
\mathcal{L}_{cavity}=\kappa (2 \hat{a} \rho_{S} \hat{a}^{\dagger}-\hat{a}^{\dagger}\hat{a}\rho_{S}-\hat{a}\hat{a}^{\dagger} \rho_{S})
\end{equation}

where we can understand this as explained in Sec.\ref{master-equations}.

The Hamiltonian of the system contains a contribution from the Raman pumping transitions, and one from the cavity coupling mediated ones. To obtain a simplified master equation for the ground states $\ket{0}$ and $\ket{1}$ the excited states are adiabatically eliminated -- that is, assuming that the excitation and decay to the excited states happens on a timescale shorter than the time evolution of the ground states, the ground states as continuously evolving\footnote{We will carry out an adiabatic elimination in the context of our model derivation Chapter \ref{sm}, so we omit the finer detail here.}. This involves assuming large detunings from the excited states, i.e. $\Delta_{s,r}$ are much greater than the other frequencies (and therefore on much shorter timescales) in the system, transition couplings $\Omega_{r,s}$ and $g_{r,s}$, decay rate $\kappa$, laser-cavity detuning.

The simplified master equation $\dot{\rho}=-i[H,\rho]+\mathcal{L}_{cavity}$ will involve only the ground state, where the cavity losses Lindblad term is that of Eq.\ref{eq:Dimerlosses}, and where we now have an effective Hamiltonian as in Ref.\cite{Dimer2007}:

\begin{equation}
\hat{H}= \omega \hat{a}^{\dagger} \hat{a} + \omega_0 \hat{J}_z + \delta \hat{a}^{\dagger} \hat{a} \hat{J}_z+ \frac{\lambda_r}{\sqrt{N}} \left( \hat{a} \hat{J}_{+}+ \hat{a}^{\dagger} \hat{J}_{-} \right)
+
\frac{\lambda_s}{\sqrt{N}} \left( \hat{a}^{\dagger} \hat{J}_{+}+ \hat{a} \hat{J}_{-} \right)
\end{equation}

where the parameters $\delta$, $\omega_0$ and $\omega$, $\lambda_{s,r}$ are functions of the transition strengths and detuning so that this equation resembles the Dicke model of Section \ref{dicke-model}; setting co- and counter-rotating term equal (which results in $\delta=0$), and carrying out some algebra, we recover the generalised Dicke model with counter-rotating terms:

\begin{equation}
\label{eq:Dimerdicke}
\hat{H}= \omega \hat{a}^{\dagger} \hat{a} + \omega_0 \hat{J}_z + \frac{\lambda}{\sqrt{N}} \left( \hat{a} + \hat{a}^{\dagger} \right) \left( \hat{J}_{+}+ \hat{J}_{-} \right)
\end{equation}

where $\lambda$ is the effective light-matter coupling $\lambda=\frac{1}{2} \sqrt{N} g \Omega / \Delta$.

In this form of the Dicke model, the coupling parameter $\lambda$ depends on laser frequencies, and is independent of the cavity and light frequencies which yielded the no-go theorem.

\subsubsection{Experimental realisation}

A closely related scheme was realised experimentally by \citet{Zhiqiang2017}. Here, the same Hamiltonian as Eq. \ref{eq:Dimerdicke} is realised by placing a cloud of $^{87} Rb$ atoms trapped via an optical lattice in an optical cavity; the atoms are pumped by counter-propagating laser beams. 

The setup uses the $5 P_{3/2}$ state of the rubidium atoms as the excited state (as discussed above, a single excited state can also mediate the effective two-level system transition). The ground states are the magnetic sublevels of the lowest hyperfine level of the $5 S_{1/2}$ state, as in Fig. \ref{fig:barrett}. Note that this paper uses \textit{three} ground state levels, $\ket{-1}$, $\ket{0}$ and $\ket{1}$, where usually the Dicke model uses two; they are simply treated by using the same operator for `moving up', i.e. from $\ket{-1}$ to $\ket{0}$ and from $\ket{0}$ to $\ket{1}$, and likewise for moving down and counting, into the usual Dicke model:

\begin{equation}
H=\omega a^{\dagger} a +\omega_0 J_z + \frac{\lambda_-}{\sqrt{2N}}(a J_{-}+a^{\dagger} J_{+})+\frac{\lambda_{+}}{\sqrt{2N}}(a J_{+}+a^{\dagger} J_{-})
\end{equation}

with the operators defined as:

\begin{equation}
J_{\pm}=\sum_{j}^N \sqrt{2} (\ket{\pm 1_j}\bra{0_j}+\ket{0_j}\bra{\mp 1_j})
\end{equation}
\begin{equation}
J_{z}=\sum_{j}^N (\ket{ 1_j}\bra{1_j}-\ket{-1_j}\bra{- 1_j})
\end{equation}

The Zeeman splitting between levels corresponds to a magnetic field of $\approx .225 mT$ applied transversally. The detuning to the excited state is large, so one can ignore its hyperfine structure (and adiabatically eliminate the excited state in the theory).

Like in the theoretical model above, this realisation allows for a tunable Dicke model, where the co- and counter-rotating coupling coefficients $\lambda_{-}$ and $\lambda_+$ can be tuned independently.

The phase diagram shows a normal phase and a superradiance phase qualitatively like the theoretical Dicke model, as well as a section of oscillatory behaviour. It is worth noting that the recorded results, the single-photon counting module (SPCM) has a time gate that makes interpretation of the oscillations limited; however, they are present for the theoretical simulations as well, though these are run on the same timescale as the experiment ($\approx 1 ms$).

These oscillations are similar to those from Ref. \cite{Bhaseen2012}, which precedes \citet{Zhiqiang2017}, but which we will discuss in Section \ref{momentum-states}.