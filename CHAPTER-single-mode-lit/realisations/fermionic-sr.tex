Collective enhancement of light coupling to fermions in a cavity has been studied extensively\cite{Ketterle2001, Larson2008}, including collective atomic recoil motion\cite{Wang2011} and observing the BCS-BEC transition\cite{Guo2012}.

However, in the spirit of the Dicke realisation, one can also extend a Dicke-like model to spinless fermions as in Ref. \cite{Keeling2014}, to investigate whether a superradiant phase survives and other difference arise. 

Setting up the atoms in a single mode, transversaly pumped cavity as before, one can expect this to behave the same for finite (high) temperature superradiance, where both fermions and bosons obey statistical physics. At low temperatures, however, where the different statistics of fermions and bosons are relevant, a few key differences arise that make the question of whether a self-organisation is presents for fermions nontrivial.

Firstly, we have another lengthscale in the problem, the Fermi wavevector, on top of the lengthscale of the optical lattice, which may influence things.

Another fundamental difference is that while the bosonic case is almost always treated in some form of two-level approximation, Pauli blocking means we can never consider this approximation for fermions. The fermionic problem will, by necessity, be a multilevel problem.

Given these considerations, the presence and nature of superradiance in a fermionic setup are not obvious.

We should point out that while here we discuss the qualitative observations and results of \citet{Keeling2014}, the exact Hamiltonian presented in Ref.\cite{Keeling2014} (albeit with bosonic statistics) will be the starting point of our single mode results in Chapter \ref{sm}; instead of an an approximate treatment twice, we defer a full derivation and analytic treatment of the model itself util then.

\input{CHAPTER-single-mode-lit/figure-codes/figure-fermionic}
\footnotetext{Reprinted figure with permission from Keeling, J., Bhaseen, M. J. \& Simons, B. D., Fermionic Superradiance in a Transversely Pumped Optical Cavity. Phys. Rev. Lett. 112, 143002 (2014). Copyright 2014 by the American Physical Society.}

The equilibrium phase diagrams, Fig.\ref{fig:fermionic} for this fermionic system show a `superradiant' phase where there is a nonzero cavity field and the atoms are self-organised in a lattice. 
However, there is an additional feature: below a critical detuning, the transition becomes first order (see Sec.\ref{phase-transitions}). 
In particular, in the case of a half-filled first band, the second order line meets the first order line in a tricritical point (see Section \ref{phase-transitions}).

The presence of this first order transition can be shown in terms of a Landau exansion (see Section \ref{phase-transitions}) of the free energy. The expansion is shown explicitly in the supplemental material of Ref.\cite{Keeling2014}, where it is pointed out that in the limit $k_F<q$ (i.e. the Fermi wavevector is smaller than the lattice spacing) the sign of the coefficients in the Laundau expansion (which determined the transition order) apply to the bosonic case as well. This is because the result obtained -- that there is a first order transition -- is in a low density approximation. The sparseness means one can neglect Pauli blocking; in other words (while the derivation itself would have a different form) the low density result should be the same for a bosonic system.

However, the first order transition hasn't been identified in any previous discussion of bosonic realisations. It is not present in Dicke model descriptions, because it requires hybridisation with higher momentum states, which the Dicke treatment ignores; but it is also not present in literature which does consider multilevel bosonic systems \cite{Bhaseen2012,Piazza2013,Klinder2015}, because these are linearised treatments which preclude seeing first order transitions. We discuss this further in Section \ref{sm-preliminary}.

\subsubsection{Unstable region}

Notice, also, that in Fig.\ref{fig:fermionic} there is an unstable region of the phase diagram. This is because, below this cutoff, the free energy is unbounded from below --- that is, it can be decreased infinitely. This means that there can be no equilibrium ground state; it does not, however, speak as to the absence of possible non-equilibrium steady states. The unstable region is present in the low density limits too, and is therefore not dependent on Pauli blocking; this means that it should be present for the ground state bosonic model too, as seen in Ref. \cite{Liu2011}. 

In an open system treatment, however, the cavity field cannot acquire infinite photon population due to cavity losses; this region corresponds to nontrivial behaviour in the open system treatments we reviewed in Section \ref{momentum-states}, Refs. \cite{Baumann2010} and \cite{Bhaseen2012}. We will discuss this `unstable' region again in Chapter \ref{sm} in the context of our own results. 
