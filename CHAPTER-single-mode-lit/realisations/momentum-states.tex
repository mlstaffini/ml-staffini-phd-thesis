Another proposed realisation of Dicke model superradiance is using momentum state of a Bose-condensed atom cloud as an effective two-level system. By nature, this approach focuses much more closely on the spatial features of the superradiance in a single model, transversely pumped cavity. We have discussed the self-organisation into a checkerboard lattice that is present in thermal atoms in Sec. \ref{thermal-atoms}, and zero temperature self-organisation using Raman pumping schemes in atomic clouds in optical cavities in Sec. \ref{raman-pumping}.

Given the same setup -- a single mode, transversely pumped cavity -- the same arguments of collective occupation of even or odd antinodes of the cavity field should and does apply to an atomic cloud of Bose-condensed atoms at zero temperature; however, the resulting self-organised transition maintains superfluid order too, i.e. it realises supersolidity.

While the three- or four-level scheme we have covered in the previous section allows for controlling the strengths of the co- and counter-rotating terms of the Dicke model independently (i.e. it allows for the realisation of the \textit{generalised} Dicke model) this single-pump setup only allows for the couplings of each term to be the same, i.e. $g=g'$ as in Section \ref{dicke-derivation}.

We begin by reviewing an experimental realisation of the Dicke superradiance using BEC momentum states by \citet{Baumann2010}. Bose-condensed atoms are trapped in an ultra high finesse cavity\footnote{A detailed explanation of the experimental setup can be found in Methods in \citet{Baumann2010}, and Refs. \cite{Ottl2006,Brennecke2007}.}, and pumped from the side; $\hvect{x}$ denoted the cavity direction, while $\hvect{z}$ denotes the pump direction. The results are obtained by directly measuring the light leaking out of the cavity with single-photon counters, so that the atomic distribution in momentum space can be observed.

\input{CHAPTER-single-mode-lit/figure-codes/figure-esslinger}
\footnotetext{Reprinted by permission from Springer Nature Customer Service Centre GmbH: Springer Nature, Dicke quantum phase transition with a superfluid gas in an optical cavity, Baumann, K., Guerlin, C., Brennecke, F. \& Esslinger, T.  Nature 464, 1301–1306 (2010).}

While this is chiefly an experimental realisation, the theory is easy to follow and draws a direct link to what we have covered so far.

The Hamiltonian for the initial setup is\footnote{Note that in this report we consistently use $\hvect{z}$ for the cavity axis, and $x\hvect{x}$ and $\hvect{y}$ for the transverse directions, with $\hvect{x}$ being the transverse pump direction; Ref. \cite{Baumann} inverts $\hvect{x}$ and $\hvect{z}$, so for comparison with the original paper these need to be swapped in what follows.}:

\begin{align}
\label{eq:EsslingerH}
\hvect{H}=\frac{\hvect{p}^2_x + \hvect{p}^2_z}{2 m} & +V_0 \cos^2(k \hvect{x})
+ \hbar \eta (\hvect{a}^{\dagger}+\hvect{a}) \cos(k\vect{x})  \cos(k\vect{z})
 -\hbar \left(\Delta_c-U_0 \cos^2(k \vect{z}) \right) \hvect{a}^{\dagger} \hvect{a}
\end{align}

$\Delta_c=\omega_p-\omega_c$ is the pump-cavity detuning; $\hvect{p}$ are the momentum operators and $\hvect{a}$ and $\hvect{a}^{\dagger}$ are the ladder operators for the light field; the potential is a standing wave in $\hvect{z}$ with amplitude $V_0=\hbar \Omega_p^2/\Delta_a$, and the scattering coefficient $\eta=g_0 \Omega_p/\Delta_a$, where $\Omega_P$ is the Rabi frequency. $U_0=g_0^2/\Delta_a$ in the last term represents  the maximal shift in the light field due to atoms. 

The collective transition can be treated by mean field theory assuming a product state for atoms and photons. There are two possible values of the phase between atoms and cavity, with a $\pi$ difference, depending on whether the atoms are localised on the even or odd sublattice of the potential $\cos (kx) \cos (kz) = \pm 1$. The backaction of the atoms in turn affects the cavity field; the combination of these factors create a runaway process where the atoms are further drawn to the sublattice they begin favouring, which deepens the light potential, which draws atoms further, until they are localised on the sublattice and the kinetic energy cost outweighs the potential energy gain.

One can directly link this to the Dicke model: in the setup, the pump frequency $\omega_p$ is much lower than the atomic frequency $\omega_a$, such that $\Delta_a=\omega_p-\omega_a$ is large and the excited states of the atoms can be adiabatically eliminated, leaving an effective two-level system of the two lowest momentum states (that are in the atomic ground state), $\ket{0, 0}$ and $\ket{\pm \hbar k, \pm \hbar k}$, Fig. \ref{fig:esslinger-atom-levels}, \footnote{We expand further on this level configuration in the context of our results in Chapter \ref{sm}.}. To transition between the ground and first excited momentum states, the system can either absorb a pump photon and emit in the cavity $a^{\dagger} J_+$, or vice versa, where $J_+=J_-^{\dagger}=\sum \ket{\pm \hbar k, \pm \hbar k}_{i \ i} \bra{0, 0}$; this can be written as:

\begin{equation}
\frac{\hbar \lambda}{\sqrt{N}} (\hvect{a}^{\dagger}+\hvect{a}) (\hvect{J}_+ + \hvect{J}_{-})
\end{equation}

which is exactly the Dicke interaction Hamiltonian, which has a transition when coupling strength $\lambda=\sqrt{\omega_0 \omega}/2$. In this realisation, $\omega_0=2 \omega_r$, $\omega=\Delta_c+U_0 N /2$, and $\lambda=\eta \sqrt{N}/2$, and the self-organisation corresponds to the macroscopic occupation of the cavity field in the Dicke model\footnote{Comparing this to the Dicke Hamiltonians we've presented so far, the qualitative difference in description should be obvious: compared to the Hamiltonians in terms of spin so far, this describes the spatial structure of light field of pump and cavity explicitly via the cosine terms, and therefore will capture the spatial self-organisation in a more explicit language.}.

To show this explicitly, one can write the many-body Hamiltonian for the system as:

\begin{equation}
\hvect{H}=\int \mathrm{d} x \mathrm{d} z \hvect{\Psi}^{\dagger} (x,z) \hvect{H}_{(1)} \hvect{\Psi} (x,z)
\end{equation}

where $H_{(1)}$ is the single particle Hamiltonian of Eq.\ref{eq:EsslingerH}, then explicitly restrict the Hilbert space of operator $\hvect{\Psi}$ to two levels, $\psi_0$ and $\psi_1 \varpropto \psi_0 \cos(kx) \cos (kz)$, where $\psi_0$ is the ground state of the one dimensional lattice Hamiltonian without interactions, $\hvect{p}^2_z/2 m+V_0 \cos^2(k \hvect{z})$. Writing $\hvect{\Psi}=\psi_0 \hvect{c}_0+\psi_1 \hvect{c}_1$, where $\hvect{c}_{0,1}$ are the atomic mode operators, one obtains from the above (up to a constant):

\begin{equation}
\frac{1}{\hbar}\hvect{H} = \omega_0 \hvect{J}_z+\omega \hvect{a}^{\dagger}\hvect{a}+
\frac{\lambda}{\sqrt{N}} (\hvect{a}^{\dagger}+\hvect{a}) (\hvect{J}_+ + \hvect{J}_{-})
+U_0 M \hvect{c}_{1}^{\dagger} \hvect{c}_{1}\hvect{a}^{\dagger}\hvect{a}
\end{equation}

where $\hvect{J}_+=\hvect{J}_-^{\dagger}=\hvect{c}_{1}^{\dagger} \hvect{c}_{0}$, $\hvect{J}_z=(\hvect{c}_{1}^{\dagger} \hvect{c}_{1}-\hvect{c}_{0}^{\dagger} \hvect{c}_{0})/2$. This is exaclty the Dicke Hamiltonian, plus the last term, which represents the dispersive cavity shift which can be neglected near the transition; the symmetry breaking of the Dicke Hamiltonian, $\hvect{a}\rightarrow -\hvect{a}$ and $\hvect{J}_{\pm}\rightarrow -\hvect{J}_{\pm}$, corresponds to the self-organisation into a chequerboard sublattice at even or odd sites.

The self organisation can be observed by turning up the pump power, i.e. increasing $\nu$, until the critical coupling strength above is achieved. Above this critical pump, the cavity photon number increases suddenly, and the momentum  distribution changes to show the localised occupation of momentum states \ref{fig:esslinger-atom-levels}, which directly shows the density modulation into one of the even or odd sublattices.

The self-organised BEC has both long range phase coherence and long range periodic density, and therefore can be considered a supersolid\cite{Andreev1969,Chester1970,Leggett1970}; the long range, atom-atom interaction that yields solidity is mediated by the cavity, and is really a discrete symmetry breaking between the two possible phase differences between pump and cavity. Unlike in optical lattice setups, the solid ordering of the atoms is emergent. The superfluid order of the BEC isn't lost when this happens.

Experimentally, cavity photon losses due to spontaneous scattering and heating can be accounted for and do not impede the realisation. When the emergent lattice becomes strong, dephasing of the atoms (confined in the lattice points in small tubes along the weakly trapped third dimension $\hvect{y}$) is expected and observed; turning down the pump, and hence decreasing the number of cavity photons and therefore the lattice depth, restores long range superfluid order.

The phase diagram of photon number as a function of pump power and detuning $\Delta_c$ is shown in \ref{fig:esslinger}. The phase transition curve agrees with the theory drawn from the Dicke model analogy, $\lambda_{cr}=\frac{1}{2}\sqrt{\frac{\omega^2+\kappa^2}{\omega}}\omega_0$, and shows the characteristic shape of the region of self-organisation. 

A similar relation between the dynamics of momentum states of a BEC in an optical cavity  and the Dicke phase transition in shown in Ref.\cite{Nagy2010d}; starting from a similar Hamiltonian:

\begin{equation}
H=-\Delta_C a^{\dagger} a + \int \mathrm{d} x \Psi^{\dagger}(x) \left[ 
-\frac{\hbar}{2 m} \frac{d^2}{dx^2}+U_0 a^{\dagger} a \ \cos (kx) + i \eta \cos (kx) (a^{\dagger}-a)
\right] \Psi (x) 
\end{equation}

where quantities have the same meaning as above, and using a variety of theoretical methods, the self-organisation is shown to realise the Dicke superradiance transition, and the effect of backaction on the solidity of the ground state is shown. Once again, it is pointed out that these types of realisations put the critical regime in an experimentally accessible parameter range.

\subsubsection{Nonequilibrium Dicke model}

We want to cover a last realisation by \citet{Bhaseen2012}; this is more generally about dynamical, explicitly nonequilibrium Dicke model, but is motivated by the experiment by \citet{Baumann2010}, and obtains a phase diagram that can be directly related to that of the experimental results. We will not cover the detail of the treatment, but simply illustrate additions to the phase diagram which we want to keep in mind going forward.

The Hamiltonian used here is similar to the collective spin Dicke Hamiltonian we covered in Section \ref{dicke-model}, plus a back-reaction term which works similarly to the $U_0$ terms in Refs.\cite{Baumann2010} and \cite{Nagy2010d}:

\begin{equation}
\label{eq:BhaseenH}
H=\omega a^{\dagger} a + \omega_0 S_z + U S_z a^{\dagger} a + g(a^{\dagger} S^{-}+a S^{+})+g'(a^{\dagger} S^{+}+a S^{-})
\end{equation}

The dynamics are then described by the Liuvillian:

\begin{equation}
\partial_t \rho = -i [H,\rho] - \kappa (\psi^{\dagger} \psi \rho-2\psi
\rho \psi^{\dagger}+\rho \psi^{\dagger} \psi)
\end{equation}

while this Hamiltonian is the generalised Dicke model, we will only cover the results where $g=g'$ here.

The parameters presented in the Dicke model equation are obtained from a microscopic derivation similar in spirit to Ref.\cite{Baumann2010}; one obtains $\omega_0=2\omega_r$, and $\omega=\omega_c-\omega_p+(5/8)N g_0^2/(\omega_a-\omega_c)$, where $U=-(1/4)g_0^2/(\omega_a-\omega_c)$ is the empty cavity frequency. The coefficients of the back-action terms are calculated considering the overlap of the intensities of atoms and light field. 

There's a slight difference in definition between \cite{Baumann2010} and \cite{Bhaseen2012}, to do with the way atomic excitations are counted: Ref.  \cite{Baumann2010} counts the number of atomic excitations as opposed to the total spin. 

To complete the connection, in large pump-atom detuning of \citet{Baumann2010}, one can write $g=g'=g_0\Omega_p/2(\omega_p-\omega_a)$.

We follow the derivation of Eq. \ref{eq:BhaseenH} from the microscopic description of the system presented in Ref .\cite{Bhaseen2012} in full detail in Chapter \ref{sm}.

\input{CHAPTER-single-mode-lit/figure-codes/figure-noneq-dicke}
\footnotetext{Adapted with permission from 1. Bhaseen, M. J., Mayoh, J., Simons, B. D. \& Keeling, J. Dynamics of nonequilibrium Dicke models. Phys. Rev. A 85, 013817 (2012). Copyright 2012 by the American Physical Society.}

The phase diagram in the regime of negative $U$, Fig. \ref{fig:noneq-dicke}, which is also the experimental regime, shows a second type of superradiance available; this is differentiated from the `standard' one (SRA) by only being available for $U<0$ and having an entirely imaginary light field\footnote{The nature of SRA and SRB attractors also differs, but we will not cover the detail of this observation.}. SRA and SRB should coexist in parts of the parameters space. The boundary of SRA yields a similar curve to the phase diagram of \citet{Baumann2010}. Note that the $\Delta_c$ on the $y$-axis of Ref. \cite{Bhaseen2012} is equal (up to a constant offset) to $-\omega$, so should be flipped when comparing.


In the regime of positive $U$, which is entirely absent from Ref.\citet{Baumann2010}, the phase diagram shows persistent oscillations which never go to a steady state; however, we focus in the experimental regime, so we defer the detail of this to the paper itself.

A last point is worth highlighting: in considering the dynamics carefully, \citet{Bhaseen2012} observe that the timescales connected to the novel phases observed vary considerably; in other words, the duration of current experiments may not be long enough to reach some of the asymptotic states.

We conclude by noting that both \citet{Baumann2010} and \citet{Bhaseen2012} observe that a treatment beyond two-level approximation would be valuable, as including higher order momentum levels could substantially alter the conclusions drawn from the theory regarding the steady states of the system. We will go on to compare our results of doing so to the results presented here in Chapter \ref{sm}.


\subsubsection{Hysteresis}
\label{sm-hysteresis}

\input{CHAPTER-single-mode-lit/figure-codes/figure-hemmerich}
\footnotetext{Reprinted from Klinder, J., Keßler, H., Wolke, M., Mathey, L. \& Hemmerich, A. Dynamical phase transition in the open Dicke model. Proc. Natl. Acad. Sci. U. S. A. 112, 3290–3295 (2015) as noncommercial use under exclusive PNAS License to Publish.}

Let us conclude the discussion of the Bose-condensed realisations of the Dicke model by briefly discussing the superradiance realised by \citet{Klinder2015} (following previous work with travelling wave pump setups\cite{Gorlitz1997, Nagorny2003}): here, on top of observing the characteristic phase diagram Fig.\ref{fig:hemmerich}, hysteresis was also reported. Before discussing hysteresis, we note that this phase diagram presents the same features as Ref. \cite{Baumann2010} as expected.% the discrepancies with the equilibrium Dicke model (theoretical diagram) are due to the ulta-narrow cavity bandwidth, as this makes timescales of dynamics and dissipation similar, and the nonequilibrium effects are substantial. I DONT QUITE GET THE DIFFERENCE THEN

Fig. \ref{fig:hemmerich-hyst} shows the measured hysteresis loop of light intensity versus pump lattice depth (i.e. pump intensity); as the pump lattice depth is increased past the transition point, a jump occurs, whereas when it is decreased the measured intensity goes back down continuously.

The hysteresis -- complemented by mean field calculations of critical exponents -- is interpreted in the context of the Kibble-Zurek model for second-order transitions\cite{Kibble1976,Zurek1985,DelCampo2013}, which is about the progressive freezing of dynamics as a critical point is approached, until the adiabatic approximation is invalid \textit{at} the critical point. In other words, the transition is interpreted as a continuous transition exhibiting hysteresis due to critical slowing down of the dynamics in a second order phase transition. The calculated KZ exponents are fitted to data and appear consistent. We shall discuss this interpretation further in Chapter \ref{sm} in light of our results.
