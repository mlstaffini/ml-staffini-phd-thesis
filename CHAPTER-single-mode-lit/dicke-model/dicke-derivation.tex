We begin the discussion of self-organisation of atoms in a single mode optical cavity by discussing the Dicke model: a well-known theoretical model describing the behaviour of $N$ atoms coupled to one cavity mode. The Dicke model is directly or tangentially related to a lot of research in the field, and it is worth discussing its subtleties in some detail before moving on to realisations of superradiance. 

The Dicke model was attributed to Dicke by Hepp and Lieb\cite{Hepp1973d}, who cited a paper which formally refers to coherent emission of atoms in free space\cite{Dicke1954}; for this reason the Hamiltonian is sometimes therefore called the Dicke-Hepp-Lieb model. The model was then articulated in a less mathematical style by \citeauthor{Wang1973} \cite{Wang1973}.

The Hamiltonian, including the counter-rotating terms --- that is, the terms that allow for creation of a photon and excitation of an atom at the same time, or absorption and de-excitation --- reads as follows:\footnote{In this report, we will introduce the Dicke model in a sufficient but not exhaustive amount of detail; for a more complete introduction of aspects beyond the ones presented here see for example the reviews \cite{Garraway2011} and \cite{Kirton2019}.}

\begin{equation}
\label{eq:Dickeladder}
H= \omega a^{\dagger} a + \omega_z \sum_{i}^N \frac{\sigma_{i}^{z}}{2} +  g \sum_{i}^N (a^{\dagger} \sigma_{i}^-+a \sigma_{i}^+ + a^{\dagger} \sigma_{i}^+ +a \sigma_{i}^-)
\end{equation}

where $g$ is the light-matter coupling; given that the total number of spins is conserved and therefore this Hamiltonian only depends on the total spin operators, we can also write in terms of collective spins:

\begin{equation}
\label{eq:Dicke}
H= \omega a^{\dagger} a + \omega_0 S^z +  g (a^{\dagger} S^-+a S^+ + a^{\dagger} S^+ +a S^-)
\end{equation}

where $\omega$ is the frequency of the light field, $\omega_0$ is the atomic harmonic oscillator frequency, and $\hbar=1$. The spin operators are collective operators defined as:

\begin{equation}
S^\pm=\sum_{i}^N \sigma^{\pm}_{i},  \quad \mathrm{and} \quad S^z=\sum_{i}^N  \sigma^{z}_{i}
\end{equation}

Note that here, rotating and counterrotating terms have the same coupling constant, i.e. if we define a new coupling constant $g'$ for the counterrotating term only (i.e. write the interaction term as $g (\alpha^{\dagger} S^-+\alpha S^+) + g' ( \alpha^{\dagger} S^+ +\alpha S^-)$), then $g'$ is the same as that of the rotating term, $g=g'$. We review the case of the generalised Dicke model for which $g\neq g'$ in Section \ref{tavis-cummings-generalised}.

One can derive the condition for the phase transition in the Dicke model as stated in Eq.\ref{eq:Dicke} (with $g \ne g'$) as a function of model parameters by diagonalising the problem and finding the value of $g$ at which fluctuations about the vacuum state become unstable; this yields: 

\begin{equation}
\label{eq:ineq-dicke-1}
4 N g^2> \omega \omega_0
\end{equation} 

When this condition is satisfied, the system undergoes a \textbf{superradiant phase transition} (SR): the atoms coherently emit into the cavity mode, resulting in a massive photon population in the cavity mode. 

Note that the form of $g$ in the derivation depends on $1/\sqrt{V}$, where $V$ is the mode volume; this means that Eq.\ref{eq:ineq-dicke-1} is essentially a statement about the critical density of the system $N/V$ in order for the superradiant phase to be reached.

\subsubsection{No-go theorem}

However, the Hamiltonian in Eq.\ref{eq:Dicke} is not a full description of the ground state transition.  The form of the Hamiltonian presented above is derived by writing an atomic and electromagnetic (classical) Lagrangian to then quantise. This Lagrangian contains a diamagnetic term in the light-matter coupling, i.e. a term dependent on $A^2$ (where $A$ is the vector potential) \cite{Rzazewski1975} which, on its own, has the effect of increasing the energy cost of creating photons; it is dropped in deriving the Dicke Hamiltonian on the assumption of a dilute system, but in the superradiant phase, where we have macroscopic occupation of a state, this assumption is clearly faulty.

The full Hamiltonian should read:

\begin{equation}
H=H_{Dicke}+\sum_i N D(\hat{a}^{\dagger}+\hat{a})^2
\end{equation}

where \( D=\frac{e^2}{2m_r} \frac{\hbar}{2 \epsilon_0 \omega V} \), and from the same derivation we obtain:

\begin{equation}
\label{eq:dicke-g-def-D}
g=\sqrt{D \omega_0 f}
\end{equation}

where the $f$ is a parameter encoding the strength of couping compared to a harmonic oscillator.
This is the point where we previously chose to neglect the diamagnetic term $D$ itself in the dilute system limit, as it is proportional to $1/V$, so $D\ll 1$ if the system is dilute\footnote{The $g$ terms were kept as they are only dependent on $1/\sqrt{V}$, and therefore more relevant.}. We've pointed out how the transition to superradiance depends on a critical density, so we cannot assume the system is dilute; we therefore need to keep the diamagnetic term. This in turn contributes a term to the transition criterion for the superradiant phase transition:

\begin{equation}
4 N g^2> (\omega + 4ND) \omega_0 
\end{equation} 

From above, Eq.\ref{eq:dicke-g-def-D}, we can write the diamagnetic term as a function of $g$; substituting in this yields:

\begin{equation}
4 N D \omega_0 f > \omega \omega_0 + 4ND \omega_0 \quad \therefore \quad f > 1+ \frac{\omega}{4ND}
\end{equation}

However, there is an identity known as the Thomas-Reiche-Kuhn rule (or oscillator strength rule), which is derived from the double commutator of the atomic Hamiltonian with the dipole moment term and has to do with the maximum response of the system to an applied field (compared to a harmonic oscillator), which states that $f\le 1$.

These two factors put together yield the so-called no-go theorem: the transition criterion in the Dicke model including the diamagnetic term is directly negated by the Thomas-Reiche-Kuhn rule\cite{Bialynicki-Birula1979}. In other words, the phase transition is forbidden in a simple scheme where a two level system is used and the atomic frequency is compared to the coupling strength $g$.

There are realisations in which an \textit{effective} two level system is created which circumvent the no-go theorem. We will review these schemes in detail in Section \ref{sm-realisations}, but we briefly sketch them here for completeness. For example, if a four-level Raman driving scheme is used \cite{Dimer2007}), i.e. a nonequilibrium, coherently driven system where the bottom two levels are the effective Dicke two-level system, this circumvents the Thomas-Reiche-Kuhn sum rule, restoring the possibility of a quantum phase transition in the system (see Section \ref{raman-pumping}). Similarly, one can use the two lowest momentum states of a Bose-Einstein condensate as an effective two-level Dicke model \cite{Nagy2010d,Baumann2010}, where the coupling between these is governed instead by kinetic energy considerations (see Section \ref{momentum-states}). In the cases for which we circumvent the no-go theorem, one can effectively treat the light-matter coupling $g=g'$ as a tunable parameter, and the possibility of a phase transition is restored.\footnote{The case of $g\ne g'$ is more complex and is discussed in Section \ref{tavis-cummings-generalised} as part of the generalised Dicke model.}

More fundamentally, however, we should interrogate whether it is even accurate to talk about the superradiant phase transition in the Dicke model in the ground state. In other words, is the ground state Dicke model as presented a complete and sufficient description around the superradiant transition? The answer is far from trivial, and still debated; in short, however, no.

There is discussion around missing factors that should be present in a ground state Dicke model, to account for the full physical model: a missing $A^2$ in the field term, and a proper description of dipole-dipole interaction\cite{Kirton2019}. Ref. \cite{Vukics2012} suggests that once all factors are properly taken into account, the (modified) Dicke model show a superradiant transition.

There is also the matter of gauge dependence of the model. The field operators are not gauge independent, and to properly compare to experimental results one needs to pick the correct one. A proper choice of gauge actually leads to the conclusion that the ground state Dicke model should show ferroelectric transition rather than a superradiance transition.

