In talking about self-organisation of atoms in a single mode optical cavity, one must necessarily introduce the Dicke model: a theoretical model describing the behaviour of $N$ atoms coupled to a single cavity mode. 

The Dicke model was attributed to Dicke by Hepp and Lieb\cite{Hepp1973d}, who cited a paper which formally refers to coherent emission of atoms in free space\cite{Dicke1954}; for this reason the Hamiltonian is sometimes therefore called the Dicke-Hepp-Lieb model. The model was then articulated in a less mathematical style by \citeauthor{Wang1973} \cite{Wang1973}.

The Hamiltonian, including the counter-rotating terms --- that is, the terms that allow for creation of a photon and excitation of an atom at the same time, or absorption and de-excitation --- reads as follows:\footnote{In this report, we will introduce the Dicke model in a sufficient but not exhaustive amount of detail; for a more complete introduction of aspects beyond the ones presented here see for example the reviews \cite{Garraway2011} and \cite{Kirton2019}.}

\wip{why no dissipation in lit review paper?}

\begin{equation}
\label{eq:Dickeladder}
H= \omega \alpha^{\dagger} \alpha + \omega_z \sum_{i}^N \sigma_{i}^{z} +  \frac{\lambda}{\sqrt{N}} \sum_{i}^N (\alpha^{\dagger} \sigma_{i}^-+\alpha \sigma_{i}^+ + \alpha^{\dagger} \sigma_{i}^+ +\alpha \sigma_{i}^-)
\end{equation}

where $\lambda$ is the atom-photon coupling; given that the total number of spins is conserved and therefore this Hamiltonian only depends on the total spin operators, we can also write in terms of collective spins:

\begin{equation}
\label{eq:Dicke}
H= \omega \alpha^{\dagger} \alpha + \omega_0 S^z +  g (\alpha^{\dagger} S^-+\alpha S^+ + \alpha^{\dagger} S^+ +\alpha S^-)
\end{equation}

where $\omega$ is the frequency of the light field, $omega_0$ is the atomic harmonic oscillator frequency, $g$ is the collective light-matter coupling constants, and $\hbar=1$. The spin operators are collective operators defined as:

\begin{equation}
S^\pm=\sum_{i}^N S^{(i)}_{\pm},  \quad \mathrm{and} \quad S^z=\sum_{i}^N S^{(i)}_{z}
\end{equation}

Note that here, rotating and counterrotating terms have the same coupling constant, i.e. the coupling constant $g'$ for the counterrotating term is the same as that of the rotating term, $g=g'$. We review the case of the generalised Dicke model for which $g\neq g'$ in \wip{SECTION}.

One can derive the condition for the phase transition in the Dicke model as stated in Eq.\ref{eq:Dicke} (with $g=g'$) as a function of model parameters by diagonalising the problem and finding the solution for which one of the modes crosses zero energy\wip{cite something}; this yields: 

\begin{equation}
4 N g^2> \omega \omega_0
\end{equation} 

When this condition is satisfied, the ground state has a nonzero photon population, which the reader will recognise as a superradiant phase transition similar to that discussed in the context of thermal atoms, i.e. a self-organisation whose hallmark is a nonzero cavity field. 

\subsubsection{No-go theorem}

However, the Hamiltonian in Eq.\ref{eq:Dicke} is not a full description of the system we are describing.  A full treatment of this can be obtain by deriving it from a full atomic and electromagnetism (classical) Lagrangian to then quantise\wip{cite again}. This Lagrangian contains a diamagnetic term in the light-matter coupling, i.e. a term dependent on $A^2$ (where $A$ is the auxiliary field) \cite{Rzazewski1975} which, on its own, has the effect of increasing the energy cost of creating photons; it is dropped in deriving the Dicke Hamiltonian on the assumption of a dilute system, but in the superradiant phase, where we have macroscopic occupation of a state, this assumption is clearly faulty.

The full Hamiltonian should read:

\begin{equation}
H=H_{Dicke}+\sum_i D(\hat{\psi}^{\dagger}+\hat{\psi})^2
\end{equation}

where \( D=\frac{e^2}{2m_r} \frac{\hbar}{2 \epsilon_0 \omega V} \). This contributes a term to the transition criterion for the superradiant phase transition:

\begin{equation}
4 N g^2> (\omega + 4ND) \omega_0 
\end{equation} 

where $g=\sqrt{D \omega_0 f}$. This $f$ is a parameter encoding the strength of couping compared to a harmonic oscillator; this yields:
\begin{equation}
4 N D \omega_0 f > \omega \omega_0 + 4ND \omega_0 \quad \therefore \quad f > 1+ \frac{\omega}{4ND}
\end{equation}

However, there is an identity known as the Thomas-Reiche-Kuhn rule (or oscillator strength rule), which is derived from the double commutator of the atomic Hamiltonian with the dipole moment term and has to do with the maximum response of the system to an applied field (compared to a harmonic oscillator), which states that $f\le 1$.

These two factors put together yield the so-called no-go theorem: the transition criterion in the Dicke model including the diamagnetic term is directly negated by the Thomas-Reiche-Kuhn rule\cite{Bialynicki-Birula1979}. In other words, the phase transition is forbidden in a simple scheme where a two level system is used and the atomic frequency is compared to the coupling strength $g$.
