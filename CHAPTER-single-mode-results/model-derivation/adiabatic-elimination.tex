We can the then rewrite Eq.\ref{eq:sm-H-w-approx} in the form $H=H_0+H_1$, separating the non-interacting ($H_0$) and interacting ($H_1$) parts:


\begin{equation}
\label{eq:sm-sw-H0}
H_0=\Delta_c \ \lightd \light + 
\Delta_a \sum_{i}^{N} \sigma^{ee}_i - 
\frac{\hbar^2 }{2m}\sum_{i}^{N} \nabla_i^2
\end{equation}

and

\begin{equation}
\label{eq:sm-sw-H1}
H_1= \sum_{i}^{N} \Big( g_0 \light \cos (kz_i) 
+ \frac{\Omega_f}{2} e^{ikx_i} +\frac{\Omega_b}{2} e^{-ikx_i}
\Big) \sigma^{eg}_{i} + H.c.
\end{equation}

%%%%%%%%%% NEED X/Z SWAP FROM HERE %%%%%%%%%%%%%%%%%%

As covered in Section \ref{schrieffer-wolff}, we want to define a transformed Hamiltonian $\tilde{H}=H_0+\frac{i}{2}[S,H_1]$ such that $\tilde{H}$ does not have coupling terms between the ground and excited electronic state, which we obtain if $[S,H_0]=i H_1$.

To obtain the structure of $S$, we begin with an $S$ similar in shape to $H_1$, and allow for some coefficient for each term which is what we want to calculate, i.e. using the notation $X^{(n)}$ for the $n$th term of operator $X$, we can build a solution as:

\begin{equation}
\label{eq:sm-S-structure}
[\hat{A}\hat{S}^{(1)}+\hat{B}\hat{S}^{(2)}+\hat{C}\hat{S}^{(3)},H_0]=i (H_1^{(1)}+H_1^{(2)}+H_1^{(3)})
\end{equation}

so that we obtain the pieces of $H_1$ one at a time, $[\hat{A}\hat{S}_{trial}^{(1)},H_0]=i H_1^{(1)}$.
Now, formally, this yields:

\begin{equation*}
[\hat{A}\hat{S}^{(1)},H_0]=\hat{A}[\hat{S}^{(1)},H_0]+[\hat{A},H_0]\hat{S}^{(1)}
\end{equation*}

However, let us define that in writing the products $\hat{A}S^{(1)}$ etc., we choose the operators $\hat{A}$, $\hat{B}$ and $\hat{C}$ such that they commute with $H_0$, so that the second term of the commutators is zero, $[\hat{A},H_0]=[\hat{B},H_0]=0$. 
Using the structure of $H_1$ to identify the $S^{(i)}$ we find:

\begin{align}
\label{SW-S-term1}
[\hat{A}\hat{S}^{(1)},H_0]=\hat{A} [ \light \ e^{i k z_i} \sigma^{eg}_{i},H_0]=i g_0 \light \ e^{i k x_i} \sigma^{eg}_{i} \\
\label{SW-S-term2}
[\hat{B}\hat{S}^{(2)},H_0]=\hat{B}[ e^{ikz_i}\sigma^{eg}_{i},H_0]=i \frac{\Omega_f}{2} e^{ikx_i} \sigma^{eg}_{i} \\
\label{SW-S-term3}
\hat{C} [e^{-ikz_i}\sigma^{eg}_{i},H_0]=i \frac{\Omega_b}{2} e^{-ik x_i}\sigma^{eg}_{i}
\end{align}

Note that we split the $g_0 \light \cos (kx_i)$ term into its exponential components, and we'll need to reassemble it for the full $\hat{S}$.
We can now carry out the commutators Eq. \ref{SW-S-term1}-\ref{SW-S-term3} to obtain $\hat{A}$, $\hat{B}$ and $\hat{C}$ and build $\hat{S}$.

We will show Eq. \ref{SW-S-term1} explicitly to point out a few subtleties, and omit the derivation of the simpler Eq. \ref{SW-S-term2} and \ref{SW-S-term3}.

Writing out Eq. \ref{SW-S-term1}:

\begin{equation}
\hat{A} \big(
\light e^{i k z_i} [\sigma^{eg},H_0]+ \light [e^{i k z_i},H_0] \sigma^{eg}+ [a,H_0] e^{i k z_i} \sigma^{eg}
\big)
\end{equation}

The first and third commutators are straightforward, $[\sigma^{eg},H_0]=-\Delta_a \sigma^{eg}$ and $[\light,H_0]=\Delta_c \light$. The second commutator is a little more involved. It comes down to calculating:

\begin{equation}
\frac{1}{2m}[e^{i k z_i},-\hbar^2 \nabla^2]=\frac{1}{2m} 
\big(
(-i\hbar \nabla)\cdot[e^{ik z_i},-i\hbar \nabla]+[e^{ik z_i},-i\hbar \nabla]\cdot(-i\hbar \nabla)
\big)
\end{equation}

With a little care for the grad terms, we get $[e^{ik z_i},-i\hbar \nabla]=-\hbar k e^{i k z_i}$; then the full commutator is:

\begin{equation}
\label{sm-S-commutator1a}
\frac{1}{2m} \big(
(-i\hbar \nabla)\cdot[e^{ikz_i},-i\hbar \nabla]+[e^{ik z_i},-i\hbar \nabla]\cdot(-i\hbar \nabla)
\big)=\frac{- \hbar^2 k^2}{2m}e^{ikz_i}+\frac{2i\hbar^2k}{2m}e^{ik z_i} \hvect{z} \nabla
\end{equation}

before moving on, we want to re-define coordinates and momenta in terms of units of wavelength $k$, $x\rightarrow kx$, $z \rightarrow kz$, and the derivatives $\partial_x \rightarrow \partial_x /k$, $\partial_z \rightarrow \partial_z /k$.
Rewriting \ref{sm-S-commutator1a}:

\begin{equation}
\label{sm-S-commutator1}
\frac{1}{2m}[e^{i z_i},-\hbar^2 \nabla^2]
=-\omega_r e^{i z_i}+2i \omega_r e^{i z_i}\partial_z
\end{equation}

where we have defined the recoil energy $\omega_r=\frac{\hbar^2 k^2}{2m}$, and losing the other dimension of the nabla as only the $z$-derivative survives.

Substituting Eq. \ref{sm-S-commutator1} into Eq. \ref{SW-S-term1} and swapping signs:

\begin{equation}
-\hat{A} \light e^{i z_i} \big(\Delta_a -\Delta_c 
+\omega_r -2i \omega_r \partial_z
\big)\sigma^{eg}_{i}= i g_0 \light e^{i z_i} \sigma^{eg}_{i}
\end{equation}

Note that doing this for $e^{-i k z_i}$ yields:

\begin{equation}
-\hat{A'} \light e^{-i z_i} \big(\Delta_a -\Delta_c 
+\omega_r +2i \omega_r \partial_z
\big)\sigma^{eg}_{i}= i g_0 \light e^{-i z_i} \sigma^{eg}_{i}
\end{equation}

i.e. the sign of the partial derivative changes, but not the overall factor of $i$, so we don't want to absorb the $-i$ necessary to equal the right hand side into the $\hat{A}$, or we will not be able to define $A'=A^*$ where $\hat{A}^*$ is the complex conjugate of $\hat{A}$.

We instead start putting together $\hat{S}$ with an overall prefactor:

\begin{equation*}
i \hat{S}=\frac{1}{2}g_0 \light (e^{i z_i} \hat{A}+e^{-i z_i}\hat{A^*})\sigma_i^{eg}+...
\end{equation*}

where we have defined:

\begin{equation}
\hat{A}=\frac{1}{\Delta_a -\Delta_c 
+\omega_r (1+2i \partial_z)}
\end{equation}

Calculating Eq. \ref{SW-S-term2} and \ref{SW-S-term3} is essentially done as well, as we've already computed commutators for exponential terms, and $\sigma^{eg}$; the only difference is that we'll pick up a $\partial_x$ instead of $\partial_z$ as the exponential terms are in $x$.

This yields the second differential operator, again leaving the overall $-i$ prefactor out so we get $\hat{C}=\hat{B}^*$\footnote{Note that both $\hat{A}$ and $\hat{B}$ are Hermitian, as the only operator term in them is a derivative, which is Hermitian; this means that while taking the complex conjugate of them is possible, $\hat{A} \ne \hat{A}^*$, they are their own Hermitian conjugate, $\hat{A} = \hat{A}^{\dagger}$, and are unchanged when taking the Hermitian conjugate of terms involving them.}. 

\begin{equation}
\hat{B}=\frac{1}{\Delta_a +\omega_r (1+2i \partial_x)}
\end{equation}

Having obtained the operators to build $\hat{S}$, before continuing we need to check that the assumption we made to write Eqs. \ref{SW-S-term1}, \ref{SW-S-term2} and \ref{SW-S-term3}, that $[\hat{A},H_0]=[\hat{B},H_0]=0$. Both $\hat{A}$ and $\hat{B}$ commute with every term of $H_0$: the only operator in $\hat{A}$ and $\hat{B}$ is the derivative, the first two terms of $H_0$ are only dependent on internal states and not on spatial functions, and the grad squared term does commute with the derivative.

Assembling $\hat{S}$, we get:

\begin{equation}
\label{sm-sw-S}
i \hat{S}=\frac{1}{2} \bigg( g_0 \light (e^{i z_i} \hat{A}+e^{-i z_i}\hat{A^*})\sigma_i^{eg}+\Omega_f e^{i x} \hat{B}+\Omega_b e^{-i x} \hat{B}^{*}
\bigg) \sigma^{eg} - H.c.
\end{equation}

We now can compute the transformed Hamiltonian $\tilde{H}=H_0+\frac{i}{2}[\hat{S},H_1]$. 

We observe that the recoil frequency $\omega_r$ is small compared to the detunings, $\omega_r / \Delta_a \sim 50 \mathrm{kHz}/1 \mathrm{THz}$, and therefore we can approximate $\hat{A}=\frac{1}{\Delta_a -\Delta_c}$ and $\hat{B}=\frac{1}{\Delta_a}$; in practice, this means we do not need to worry about commuting the partial derivatives. 

We are then just left with algebra involving two kinds of commutator only: $[\sigma^{ge},\sigma^{eg}]$, and $[\light \sigma^{eg},\lightd \sigma^{ge}]$ (and their conjugates), with $[\light,\light^{\dagger}]=1$ as normal. 

Remembering that $\sigma^{eg}=\ket{e} \bra{g}$, the first commutator is $[\sigma^{ge},\sigma^{eg}]=\ket{g}\bra{e}\ket{e}\bra{g}-\ket{e} \bra{g}\ket{g}\bra{e}$; we want to only work in the ground state manifold, since we're eliminating the excited state, therefore, for a single atom, $[\sigma^{ge},\sigma^{eg}]=1$, and $[\sigma^{eg},\sigma^{ge}]=-1$.

We can then use this to calculate the second type of commutator:

\begin{equation}
[\lightd \sigma^{ge},a \sigma^{eg}]=\lightd \light [\sigma^{ge}, \sigma^{eg}]+[\lightd,\light] \sigma^{eg}\sigma^{ge}=\lightd \light
\end{equation}

using commutator identity $[AB,C]=A[B,C]+[A,C]B$; the second term is entirely in the excited state manifold, $\sigma^{eg}\sigma^{ge}=\sigma^{ee}$, so we can ignore it. Similarly, $[\light^{\dagger}\sigma^{ge},\light \sigma^{eg}]=1-\light \light^{\dagger}=\light^{\dagger}\light$. 

Working out prefactors, the Hamiltonian for the ground state only reads:

\begin{align}
\label{eq:Bh2010-A4}
\tilde{H}= & H_0-\frac{\Omega_f^2}{4}\hat{B}-\frac{\Omega_b^2}{4}\hat{B}^* -\frac{\Omega_f\Omega_b}{8}\left[ \hat{B} ( e^{2i x}+e^{-2i x})+\hat{B}^* (e^{2i x}+e^{-2i x}) \right] 
\nonumber \\
& -\frac{g_0^2}{8} \lightd \light \left[
(\hat{A}+\hat{A}^*)(1+e^{-2i z})+(\hat{A}+\hat{A}^*) (1+e^{2i z})
\right] 
\nonumber \\
& -\frac{g_0}{8} \lightd (\Omega_f e^{i x}+\Omega_b e^{-ix})(\hat{A} e^{-iz}+\hat{A}^* e^{iz})
-\frac{g_0}{8} \light (\Omega_f e^{-ix}+\Omega_b e^{ix})(\hat{A} e^{iz}+\hat{A}^* e^{-iz}) 
\nonumber \\
& -\frac{g_0}{8}(e^{iz}+ e^{-iz}) \left[
 a (\Omega_f \hat{B} e^{-ix}+\Omega_b \hat{B}^* e^{ix})
 + \light^{\dagger} (\Omega_f \hat{B} e^{ix}+\Omega_b \hat{B}^* e^{-ix})
\right]
\end{align}

Substituting in the approximations $\hat{A}=\frac{1}{\Delta_a -\Delta_c}$ and $\hat{B}=\frac{1}{\Delta_a}$ explicitly, Eq. \ref{eq:Bh2010-A4} becomes:

\begin{align}
\tilde{H}= & H_0-
\frac{\Omega_f^2+\Omega_b^2}{4 \Delta_a}
 -\frac{\Omega_f\Omega_b}{4 \Delta_a}\left(e^{2ix}+e^{-2ix}\right) 
-\frac{g_0^2}{4 (\Delta_a-\Delta_c)} \light^{\dagger}\light \left(
(1+e^{-2iz})+(1+e^{2iz})
\right) 
\nonumber \\
& - \frac{g_0}{8} \left(
\frac{1}{(\Delta_a)}+ \frac{1}{(\Delta_a-\Delta_c)} 
\right) 
\cos (z) 
\left[
\light^{\dagger} (\Omega_f e^{ix}+\Omega_b e^{-ix}) +\light (\Omega_f e^{-ix}+\Omega_b e^{ix}) 
\right]
\end{align}

We can further simplify the two $g_0$ terms with some algebra, to obtain a form explicitly dependent on combinations of $\Omega_f \pm \Omega_b$, so that we can straightforwardly see which terms will drop out when we set $\Omega_f=\Omega_b$ like in the Dicke model setup.

Doing this, tidying up and dropping the tilde, we finally get the Hamiltonian for the ground state:

\begin{multline}
\label{eq:sm-groundstate-h}
H=-\omega_r \nabla^2 - \frac{1}{4 \Delta_a} 
\left[ \Omega_f^2+\Omega_b^2 +2 \Omega_f \Omega_b \cos (2x) \right]
\\
+ \left[ \Delta_c-\frac{g^2_0}{\Delta_a - \Delta_c} \cos^2(z) \right] \light^{\dagger} \light 
-\frac{g_0}{4} \cos (z) \left( \frac{1}{\Delta_a} +\frac{1}{\Delta_a-\Delta_c} \right) 
\\
\times \left[ \cos(x) (\Omega_f+\Omega_b)(\light^{\dagger}+\light)+
i \sin(x) (\Omega_f-\Omega_b)(\light^{\dagger}-\light) \right]
\end{multline}
