We now want to write Eq. \ref{eq:sm-groundstate-h} in a form that makes the momentum lattice structure obvious.

First, some more simplifying: we work in the $\Omega_f=\Omega_b=\Omega$ case from here on; given that we have assumed large atom-pump detuning to justify adiabatically eliminating the ground state, we also work in the limit $\Delta_a \gg \Delta_c$ so that $\frac{1}{\Delta_a-\Delta_c} \approx \frac{1}{\Delta_a}$. We also write $\omega=\Delta_c$.

This leaves us with:

%\wip{We just lose the constant in 1+cos(2z) and 1+cos(2x) bc constant?}

\begin{multline}
\label{eq:sm-groundstate-h-2}
H=\omega \light^{\dagger} \light  -\omega_r \nabla^2 - \frac{\Omega^2}{2 \Delta_a} \cos (2x)
-\frac{g^2_0}{2 \Delta_a} \cos(2z)\light^{\dagger} \light 
-\frac{g_0 \Omega}{\Delta_a} \cos (x) \cos(z) (\light^{\dagger}+\light) 
\end{multline}

We want to introduce momentum space operators $c_k$ and $c^{\dagger}_k$ and write the Hamiltonian in terms of these. If we define the position space operators $\hat{c}(\vect{r})=\frac{1}{\sqrt{A}} \sum_k e^{ik\vect{r}} \hat{c}_k$ and its Hermitian conjugate $\hat{c}^{\dagger}(\vect{r})$, we can calculate:

\begin{equation}
\label{eq:sm-to-momentum-space}
H_k = \int \mathrm{d} \vect{r} \ \hat{c}^{\dagger}(\vect{r}) \hat{H} \hat{c}(\vect{r}) = \frac{1}{A} \int \mathrm{d} \vect{r} 
\sum_{k,k'} \hat{c}^{\dagger}_{k'} \hat{H} \hat{c}_k e^{i(k'-k)\vect{r}} 
\end{equation}

We can do this for each term of Eq. \ref{eq:sm-groundstate-h-2}. 

The grad squared term will turn into a $-k^2$ in momentum space. The only subtlety is that Eq. \ref{eq:sm-groundstate-h-2} was made unitless in the cavity wavevector $k_0$, so $\omega_r k^2$ is actually:

\begin{equation*}
\frac{k_0^2}{2m} \left(\frac{k}{k_0}\right)^2=\frac{k^2}{2m}
\end{equation*}

in $k$ with dimensionful units.

Similar subtleties will happen in doing the other terms; let's redefine the $k$ labels to facilitate things as $n \vect{q}_x + m \vect{q}_z$, where $\vect{q}_x= k_0 \hvect{x}$ and $\vect{q}_z= k_0 \hvect{z}$ explicitly contain units and directions, i.e. the $n$ and $m$ integers count moving one unit in a $k_0$-spaced momentum lattice in the $x$ or $z$ direction.

Let's now carry out one of the cosine terms to show how these work out. Take for example $\frac{\Omega^2}{2 \Delta_a} \cos (2x)$.

In terms of the new labels, we can write $\cos (2x) = \frac{1}{2} \left( e^{2i \vect{q}_x \cdot \vr} + e^{-2i \vect{q}_x \cdot \vr} \right)$, as previously $z$ contained the factor $k_0 \hvect{x}$. Working out Eq. \ref{eq:sm-to-momentum-space} for this term we get:

\begin{equation}
\frac{1}{2 A} \int \mathrm{d} \vect{r} 
\sum_{n,n',m,m'} \hat{c}^{\dagger}_{n \vect{q}_x + m \vect{q}_z} \hat{c}_{n' \vect{q}_z + m' \vect{q}_x}
(e^{i(n-n'-2)\vect{q}_x \cdot \vr} +e^{i(n-n'+2)\vect{q}_x \cdot \vr} )
 e^{i(m'-m)\vect{q}_z \cdot \vr} 
\end{equation}

%%%THIS ME

From the integral over the exponents we get the delta functions $\delta_{n,n'}$ and $\delta_{m,m'\pm 2}$. We introduce a sum over a parameter $s$ encoding the two signs of scattering, i.e. $s=\pm 1$, so that we can write the two terms as $\frac{\Omega^2}{4} \sum_{s} \hat{c}^{\dagger}_{\textbf{k}}\hat{c}_{\textbf{k}+2s\textbf{q}_x}$ where we've recomposed the $k$ labels for tidiness and freed up the $n$ and $m$ levels to write the atomic state below.

Doing the rest of the terms similarly, we can write the momentum space Hamiltonian:

\begin{multline}
\label{eq:sm-momentum-H}
\hat{H}=\omega \hat{\light}^{\dagger} \hat{\light} + \sum_k \bigg[ 
\ \frac{k^2}{2m}\hat{c}^{\dagger}_{\textbf{k}}\hat{c}_{\textbf{k}}
-\frac{g_0^2}{4\Delta_a}\hat{\light}^{\dagger}\hat{\light} \sum_{s}^{\pm1} \hat{c}^{\dagger}_{\textbf{k}}\hat{c}_{\textbf{k}+2s\textbf{q}_z} \\
- \frac{g_0 \Omega}{4\Delta_a}(\hat{\light}^{\dagger}+\hat{\light}) \sum_{s,s'}^{\pm1}
\hat{c}^{\dagger}_{\textbf{k}}\hat{c}_{\textbf{k}+s\textbf{q}_x+s'\textbf{q}_z}
-\frac{\Omega^2}{4\Delta_a}\sum_s^{\pm1} \hat{c}^{\dagger}_{\textbf{k}}\hat{c}_{\textbf{k}+2s\textbf{q}_x} \bigg]
\end{multline}

Having built the momentum space structure, we can use it to write the collective atomic state of the $N$ Bose-condensed (i.e. indistinguishable) atoms. We have a lattice structure labelled by $n$ and $m$, the sites of which we want to populate with atoms, with an associated weight.

Since we have a Bose-condensed gas, we have a single macroscopically occupied wavefunction, i.e. $N$ particles in the same single particle state. We can write this as:

\begin{equation}
\label{eq:sm-atom-states}
\ket{\Phi}=\left( \sum_{nm} \phi_{nm} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right)^N \ket{0}
\end{equation}

This allows us to work with the $\phi_{nm}$ as the effective atomic wavefunction over the fixed energy levels, as the rest is of Eq. \ref{eq:sm-atom-states} is structure.