Before continuing, we want to take a slight detour and explain how to connect the microscopic parameters $g_0$, $\omega_r$, $\Delta_c$ and $\Delta_a$ of Eq. \ref{eq:sm-groundstate-h} to the macroscopic parameters $g$, $U$, and $\omega$ that appear in Dicke model equations:

\begin{equation}
\label{eq:oh-no-its-Dicke-again}
H=\omega_0 S^z + (\omega+U S^z) \light^{\dagger} \light + g(\light^{\dagger} S^{-}+\light S^{+})+ g'(\light^{\dagger} S^{+}+\light S^{-})
\end{equation}

While we omit the detail of this and refer to Ref. \cite{Bhaseen2012}, for it, we sketch here a way to understand how to go about doing this.

If we build a basis for the atomic states, such that the basis states are eigenstates of the first line of Eq. \ref{eq:Bh2010-A5}, we can obtain expectation values out of the Hamiltonian -- this should make intuitive sense.

Let us call this basis set $\phi_{m}$; this is a set of solutions in the pump direction only, as these solve the atomic and pump part of the Hamiltonian only.

On top of these, there should be a basis for the cavity structure; a sensible choice for this is $\cos (nx)$ (plus normalisation), as this will make calculating expectations with the coupling term of the Hamiltonian (which goes with $\cos (x)$) more straightforward\footnote{The $n=1$ will yield a factor of a half from a $\cos^2 (x)$ contribution, and anything $n>1$ will yield a zero overlap.}.

Let's put these together and write a function of the form:

\begin{equation}
\Phi_{n,m}\varpropto\phi_m \cos(nx)
\end{equation}

As in Ref. \cite{Bhaseen2012}, in the case when $\Omega_f=\Omega_b=\Omega$ we can rewrite the first line of Eq. \ref{eq:sm-groundstate-h} as the Mathieu equation. We then use the even solutions\footnote{ If $\Omega_f \ne \Omega_b$, the odd subspace of Mathieu solutions is needed as well.} to the Mathieu equation $\partial^y + (a-2q \ cos(2x) )y=0$ for $\phi_m$ works. The Mathieu parameter is here $q=\Sigma_f \Sigma_b / ( 4 \Delta_a \omega_r)$, a dimensionless measure of pump strength.

We can now use these states to evaluate expectation values and compare to the Dicke model parameters by restricting to only using the first two states of our basis as the Dicke ground and excited state.

From the Dicke model, we can write the energy of ground and excited state by setting $S^z \pm N/2$ in the non-interacting part of Eq. \ref{eq:oh-no-its-Dicke-again}:

\begin{equation}
\label{eq:Dicke-params1}
E_{\uparrow,\downarrow}= \mp \frac{\omega_0}{2} N + \left(\omega \pm \frac{UN}{2} \right) n_{ph}
\end{equation}

where $n_{ph}$ is the number of photons. The terms of this can be obtained by comparing to expectation values calculated with Eq. \ref{eq:sm-groundstate-h}, $\langle\Phi_{0,0} \vert \tilde{H} \vert \Phi_{0,0}\rangle$ and $\langle\Phi_{1,1} \vert \tilde{H} \vert \Phi_{1,1}\rangle$.

To begin, $\omega$ and $U$ can be obtained by we need to compare the contents of the brackets in Eq. \ref{eq:Dicke-params1} to the matrix elements calculated from the $\light^{\dagger} \light$ term of Eq. \ref{eq:sm-groundstate-h}. 

We know this term will have the following general form:

\begin{equation}
\label{microscopic-dicke-params}
\Delta_C-\frac{g_0^2}{\Delta_c-\Delta_a} (A S^z + B)
\end{equation}

We can determine $A$ and $B$ by calculating the expectation of this term for $\Phi_{0,0}$ and $\Phi_{1,1}$ and comparing to $S^z= \pm 1/2$. This means calculating the matrix elements $\langle \Phi_{0,0} \vert \cos^2 x \vert \Phi_{0,0}\rangle$ and $\langle \Phi_{1,1} \vert \cos^2 x \vert \Phi_{1,1}\rangle$.

Calculating $\langle \Phi_{0,0} \vert \cos^2 x \vert \Phi_{0,0}\rangle$ is just obtaining the average $\langle \cos^2 x \rangle=1/2$, as $\Phi_{0,0}$ has no structure in the $x$ direction (and the $\phi_n$ are normalised).

To calculate $\langle \Phi_{1,1} \vert \cos^2 x \vert \Phi_{1,1}\rangle$ we need to obtain  $\langle \cos^4 x \rangle$; this is:

\begin{equation}
\langle \cos^4 x \rangle = \frac{1}{4} \langle (1+ \cos^2(2x)^2 \rangle=
\frac{1}{4} (1+\langle \cos^2 (2x) ) \rangle)= \frac{3}{8}
\end{equation}

as $\langle \cos x \rangle =0$. However, we also need to normalise this, and if $\langle \Phi_{1,1} \vert \Phi_{1,1}\rangle$ and $\langle \phi_n \vert \phi_{n} \rangle$ are normalised, we need to add a factor of $2$ such that the cosine squared parts normalise. Therefore, $\langle \Phi_{1,1} \vert \cos^2 x \vert \Phi_{1,1}\rangle = 3/4$. 

We can now obtain $A$ and $B$ by the solving $A S^z+B$ with the values we have calculated for $S^z=\pm N/2$:

\begin{align*}
-A \frac{N}{2}+B &=\frac{N}{2}\\
A \frac{N}{2} + B &= \frac{3N}{4}
\end{align*}

which yields $B=\frac{5N}{8}$ and $A=-\frac{1}{4}$. Plugging these into the braket of Eq. \ref{microscopic-dicke-params} and comparing to the bracket of \ref{eq:Dicke-params1}, we get:

\begin{equation}
\omega=\Delta_c - \frac{5 g_0^2 N}{8(\Delta_c-\Delta_a)}
\end{equation}

and 

\begin{equation}
U=-\frac{g_0^2}{4(\Delta_a-\Delta_c)}
\end{equation}

We can also obtain $\omega_0$ in the weak pumping limit, which is necessary for a two level approximation; we can tell $\omega_0 = \omega_r (1+F(q))$ from the first line of Eq. \ref{eq:sm-groundstate-h}, evaluating the second term at $s$, we can approximate this to $\omega_0\thicksim 2 \omega_r$.

Calculating the coupling $g=g'$ (as we've set the microscopic pump frequencies equal) will require getting the off-diagonal terms $\langle \Phi_{0,0} \vert \tilde{H} \vert \Phi_{1,1}\rangle=g\light^{\dagger}+g'\light$ with the Mathieu solutions; doing this, and approximating the Mathieu equation solutions as the Mathieu parameter $q$ is small, one gets:

\begin{equation}
g=g'\approx - \frac{g_0 \Omega}{4} \left( \frac{1}{\Delta_a} + \frac{1}{\Delta_a-\Delta_c} \right)
\end{equation}

That concludes the parameter comparison; we have drawn a complete picture of the connection of the microscopics we will be working with to the well-established Dicke model, and why they are equivalent in the two-level approximation.