We now want to derive equations of motion for the light field, and the atomic field in the form of the $\phi_{nm}$ paramatrisation we've introduced.

We start with $\phi_{nm}$ as they require a bit of work to dig out of the current Hamiltonian.

In order to derive $\partial_t \phi_{n,m}$, we need to begin by considering Lindblad equation $\partial_t \rho=-i [H, \rho]- \mathcal{L}[\psi]$ as introduced in \ref{master-equations} and assume a few things explicitly. 

For the atoms, the loss term is zero, so we only have $\partial_t \rho=-i [H, \rho]$. We assume $\rho_{atom}=\ket{\Phi}\bra{\Phi}$, where $\ket{\Phi}$ is Eq. \ref{eq:sm-atom-states}, and that the total density matrix is separable, $\rho=\rho_{atom} \otimes \rho_{light}$.

We have a Hamiltonian $H=H_{atom}+H_{light}+H_{atom,light}$, which means 

\begin{multline}
\partial_t \rho_{atom} = \Tr_{light} \Big(
-i[H_{atom},\rho_{atom} \otimes \rho_{light}]
-i[H_{atom,light},\rho_{atom} \otimes \rho_{light}]\\
-i[H_{light},\rho_{atom} \otimes \rho_{light}]
-\mathcal{L}[\psi] 
\Big)
\end{multline}

The last term is zero, and the second to last term also traces out. Tracing out the light field from the second term replaces the $\psi$ operators with their expectation value in the interaction terms as the light field part of these terms will look like $Tr_{light} (\psi \rho_{light})=\langle \psi \rangle$ and so forth. So after tracing out the light field from the first terms we're left with:

\begin{equation}
\partial_t \rho_{atom}=-i[\tilde{H}_{atom},\rho_{atom}]
\end{equation}

where $\tilde{H}_{atom}=H_{atom}+H_{atom,light}(\psi \rightarrow \langle \psi \rangle)$.

Given that we've assumed $\rho_{atom}=\ket{\Phi}\bra{\Phi}$, we can write this as:

\begin{equation}
\partial_t \ket{\Phi}\bra{\Phi} + \ket{\Phi} \partial_t \bra{\Phi} = - i \tilde{H} \ket{\Phi}\bra{\Phi}+i \ket{\Phi}( \bra{\Phi} \tilde{H})
\end{equation}

from which we get $\partial_t \ket{\Psi}=-i H \ket{\Psi}$ and the Hermitian conjugate equation for $\bra{\Psi}$.

We can compute $H \ket{\Psi}$ piece by piece with some book-keeping care; take for example the term $c_k^{\dagger} c_{k+2s \vect{q}_x}$ which we calculated explicitly in the previous section:

\begin{equation}
\label{eq:sm-example-eom-deriv}
\partial_t \left( \sum_{nm} \phi_{nm} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right)^N \ket{0} 
= +i \frac{\Omega^2}{4 \Delta_a} \sum_{k,s}
c_k^{\dagger} c_{k+2s \vect{q}_x} \left( \sum_{nm} \phi_{nm} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right)^N \ket{0}+...
\end{equation}

Consider this term without the leading $c_k^{\dagger}$. Let's write it in a cartoon way as $c (...)^N \ket{0}$ so we can sketch how to get this term simply.

First, we can write $c (...)^N \ket{0}= (...)^N c \ket{0}+[c,(...)^N] \ket{0}$. The first term is zero as it has an annihilation operator acting on the empty state. Expanding a commutator of powers would normally have the structure $[a,b^n]=b^{n-1}[a,b]+b^{n-2}[a,b]b+...[a,b]b^{n-1}$; however, we know here that $[c,(...)]$ is a number as this is the commutator $[c_n,c^{\dagger}_m]$ plus prefactors, so we can write $[c,(...)^N] \ket{0}=N(..)^{N-1}[c,(...)]$. Doing the commutator explicitly, where we've expanded the $k$ into components for clarity:

\begin{equation}
\label{sm-eom-deriv-comm}
\left[c_{(u+2s) \vect{q}_x+v \vect{q}_z}, \sum_{nm} \phi_{nm} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right]=\sum_{nm} \phi_{nm} \delta_{u+2s,n} \delta_{v,m}
\end{equation}

So the right hand side of Eq. \ref{eq:sm-example-eom-deriv} is:

\begin{equation}
\label{eq:sm-example-eom-deriv-2}
i \frac{\Omega^2}{4 \Delta_a} \sum_{u,v} \phi_{u+2s, v} c^{\dagger}_{u\textbf{q}_x+v\textbf{q}_z} N (...)^{N-1}  \ket{0}
\end{equation}

The left hand side of Eq. \ref{eq:sm-example-eom-deriv} yields:

\begin{equation}
\partial_t \left( \sum_{n,m} \phi_{n,m} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right)^N \ket{0} = N (...)^{N-1} \left( \sum_{n,m} \partial_t \phi_{n,m} c^{\dagger}_{n\textbf{q}_x+m\textbf{q}_z} \right)^N \ket{0}
\end{equation}

From this, eliminating common terms, we get: 

\begin{equation}
\partial_t \phi_{n,m}=i \frac{\Omega^2}{4 \Delta_a} \sum_{s}  \phi_{n+2s, m}+...
\end{equation}

We build the rest of the terms the same way.

The light field equation is a little different, as we want to work with time evolution of expectation values, i.e. Heisenberg picture; getting the commutators $[H,\psi]$ is straightforward for the light-only terms, whereas with the same reasoning as the beginning for atoms, the atom-light terms require calculating the overlaps $\hat{O}\rho_{atom}$. 

This works similarly to the calculation we've sketched above, but because it's of the form $\bra{\Phi} c_k^{\dagger} c_{k+2s \vect{q}_z} \ket{\Phi}$ the calculation happens `twice', one side acting on the bra and the other on the ket, so that two $\phi_{n,m}$ are picked out by the ladder operators with matching quantum numbers; we also get an extra factor of $N$ as there are two $N$ factors like in Eq. \ref{eq:sm-example-eom-deriv-2}, only one of which is cancelled by the $N$ factor on the left-hand side.

Doing this for all terms of the Hamiltonian for both $\phi_{n,m}$ and $\psi=\langle \psi \rangle$, we get equations of motion:

\begin{align}
\partial_t \phi_{nm} = &-i \omega_r (n^2+m^2) \phi_{nm} + i \frac{g_0^2 \abs{\psi}^2}{4 \Delta_a}\sum_s \phi_{n,m+2s}
\nonumber \\&
+i \frac{g_0 \Omega (\psi+\psi^{\dagger})}{4 \Delta_a}\sum_{s,s'} \phi_{n+s,m+s'}
+i \frac{\Omega^2}{4 \Delta_a}\sum_{s}\phi_{n+2s,m}
\\
\partial_t \psi= &-(\kappa+i\omega)\psi+i \frac{g_0^2 N}{4 \Delta_a} \psi \sum_s \phi^*_{n,m+2s}\phi_{n,m} \nonumber \\ 
&+i \frac{g_0 \Omega N}{4 \Delta_a} \sum_{s,s'} \phi^*_{n+s,m+s'}\phi_{n,m}
\end{align}

where, again, the $n$ quantum number spans the cavity direction, while the $m$ quantum number spans the pump direction of the momentum lattice of spacing $k_0$, the wavevector of the light.

We are almost done, and just need to tidy up our quantities. We introduce the dimensionless paramters:

\begin{equation}
P^2=\frac{\Omega^2}{4 \Delta_a \omega_r}, \qquad \lambda^2=\frac{g_0^2 |\psi|^2}{4 \Delta_a \omega_r}
\end{equation}

which are a scaled, dimensionless version of pump and light field.

We also define the quantity $E_0$ as in Ref. \cite{Keeling2014}, which yields the magnitude of the backaction of the cavity field on the atoms\footnote{In other words, this term is the microscopic version of the $U$ term in Dicke.}:

\begin{equation}
E_0=\frac{g^2 N}{4 \Delta_a}
\end{equation}

We now have equations of motion in $\phi_{nm}$ and the light field $\lambda$, which will be our starting point:

\begin{align}
\label{eq:single-mode-EOMS1}
\partial_t \phi_{nm} = &-i \omega_r \bigg[ (n^2+m^2) \phi_{nm}-|\lambda|^2 \sum_s \phi_{n+2s,m}\nonumber \\&-P(\lambda+\lambda^*)\sum_{s,s'} \phi_{n+s,m+s'}-P^2 \sum_{s}\phi_{n,m+2s} \bigg]\\
\label{eq:single-mode-EOMS2}
\partial_t \lambda= &-(\kappa+i\omega)\lambda+iE_0\lambda \sum_s \phi^*_{n+2s,m}\phi_{n,m} \nonumber \\ 
&+iE_0P \sum_{s,s'} \phi^*_{n+s,m+s'}\phi_{n,m}
\end{align}

\wip{check whether need more experimental paramters in table}

In these results, as in \cite{Bhaseen2012}, we will be using parameter values approximately describing the experiment from Ref. \cite{Baumann2010}. These values are reported in Table \ref{tab:sm-num-vals}. In the code and results, we will often be using a dimensionless form of the frequency $\omtilde$; we define this as $\omtilde=\omega/E_0$.

%%%TABLE OF PARAMETERS%%%

\begin{table}[t]
\centering
    \begin{tabular}{ | c | c | }
    \hline
    \textbf{Parameter} & \textbf{Value} \\ \hline
    $\mathrm{N_a}$ & $10^5$ \\	
  	$\kappa$ & 8.1 MHz \\
  	$E_0$ & 40 MHz \\
  	$\omega_r$ & 0.05 MHz \\
    \hline
\end{tabular}
\caption{Numerical values of model parameters describing approximately the experiment of Ref. \cite{Baumann2010}. Units are absent unless specified.}
\label{tab:sm-num-vals}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%