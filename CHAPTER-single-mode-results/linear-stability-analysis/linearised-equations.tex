We want to linearise the equations of motion in the light field -- that is, to only keep the contributions from single-photon scattering. This is valid if $\lambda$ is small; we know $\lambda=0$ up to the superradiant transition, and then in a second order transition the order parameter grows continuously as discussed in Section \ref{phase-transitions}. The linear stability isn't reliable in a first order transition, and we will have to use different tools for that.

We will make use of this consideration in a moment; first, we need to apply some gentle pressure to the atomic field equations, so that we may massage them into something much more treatable.

We want to begin by mapping the atomic field onto an explicitly multilevel field as follows:

\begin{equation}
\label{eq:linstab1}
\phi_{nm}=\alpha \phi_{nm}^0 +\sum_{l} \beta_{l} \phi^l_{nm}
\end{equation}

where the expressions $\phi^l_{nm}$ are the $l$th atomic momentum eigenstate of Eq.\ref{eq:single-mode-EOMS1} with $\lambda=0$ (i.e. the bare atomic eigenstates), the coefficients $\alpha$ and $\beta_l$ determine the relative contribution to the overall state, and the $\phi_{nm}$ are normalised, $|\phi_{nm}^l|^2=1$. 

The reason we want to do this is that, if we can then calculate the (fixed) eigenstates by physical considerations, we only need worry about the equations of motion of the coefficients $\alpha$ and $\beta_l$, and we will have substantially reduced the complexity of the coupled equations -- that is, we want to go from our equations of motion in \(\partial_{t} \phi_{nm} \) and \(\partial_{t} \lambda \) to equations of motion for \(\partial_{t} \alpha \), \(\partial_{t} \beta_{l} \) and \(\partial_{t} \lambda \) in terms of \( \alpha,\beta_{l} \) and \(\lambda\) only. 
In order to do this, we will need to calculate the overlaps between the eigenstates \(\phi\) terms in our original equations of motion explicitly (i.e. obtain the coefficients explicitly).

Let us consider these eigenvectors.

We know \(\phi^0\) should be the ground state of Hamiltonian:

\begin{center}
\( \hat{H} \)= kinetic energy + pump lattice
\end{center}

since we know there will be no cavity photon field for the ground state\footnote{As a reminder: n indexes span the pump direction, m indexes the cavity direction.}. 

We can also put together an effectuve Hamiltonian for the atomic fields \( \phi^l\), indexed by the lattice numbers (note that these lattice indeces refer to a momentum-space lattice), because we know the effect of the scattering from the full Hamiltonian, as:

\begin{equation}
H_{(n'm')(nm)}=(n^2+m^2) \delta_{nn'} \delta_{mm'}-P^2 \sum_{s=\pm1}\delta_{n,n'+2s} \delta_{m,m'}
\end{equation}

where as a reminder, $P$ is the dimensionless pump power.

Because the problem is diagonal in $m$, the ground state will be a product of an $m$-dependent part and an $n$-dependent part; we know that $m=0$ for the ground state, so for $\phi^0$ we have:

\begin{equation}
\label{eq:linstab2}
\phi_{nm}^0=\delta_{m,0} \chi_{n}^0
\end{equation}

where the $n$-dependent part satisfies the equation:

\begin{equation}
\label{eq:linstab3}
\hat{h}_{n'n}^0 \chi_{n}^0= \epsilon^0 \chi_{n}^0
\end{equation}

with the $n$-only, ground state Hamiltonian (obtained from the full Hamiltonian) being:

\begin{equation}
\label{eq:linstab3b}
h_{n'n}^0=\delta_{nn'}n^2-P^2\sum_{s=\pm1} \delta_{n,n'+2s}
\end{equation}

Note that this form of $h$ defines two separate subspaces of even and odd numbered indices, because the scattering only goes between indices two units apart. This makes sense in the context of the even/odd site localisation of the superradiant phase which we've discussed extensively in Chapter \ref{sm-lit} and above.

Let us assume that the even subspace is the one yielding the lower eigenvalue, and hence the one relevant to us in considering the ground state (this is justified in the sense that it is definitely true for $P=0$, but the fact that it holds for nonzero $P$ is technically unproven).

We sketch out the form of $h^0_{nn'}$ here to help with visualising what it looks like:

\begin{equation}
\label{eq:linstab-bigmatrix}
h^0_{m'm}=
\begin{pmatrix}
(-m)^2 & 0 & -P^2 & 0 & \cdots &  &  & \cdots & 0 \\
0 & \ddots & \ddots & \ddots &  &  &  &  & \vdots \\
-P^2 & \ddots & 4 & 0 & -P^2 & 0 & 0 & & \\
0 & \ddots & 0 & 1 & 0 & -P^2 & 0 & & \\
\vdots & & -P^2 & 0 & 0 & 0 & -P^2 & & \\
 & & 0 & -P^2 & 0 & 1 & 0 & & \vdots \\
 & & 0 & 0 & -P^2 & 0 & 4 & \ddots & -P^2  \\
\vdots & &  & &  & \ddots & \ddots & \ddots & 0  \\
0 & \cdots &  & & \cdots & 0 & -P^2 & 0 & m^2\
\end{pmatrix}
\end{equation}
\\
We can then imagine diving this into even- and odd-only subspaces, and using the even-only version to compute $\xi_n^0$ with only even entries.

Having obtained the structure of the ground state eigenvector, let us consider $\phi_{nm}^l$. 

Here is where we finally make use of the assumption of linearisation in the light field: because we are only considering single photon processes, the $l$-th eigenstate is restricted to being the $l$th momentum eigenstate that can be reached by one-photon scattering starting from the ground state, i.e. it has to be the $l$-th lowest level yielding a nonzero overlap with $\sum_{s,s'}\phi_{n+s,m+s'}^0$, $s=s'=\pm1$. We can encode this as:

\begin{equation}
\label{eq:linstab4}
\phi_{nm}^l=\frac{1}{\sqrt{2}}\chi^l_n (\delta_{m,1}+\delta_{m,-1})
\end{equation}

Like before, the vector $\chi^l_n$ obeys a transition Hamiltonian:

\begin{equation}
\label{eq:linstab5}
\hat{h}_{n'n}^l \chi_{n}^l= \epsilon^l \chi_{n}^l
\end{equation}

with:

\begin{equation}
\label{eq:linstab6}
h_{n'n}^l=\delta_{nn'}(1+n^2)-P^2\sum_{s=\pm1} \delta_{n,n'+2s}
\end{equation}

This has a similar matrix structure to Eq.\ref{eq:linstab3b} with the extra momentum from the photon scattering. 

Similarly to the above, we have obtained an equation in terms of a vector depending purely on $n$, $\chi_n^l$, and carried out the $m$-dependence explicitly in the delta functions. The eigenvalues $\epsilon$ can simply be computed explicitly, because we have defined the structure of both the Hamiltonian and the $\chi$ vectors.

We can now substitute Eq.\ref{eq:linstab1} in our equations of motion, and then make use of Eq.\ref{eq:linstab2} and \ref{eq:linstab4} and some book keeping to obtain equations of motion for \(\partial_{t} \alpha \), \(\partial_{t} \beta_{l} \) and \(\partial_{t} \lambda \).

We will briefly sketch the derivation and obtain the equations of motion for the linear stability analysis.

To do this, we multiply $\partial_t \phi_{nm}$ by $(\phi_{nm}^0)^*$ (to obtain $\partial_t \alpha$)\footnote{Note that since we've defined $\phi^l_{nm}$ as fixed eigenstates, they are time-independent and we can bring $(\phi_{nm}^0)^*$ inside the time derivative.}:

\begin{align}
\partial_t [ (\phi_{nm}^0)^* \phi_{nm} ] &= \partial_t \bigg( \alpha (\phi_{nm}^0)^* \phi^0_{nm}+\sum_l \beta_l (\phi_{nm}^0)^* \phi_{nm}^l \bigg)= \\
&=\partial_t \alpha
\end{align}

where we've used orthonormality of the wavefunctions (which can alternatively be verified through the delta function definitions). Similarly, we multiply $\partial_t \phi_{nm}$ by $(\phi_{nm}^l)^*$ to obtain $\partial_t \beta_l$; finally, we obtain new equations of motion:

\begin{align}
&\partial_{t} \alpha = -i \omega_{r}  [ \alpha \epsilon^0 -\sqrt{2} P (\lambda+\lambda*)\sum_{l,m,s\pm 1} \beta_{l}  \chi_{n}^0 \chi^l_{n+s} ]\\
&\partial_{t} \beta_{l} = -i \omega_{r}  [ \beta_{l} (\epsilon^l - \vert \lambda\vert^2)-\alpha \sqrt{2} P  (\lambda+\lambda*) \sum_{l,m,s\pm 1} \chi_{n}^l \chi^0_{n+s} ]\\
&\partial_{t} \lambda = -(\kappa+i \omega) \lambda +  i E_{0} \sum_{l} [ \lambda \vert \beta_{l} \vert^2 + P \sqrt{2} ( \alpha^* \beta_{l} + \alpha \beta^*_{l})  \sum_{m,l} \sum_{s\pm 1} \chi_{n}^l \chi^0_{n+s} ]
\end{align}

Where, again, we have neglected any scattering to higher cavity momentum states because of the assumption that $\lambda$ is small.

Then, definining the overlaps as coefficients which can later be calculated numerically,

\begin{equation}
O^l=\sum_{n,s}  \chi_{n}^l \chi^0_{n+s} = \sum_{n,s}  \chi_{n}^0 \chi^l_{n+s}
\end{equation}

We get to the final expression for the equations of motion in terms of $\alpha$, $\beta_l$ and $\lambda$ only:

\begin{align}
\label{eq:linstab-EOM1}
&\partial_{t} \alpha = -i \omega_{r}  [ \alpha \epsilon^0 -\sqrt{2} P (\lambda+\lambda^*)\sum_{l} \beta_{l}  O^l ]
\\
\label{eq:linstab-EOM2}
&\partial_{t} \beta_{l} = -i \omega_{r}  [ \beta_{l} (\epsilon^l - \vert \lambda\vert^2)-\alpha \sqrt{2} P  (\lambda+\lambda^*) O^l ]
\\
\label{eq:linstab-EOM3}
&\partial_{t} \lambda = -(\kappa+i \omega) \lambda +  i E_{0} \sum_{l} [ \lambda \vert \beta_{l} \vert^2 + \sqrt{2} P( \alpha^* \beta_{l} + \alpha \beta^*_{l}) O^l ]
\end{align}