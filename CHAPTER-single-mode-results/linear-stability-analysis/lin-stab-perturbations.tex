
Before we begin with the linear stability itself, we want to point out that in the normal state, i.e. all atoms are in the ground state, $\alpha=1$, $\beta_l=0$ for all $l$ in our explicit multilevel setup, and the light field in the cavity is zero, $\lambda=0$. This yields:

\begin{equation}
\partial_t \alpha = -i \omega_r \epsilon^0 \alpha \quad \therefore \quad \alpha=e^{-i \omega_r \epsilon^0 t} \alpha_0
\end{equation}

Therefore, we want to transform into a co-rotating frame of the ground state coefficient so we won't have to worry about its time dependence: 

\begin{equation}
\alpha \rightarrow e^{-i \omega_r \epsilon^0 t} \tilde{\alpha} \quad \mathrm{and} \quad \beta_l \rightarrow e^{-i \omega_r \epsilon^0 t} \tilde{\beta_l} 
\end{equation}

This leaves the equations of motion largely unchanged (the light field equation is completely unchanged other than an addition of tildes on alphas and betas since the exponential factors end up cancelling):

\begin{align}
&\partial_{t} \tilde{\alpha} = i \omega_{r} \sqrt{2} P (\lambda+\lambda^*)\sum_{l} \tilde{\beta}_{l}  O^l 
\\
&\partial_{t} \tilde{\beta}_{l} = -i \omega_{r}  [ \beta_{l} (\epsilon^l-\epsilon^0 - \vert \lambda\vert^2)-\tilde{\alpha} \sqrt{2} P  (\lambda+\lambda^*) O^l ]
\end{align}

We can now introduce small perturbations around the steady state (dropping the tildes from here on for convenience):

\begin{equation}
\alpha=1+\delta \alpha, \quad \beta=\delta \beta, \quad \lambda= \delta \lambda
\end{equation}

Substituting these into Eq.\ref{eq:linstab-EOM1}-\ref{eq:linstab-EOM3} and only keeping terms of linear order in the small perturbations, i.e. discarding anything with more than one delta term under the assumption that these become too small to matter:

\begin{align}
\label{lin-pert-EOM1}
&\partial_t (\delta \alpha)=0 \\
\label{lin-pert-EOM2}
&\partial_t (\delta \beta_l)= -i \omega_{r}  [(\epsilon^l-\epsilon^0)\delta \beta_l - \sqrt{2} P  (\delta \lambda+\delta \lambda^*) O^l ] \\
\label{lin-pert-EOM3}
&\partial_t (\delta \lambda) = -(\kappa+i \omega_{r}) \delta \lambda + i \sqrt{2} E_{0} P \sum_{l} [(\delta \beta_l+\delta \beta_l^*) O^l ]
\end{align}

A general solution to this set of coupled differential equations is the following (where the phase must match to satisfy the coupled equations):

\begin{align}
&\delta \beta_l=u_l e^{-i\nu t} + v_l e^{i \nu^* t} \\
&\delta \lambda=x e^{-i\nu t} + y e^{i \nu^* t}
\end{align}

If $\nu$ is complex, then the imaginary part is what contributes to the growth or decay of the oscillations (i.e. turns into a real exponential). 

When the imaginary part of $\nu$ is negative, the oscillations decay exponentially, since $e^{-i \Im m(\nu) t}=e^{\Im m (\nu) t}$ is negative if $\Im m (\nu)$ is negative), i.e. we are in the stable equilibrium.
In contrast, when the imaginary part is positive, the oscillations grow exponentially, i.e. the steady state goes unstable. 
Right at the linear stability line, therefore, $\nu$ is purely real, since its imaginary part is crossing from negative to positive. This is how we can find the linear stability line.

Substituting the general solutions into the Eq.\ref{lin-pert-EOM1}-\ref{lin-pert-EOM3} and rearranging terms, one can eliminate $u_l$ and $v_l$ from the expressions, obtaining equations exclusively in terms of $x$, $y$, and $\nu$. Defining the following quantity to tidy up the algebra:

\begin{equation}
\chi(\nu)= 4 \omega_r^2 E_0 P^2 \sum_l (O^l)^2 \frac{\Delta \epsilon^l}{(\omega^2 \Delta\epsilon^l)^2-\nu^2}
\end{equation}

where $\Delta\epsilon^l=\epsilon^l-\epsilon^0$, and noting that $A(\nu)^*=-A(\nu)$ one obtains a set of equations in $x$ and $y$, which can be written as a matrix $M(\nu)$:

\begin{equation}
\begin{pmatrix}
(i \nu - \kappa) - i \omega  -i \chi(\nu) & -i \chi(\nu) \\
-i \chi(\nu) & - (i \nu - \kappa) - i \omega  -i \chi(\nu)
\end{pmatrix}
\begin{pmatrix}
x \\
y
\end{pmatrix}
=0
\end{equation}

The solution to this, $Det[M(\nu)]=0$, is actually a set of two equations, because this is a complex quantity, and yields a family of solutions to the above. Therefore, one must solve the set of equations:

\begin{align}
&\Re e(Det[M(\nu)])=0 \\
\label{linstab-impart}
&\Im m(Det[M(\nu)])=0 \\
&Det[M(\nu)]=-(i\nu-\kappa)^2-\omega^2 + 8 \omega E_0 \omega_{r}^2 P^2 \sum_l \frac{\Delta\epsilon^l(O^l)^2}{(\omega_r^2 \Delta\epsilon^l)^2-\nu^2}
\end{align}

However, as discussed above, at the linear stability line $\Im m(Det[M(\nu)])=0$; then, Eq.\ref{linstab-impart} yields the trivial solution:

\begin{align}
\Im m(Det[M(\nu)])&=-2i\nu \kappa=0 \quad \rightarrow \quad \nu=0
\end{align}

And we're left with the real equation:

\begin{equation}
\label{linstab-secular}
Det[M(\nu)]=-\kappa^2-\omega^2 + 8 \omega E_0 \omega_{r}^2 P^2 \sum_l \frac{\Delta\epsilon^l(O^l)^2}{(\omega_r^2 \Delta\epsilon^l)^2}=0
\end{equation}

which is simply a quadratic equation in $\omega$ that yields the location of the linear stability line, i.e. solving this equation yields the critical pump $P_{crit}$ for a given $\omega$. 
