Having charted an analytical map of the system, we can now take a step back to our original equations of motion, Eqs.\ref{eq:single-mode-EOMS1} and \ref{eq:single-mode-EOMS2}.

In doing so, we are shedding several assumptions that made our life easier: linearity in the light field, linear stability analysis. We're very much back to a computationally expensive set of coupled equations -- a single point time evolution takes one day of CPU time. 

There are, however, a few factors to consider in favour of this approach: one, that this problem can be `trivially' parallelised extremely efficiently, that is, one can just run several points at once. Given that we already have a road map of where to look in the form of a phase boundary, and armed with some further considerations we will discuss below, this will reduce the actual time to obtain vast amount of data substantially; and two, computationally expensive is economically fairly cheap. A computer running for a few weeks to find something interesting to inform whether it is worth building a complex experimental apparatus is a good investment.

The key idea is to code in a starting state and a time evolution routine following Eqs.\ref{eq:single-mode-EOMS1} and \ref{eq:single-mode-EOMS2}, and simply let the system model evolve in time\footnote{`Simply' here being a somewhat hindsight-driven choice of words.}. As mentioned, this yields about 5s of simulated time evolution in roughly a day of cpu time --- but is also inexpensive and yielding readily available data.

There are two aspects of the phase diagrams that the linear stability could not by nature capture, which we hinted at: the nature of the phase boundary line, and the dynamics within the phase transition. 

Considering this, the code is set up to perform two different types of operation:

\begin{itemize}
\item Adiabatic sweeps across (up, down, or up and back down) either parameters $\tilde{\omega}$ or $P$ - that is, starting at a point $(P, \tilde{\omega})$, letting the system reach a steady state, and then using the state as the starting state for the next set of coordinates, either $(P+\delta P, \tilde{\omega})$ or $(P, \tilde{\omega}+\delta\tilde{\omega})$ -- where $\tilde{\omega}=\omega/E_0$ is the dimensionless frequency we actually use in the code, and in the results from here on.  

There is a subtlety here, which is -- this may not always happen, as we do not expect the system to transition into `trivial', equilibrium superradiance in the region of instability below $\tilde{\omega}=2$, as discussed in Section \ref{sm-preliminary}. One needs to be very careful in handling this. Our code contains a time limit after which the code will carry on with the sweep even if the state hasn't converged, so one needs to consider this carefully, and the code contains several error messages to diagnose this.

This mode of operation is integrated with the linear stability code from Section \ref{sm-lin-stab}. It is possible when sweeping across $\tilde{\omega}$ to obtain the critical value from the linear stability code; the code will then automatically start and end at some small distance $d \tilde{\omega}$ on either side of the critical value, scanning across the stability boundry, to investigate the order of transition and existence of multistability.

\item Single point time-evolution, that is, starting the system at a certain value of parameters $P$ and $\tilde{\omega}$. Physically, this is equivalent to a sudden quench into the superradiant regime, instead of ramping up into it. This is the mode of operation which yields the dynamics of the superradiant behaviour; it is less computationally expensive, and since it is not attempting any sort of ramping or parameter evolution beyond the time evolution, one can just let the system time evolve for as long as it necessary for the point which do not converge.

\end{itemize}